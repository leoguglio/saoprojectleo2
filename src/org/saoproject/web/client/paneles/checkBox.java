package org.saoproject.web.client.paneles;

import java.util.List;

import org.gwtopenmaps.openlayers.client.Map;
import org.gwtopenmaps.openlayers.client.Style;
import org.gwtopenmaps.openlayers.client.layer.WFS;
import org.project.giisco.client.Mapa;
import org.project.giisco.client.Tabla;
import org.project.giisco.giu.client.BarraMapa;
import org.saoproject.web.client.GreetingService;
import org.saoproject.web.client.GreetingServiceAsync;
import org.saoproject.web.client.LayerGroup;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class checkBox {

	private WFS wfsCampania;
	GreetingServiceAsync Servicio = GWT.create(GreetingService.class);


public CheckBox crearCheckBox(final String nombreWFS, final List<LayerGroup> listaGr, final Mapa mapaBase, final Map map,
			 final BarraMapa barra,final Tabla tb2)
	{
		
		final VerticalPanel vPanel = new VerticalPanel();
		vPanel.setSpacing(4);
		final CheckBox check= new CheckBox(nombreWFS);
		final int indice =  nombreWFS.indexOf("_");
		final String codigoWFS = nombreWFS.substring(0,indice);
		final String codigoZona = nombreWFS.split("_")[1];
		check.addClickHandler(new ClickHandler() {
				
		public void onClick(ClickEvent arg0) {
	    wfsCampania= findCampanaWFS(listaGr, check.getText());
				if (check.getValue())
				{	
				Style estilo = new Style();
			    estilo.setStrokeWidth(1);
		        estilo.setFillOpacity(0.4);
		        estilo.setPointRadius(3);
		        estilo.setStrokeOpacity(0.7);
		        wfsCampania.setStyle(estilo);
					mapaBase.agregarCapasMapa(map,wfsCampania);
				}
				else
				{  mapaBase.eliminarLayer(map,wfsCampania);
				}
					
				variantPointInsertTableCampaña(codigoWFS,codigoZona,check.getValue());
				}

				private void variantPointInsertTableCampaña(String campaniaWFS,
						String nameZona, boolean value) {

					if (value){	
						
						AsyncCallback<List<String>> callback= new AsyncCallback<List<String>>()
						{
							public void onSuccess(List<String> result) {
								 tb2.AgregarFilaCampaña(result);
								}
							public void onFailure(Throwable caught) {
							      Window.alert("Error al solicitar Campaña");}
						};
						Servicio.ObtenerCampania(codigoWFS,codigoZona, callback);
								
					}
					else
						tb2.EliminarFilaCamp(campaniaWFS, nameZona);

				}
				
				private WFS findCampanaWFS(List<LayerGroup> lista, String nameCamp) {
				boolean encontrado=false;			
				int i=0;
				WFS wfsCampaniaSearch=lista.get(0).getLayer();
				while (i<lista.size() && (!encontrado))
					{
						if (listaGr.get(i).getItem().compareTo(nameCamp) ==0)
						{ 	
							wfsCampaniaSearch= lista.get(i).getLayer();
							encontrado=true;
							wfsCampaniaSearch.getFeatureById("default");
						}
						i=i+1;
					}				
			    	return wfsCampaniaSearch;
					}
    });
			
	return check;
	}
	
}
