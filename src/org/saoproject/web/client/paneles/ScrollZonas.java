package org.saoproject.web.client.paneles;


import java.util.List;

import org.gwtopenmaps.openlayers.client.Map;
import org.gwtopenmaps.openlayers.client.layer.WFS;
import org.gwtopenmaps.openlayers.client.layer.WFSParams;
import org.gwtopenmaps.openlayers.client.util.JSObject;
import org.project.giisco.client.Mapa;
import org.project.giisco.client.Tabla;
import org.project.giisco.giu.client.BarraMapa;
import org.project.giisco.historial.client.Historial;
import org.project.giisco.wfs.client.WFSManipulation;
import org.saoproject.web.client.GreetingService;
import org.saoproject.web.client.GreetingServiceAsync;
import org.saoproject.web.client.paneles.item.ItemZona;
import org.saoproject.web.shared.Direcciones;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CustomScrollPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.ScrollPanel;


public class ScrollZonas extends CustomScrollPanel 
{
	
	public String URLDIR = Direcciones.URLDIR;
	private static FlowPanel vPanel;
	GreetingServiceAsync Servicio = GWT.create(GreetingService.class);

	public ScrollZonas(String clave, final List<WFS> listWFS, final Mapa mapaBase,
	final Map map, final BarraMapa barra, final Tabla tb, final Historial historial) 
	{			
		super() ;
		vPanel = new FlowPanel();
	
		
	     
	    final String claveItem= clave;

	    AsyncCallback<List<WFSManipulation>> callback= new AsyncCallback<List<WFSManipulation>>() {
			public void onSuccess(List<WFSManipulation> result) {

		for (int indice=0; indice<result.size();indice++)
				{ 
					WFSManipulation wfsManip = result.get(indice);
					WFSParams wfsParams =new WFSParamsExt();

					if (wfsManip.getName().indexOf(claveItem)>-1)
					{	
						WFS wfsZona = wfsManip.setWfsLayer(wfsParams);
						ItemZona itemTemp=new ItemZona(wfsZona, mapaBase, map,tb,historial);
						itemTemp.addStyleName("zone-item");
						vPanel.add(itemTemp);
					}
				}
				
			}

			public void onFailure(Throwable caught) {
			      Window.alert("Error al cargar");
			}
		};
	   Servicio.obtenerWFS(URLDIR +"/geoserver/ows?service=WFS&request=GetCapabilities",Direcciones.workSpace, callback);
			
 	   add(vPanel);
		    
	}
	
	
	public class WFSParamsExt extends WFSParams 
	{

        protected WFSParamsExt(JSObject jsObject) {
                super(jsObject);
        }
        public WFSParamsExt(){
                this(JSObject.createJSObject());
        }
}
}
