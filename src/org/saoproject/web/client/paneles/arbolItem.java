package org.saoproject.web.client.paneles;

import java.util.ArrayList;
import java.util.List;

import org.gwtopenmaps.openlayers.client.Map;
import org.gwtopenmaps.openlayers.client.layer.WFS;
import org.gwtopenmaps.openlayers.client.layer.WFSParams;
import org.gwtopenmaps.openlayers.client.util.JSObject;
import org.project.giisco.client.Mapa;
import org.project.giisco.client.Tabla;
import org.project.giisco.giu.client.BarraMapa;
import org.project.giisco.historial.client.Historial;
import org.project.giisco.wfs.client.WFSManipulation;
import org.saoproject.web.client.LayerGroup;
import org.saoproject.web.client.idioma.StringsDepot;
import org.saoproject.web.client.paneles.item.ItemCampania;

import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;

public class arbolItem {

	LayerGroup layerGr;
	@SuppressWarnings("deprecation")
	public ScrollPanel DevolverTree(String descripcion, String key, int indiceGroup, List<LayerGroup> capaswfs, final Mapa mapaBase, final Map map,
		 final BarraMapa barra,final Tabla tb2, List<WFSManipulation>layersGeo,int idioma, StringsDepot tl, Historial historial ) 
	{
		
		final Tree tree = new Tree();
       
		final TreeItem outerRoot = new TreeItem(descripcion);
		WFSParams params =new WFSParamsExt();
		int indice;
		
		LayerGroup layerGr= new LayerGroup();
		String grupo;

		capaswfs= layerGr.setLayersGroup(layersGeo, key, params,indiceGroup);
		List<LayerGroup> layersWfs1  = new ArrayList<LayerGroup>();
				
		layersWfs1=layerGr.setLayersGroup(layersGeo, key, params,indiceGroup);

		        int i =0;
				while ( layersWfs1.size()>0)
				{
					layerGr= layersWfs1.get(i);
					grupo= layerGr.getGroup();
					
					TreeItem outerRoot2= new TreeItem(grupo);
					indice=0;
					
					while (indice<layersWfs1.size())
					{	
						if (layersWfs1.get(indice).getGroup().matches(grupo))
						{
							WFS campania = findCampanaWFS(capaswfs,layersWfs1.get(indice).getItem(),capaswfs);
							outerRoot2.addItem(new ItemCampania(campania, mapaBase, map, tb2, historial)); 
							layersWfs1.remove(indice);
						}
						else
							indice=indice+1;
					}outerRoot.addItem(outerRoot2);
					

				}
		tree.addItem(outerRoot);
		 ScrollPanel panel = new ScrollPanel();
   	   panel.add(tree);
   	//   panel.setHeight("160px");
		return panel;
	}
	private WFS findCampanaWFS(List<LayerGroup> lista, String nameCamp,List<LayerGroup> listaGr) {
		boolean encontrado=false;			
		int i=0;
		WFS wfsCampaniaSearch=lista.get(0).getLayer();
		while (i<lista.size() && (!encontrado))
			{
				if (listaGr.get(i).getItem().compareTo(nameCamp) ==0)
				{ 	
					wfsCampaniaSearch= lista.get(i).getLayer();
					encontrado=true;
					wfsCampaniaSearch.getFeatureById("default");
				}
				i=i+1;
			}				
	    	return wfsCampaniaSearch;
			}
	public class WFSParamsExt extends WFSParams {

        protected WFSParamsExt(JSObject jsObject) {
                super(jsObject);
        }
        public WFSParamsExt(){
                this(JSObject.createJSObject());
        }
}
	
}
