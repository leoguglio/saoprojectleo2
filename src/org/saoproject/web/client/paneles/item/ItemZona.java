package org.saoproject.web.client.paneles.item;

import java.util.List;

import org.gwtopenmaps.openlayers.client.Map;
import org.gwtopenmaps.openlayers.client.Style;
import org.gwtopenmaps.openlayers.client.layer.WFS;
import org.project.giisco.client.Mapa;
import org.project.giisco.client.Tabla;
import org.project.giisco.historial.client.Historial;
import org.saoproject.web.client.Configs;
import org.saoproject.web.client.GreetingService;
import org.saoproject.web.client.GreetingServiceAsync;
import org.saoproject.web.client.paneles.ButtonColor;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;

public class ItemZona extends ColorButtonItem {
	private CheckBox check;
	private ButtonColor btnColor;
	private WFS zona;
	private Map map;
	private Mapa mapa;
	GreetingServiceAsync Servicio = GWT.create(GreetingService.class);

	
	
	public ItemZona(WFS wfsZona, final Mapa mapaBase, final Map map, final Tabla tablaZona,final Historial historial){
		super();
		
		this.map=map;
		this.mapa=mapaBase;
		zona=wfsZona;
		check = new CheckBox(zona.getName());
		check.addStyleName(Configs.CSS_SCROLLPANELITEMTEXT);
		check.addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent arg0) {
					if (check.getValue())
					{
						Style estilo = new Style();
				        estilo.setFillColor("#"+btnColor.getColor());
				        estilo.setStrokeColor("#"+btnColor.getColor());
				        btnColor.setColor(btnColor.getColor());					     
				        estilo.setStrokeWidth(1);
				        estilo.setFillOpacity(0.4);
				        estilo.setPointRadius(10);
				        estilo.setStrokeOpacity(0.7);
				        zona.setStyle(estilo); 
				        
				        mapaBase.agregarCapasMapa(map, zona);
				        
				        //PopsUps pop = new PopsUps(map,wfsZona,mapaBase);
				        historial.addMove(map, zona,historial.CODE_ADD,check);
					}
					else	
					{	
						mapaBase.eliminarLayer(map,zona);
						historial.addMove(map, zona,historial.CODE_REM,check);

					}
					variantPointInsertTableZona(zona.getName(),check.getValue());
				}

				private void variantPointInsertTableZona(String nameZona, boolean click) {
					
					if (click)
					{
						if (! nameZona.equals("zonas"))
						{
							AsyncCallback<List<String>> callback= new AsyncCallback<List<String>>()
							{
								public void onSuccess(List<String> result) {
									tablaZona.AgregarFila(result);}
								public void onFailure(Throwable caught) {
							      Window.alert("Error al solicitar Zona");}
							};Servicio.ObtenerZona(zona.getName().substring(5),callback);
						}
					}
					else
						if (! nameZona.equals("zonas"))
							   tablaZona.EliminarFila(nameZona);
				
					
				}

			});
		
		add(check);
		btnColor = new ButtonColor(this);		
		add(btnColor);
		
	}


	public void notificarCambio(String color) {
		mapa.eliminarLayer(map,zona);
		Style estilo = new Style();
        estilo.setFillColor("#"+color);
        estilo.setStrokeColor("#"+color);
        estilo.setStrokeWidth(1);
        estilo.setFillOpacity(0.4);
        estilo.setPointRadius(10);
        estilo.setStrokeOpacity(0.7);
		zona.setStyle(estilo);
        mapa.agregarCapasMapa(map, zona);
		 
	}


	@Override
	public boolean isSelected() {
		// TODO Auto-generated method stub
		return check.getValue();
	}


	@Override
	public String getNombreCapa() {
		// TODO Auto-generated method stub
		return zona.getName();
	}
	

}
