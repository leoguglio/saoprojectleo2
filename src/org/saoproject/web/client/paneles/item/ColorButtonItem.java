package org.saoproject.web.client.paneles.item;

import org.saoproject.web.client.paneles.ButtonColor;

import com.google.gwt.user.client.ui.FlowPanel;

public abstract class ColorButtonItem extends FlowPanel
{
	protected ButtonColor btnColor;
	
	public abstract void notificarCambio(String color);
	public abstract boolean isSelected();
	public abstract String getNombreCapa();
	
}
