package org.saoproject.web.client.paneles.item;

import java.util.List;

import org.gwtopenmaps.openlayers.client.Map;
import org.gwtopenmaps.openlayers.client.Style;
import org.gwtopenmaps.openlayers.client.layer.WFS;
import org.project.giisco.client.Mapa;
import org.project.giisco.client.Tabla;
import org.project.giisco.historial.client.Historial;
import org.saoproject.web.client.GreetingService;
import org.saoproject.web.client.GreetingServiceAsync;
import org.saoproject.web.client.paneles.ButtonColor;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;


public class ItemCampania extends  ColorButtonItem
{
	private CheckBox check;	
	private WFS campania;
	private Map map;
	private Mapa mapa;
	GreetingServiceAsync Servicio = GWT.create(GreetingService.class);
	
	public ItemCampania(WFS wfsCampana, final Mapa mapaBase, final Map map, final Tabla tablaCampana,final Historial historial){
			super();
			this.map=map;
			this.mapa=mapaBase;
			campania=wfsCampana;
			
			check = new CheckBox(campania.getName());
			
			final int indice =  campania.getName().indexOf("_");
			final String codigoWFS = campania.getName().substring(0,indice);
			final String codigoZona = campania.getName().split("_")[1];
			
			check.addClickHandler(new ClickHandler() {
					
				public void onClick(ClickEvent arg0) {
			    
						if (check.getValue())
						{	
							Style estilo = new Style();
							estilo.setFillColor("#"+btnColor.getColor());
					        estilo.setStrokeColor("#"+btnColor.getColor());
					        btnColor.setColor(btnColor.getColor());
					        estilo.setStrokeWidth(1);
					        estilo.setFillOpacity(0.4);
					        estilo.setPointRadius(3);
					        estilo.setStrokeOpacity(0.7);
					        campania.setStyle(estilo);
							mapaBase.agregarCapasMapa(map,campania);
						}
						else
						{  mapaBase.eliminarLayer(map,campania);
						}
							
						variantPointInsertTableCampania(codigoWFS,codigoZona,check.getValue());
						}

						private void variantPointInsertTableCampania(String campaniaWFS,
								String nameZona, boolean value) {

							if (value){	
								
								AsyncCallback<List<String>> callback= new AsyncCallback<List<String>>()
								{
									public void onSuccess(List<String> result) {
										 tablaCampana.AgregarFilaCampaña(result);
										}
									public void onFailure(Throwable caught) {
									      Window.alert("Error al solicitar Campaña");}
								};
								Servicio.ObtenerCampania(codigoWFS,codigoZona, callback);
										
							}
							else
								tablaCampana.EliminarFilaCamp(campaniaWFS, nameZona);

						}
						
						/*private WFS findCampanaWFS(List<LayerGroup> lista, String nameCamp) {
						boolean encontrado=false;			
						int i=0;
						WFS wfsCampaniaSearch=lista.get(0).getLayer();
						while (i<lista.size() && (!encontrado))
							{
								if (listaGr.get(i).getItem().compareTo(nameCamp) ==0)
								{ 	
									wfsCampaniaSearch= lista.get(i).getLayer();
									encontrado=true;
									wfsCampaniaSearch.getFeatureById("default");
								}
								i=i+1;
							}				
					    	return wfsCampaniaSearch;
							}*/
		    });				
			
			add(check);
			btnColor = new ButtonColor(this);
			add(btnColor);
		
		
	}
	public void notificarCambio(String color) {
		mapa.eliminarLayer(map,campania);
		Style estilo = new Style();
        estilo.setFillColor("#"+color);
        estilo.setStrokeColor("#"+color);
        estilo.setStrokeWidth(1);
        estilo.setFillOpacity(0.4);
        estilo.setPointRadius(3);
        estilo.setStrokeOpacity(0.7);
		campania.setStyle(estilo);
        mapa.agregarCapasMapa(map, campania);
		 
	}
	@Override
	public boolean isSelected() {
		// TODO Auto-generated method stub
		return check.getValue();
	}
	@Override
	public String getNombreCapa() {
		// TODO Auto-generated method stub
		return campania.getName();
	}
	
	
}
