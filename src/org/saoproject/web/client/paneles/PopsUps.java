package org.saoproject.web.client.paneles;
import java.util.ArrayList;
import java.util.List;

import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.Map;
import org.gwtopenmaps.openlayers.client.Marker;
import org.gwtopenmaps.openlayers.client.Style;
import org.gwtopenmaps.openlayers.client.control.LayerSwitcher;
import org.gwtopenmaps.openlayers.client.control.ScaleLine;
import org.gwtopenmaps.openlayers.client.control.SelectFeature;
import org.gwtopenmaps.openlayers.client.control.WMSGetFeatureInfo;
import org.gwtopenmaps.openlayers.client.control.WMSGetFeatureInfoOptions;
import org.gwtopenmaps.openlayers.client.event.GetFeatureInfoListener;
import org.gwtopenmaps.openlayers.client.event.VectorFeatureSelectedListener;
import org.gwtopenmaps.openlayers.client.event.VectorFeatureUnselectedListener;
import org.gwtopenmaps.openlayers.client.feature.VectorFeature;
import org.gwtopenmaps.openlayers.client.geometry.LinearRing;
import org.gwtopenmaps.openlayers.client.geometry.Point;
import org.gwtopenmaps.openlayers.client.geometry.Polygon;
import org.gwtopenmaps.openlayers.client.layer.Vector;
import org.gwtopenmaps.openlayers.client.layer.WFS;
import org.gwtopenmaps.openlayers.client.layer.WMS;
import org.gwtopenmaps.openlayers.client.popup.FramedCloud;
import org.gwtopenmaps.openlayers.client.popup.Popup;
import org.project.giisco.client.Mapa;
import org.saoproject.web.client.GreetingService;
import org.saoproject.web.client.GreetingServiceAsync;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;


public class PopsUps {
	private GreetingServiceAsync Servicio = GWT.create(GreetingService.class);
	String codigo;
	String nombre;
	public VectorFeature poligono;
	public Polygon pol;
	public LinearRing li;
	public LonLat centro;	
	public Popup popup, popup2;
	public double area;
	public static  List<VectorFeature> poligonos = new ArrayList<VectorFeature>();
	public static Vector zonas;
	private Point[] pointList;
	Vector vectorLayer;
	
// --------------------------------------------------------------------------------------		
	public PopsUps() {
		 
	  
	}
	
		
// -------- PopsUps para opciones de mapa..........
	public PopsUps(final Map map, final Mapa mapa, final WFS wfs){
		 
	 
	        //Adds the WMSGetFeatureInfo control
	        WMSGetFeatureInfoOptions wmsGetFeatureInfoOptions = new WMSGetFeatureInfoOptions();
	        wmsGetFeatureInfoOptions.setMaxFeaturess(50);
	        wmsGetFeatureInfoOptions.setLayers(new WMS[]{mapa.wmsLayer});
	        wmsGetFeatureInfoOptions.setDrillDown(true);
	        //to request a GML string instead of HTML : wmsGetFeatureInfoOptions.setInfoFormat(GetFeatureInfoFormat.GML.toString());
	 
	        WMSGetFeatureInfo wmsGetFeatureInfo = new WMSGetFeatureInfo(
	                wmsGetFeatureInfoOptions);
	 
	        wmsGetFeatureInfo.addGetFeatureListener(new GetFeatureInfoListener() {
	            public void onGetFeatureInfo(GetFeatureInfoEvent eventObject) {
	                //if you did a wmsGetFeatureInfoOptions.setInfoFormat(GetFeatureInfoFormat.GML.toString()) you can do a VectorFeature[] features = eventObject.getFeatures(); here
	                
	                Window.alert("aa- " + eventObject.getText());
	                
	            }
	        });
	        map.addControl(wmsGetFeatureInfo);
	        wmsGetFeatureInfo.activate();
	        //Lets add some default controls to the map
	        map.addControl(new LayerSwitcher()); //+ sign in the upperright corner to display the layer switcher
	        
	        map.addControl(new ScaleLine()); //Display the scaleline
	 
	        //Center and zoom to a location
	      
	 
	      mapa.mapAsWidget().getElement().getFirstChildElement().getStyle().setZIndex(0); //force the map to fall behind popups
	    }
		
	
	
	
// -----------------------------------------------------	
	public PopsUps(final Map map, final WFS wfs, final Mapa mapa){
	    if (!wfs.getName().equals("zonas")) {
			AsyncCallback<List<String>> callback= new AsyncCallback<List<String>>() {
				public void onSuccess(List<String> result) {
					codigo = result.get(0);
					nombre = result.get(1);
					double punto1a = Double.parseDouble(result.get(2));
					double punto1b = Double.parseDouble(result.get(3));
					Point punto1 = new Point(punto1b,punto1a);
					
					double punto2a = Double.parseDouble(result.get(4));
					double punto2b = Double.parseDouble(result.get(5));
					Point punto2 = new Point(punto2b,punto2a);
					
					double punto3a = Double.parseDouble(result.get(6));
					double punto3b = Double.parseDouble(result.get(7));
					Point punto3 = new Point(punto3b,punto3a);
					centro = new LonLat(punto1b - ((punto1b - punto3b) / 2), punto1a - ((punto1a - punto3a) / 2));
					
					double punto4a = Double.parseDouble(result.get(8));
					double punto4b = Double.parseDouble(result.get(9));
					Point punto4 = new Point(punto4b,punto4a);
					
					double punto5a = Double.parseDouble(result.get(10));
					double punto5b = Double.parseDouble(result.get(11));
					Point punto5 = new Point(punto5b,punto5a);
					
					
					double punto6a = Double.parseDouble(result.get(12));
					double punto6b = Double.parseDouble(result.get(13));
					Point punto6 = new Point(punto6b,punto6a);
					
					
					if (punto6a==0){
					pointList = new Point[5];
					pointList[0] = punto1;
					pointList[1] = punto2;
					pointList[2] = punto3;
					pointList[3] = punto4;
					pointList[4] = punto5;
					}
					else
					{
					pointList = new Point[6];
					pointList[0] = punto1;
					pointList[1] = punto2;
					pointList[2] = punto3;
					pointList[3] = punto4;
					pointList[4] = punto5;
					pointList[5] = punto6;
					}
					//
					
					
					
					li = new LinearRing(pointList);
					pol = new Polygon(new LinearRing[] { li });
					area= pol.getArea();
					
					
					poligono = new VectorFeature(pol);
					
					Style estilo = new Style();
			        
			        estilo.setStrokeWidth(1);
			        estilo.setFillOpacity(0.4);
			        estilo.setPointRadius(10);
			        estilo.setStrokeOpacity(0.7);
					poligono.setStyle(estilo);
					
					poligonos.add(poligono);
					
					
					//---------------------------------
				   
					vectorLayer = mapa.vectorLayer;
					
			 
			        
			        for (int indice = 0; indice< poligonos.size(); indice++){
			        	vectorLayer.addFeature(poligonos.get(indice));
			        }
			        
			        //capture clicks on the vectorlayer
			        vectorLayer.addVectorFeatureSelectedListener(new VectorFeatureSelectedListener() {
			            public void onFeatureSelected(FeatureSelectedEvent eventObject) {
			                Window.alert("selected a vector" + eventObject.getType());
			            }
			        });
			 
			              
			 
			         
			        mapa.mapAsWidget().getElement().getFirstChildElement().getStyle().setZIndex(0);
					
					
					
				//	wfs.addFeature(poligono);
					
					
					//map.addLayer(v);
				    
				//    SelectFeature selectFeature = new SelectFeature(wfs);
				  //  map.addControl(selectFeature);
				   // selectFeature.activate();
				}
				public void onFailure(Throwable caught) {
					Window.alert("Error al solicitar Zona");}
			};
			Servicio.ObtenerZona(wfs.getName().substring(5), callback);
		}

	    
	    
	    
	    //----------------------------------
	    
	    
	    
	    
	    /*wfs.addVectorFeatureSelectedListener(new VectorFeatureSelectedListener() {
	        public void onFeatureSelected(FeatureSelectedEvent eventObject) {
	            popup = new FramedCloud("id1",centro, null, "<h3>Zona: "+ wfs.getName() +"</h3><BR/><u>Código</u>: " + codigo + "<br><u>Nombre</u>: " + nombre + "<br><i>Area</i>: " + area + " Km^2", null, true);
	            popup.setPanMapIfOutOfView(true);
	            poligono.setPopup(popup);		

	            map.addPopup(popup);
	            
	            
	            Timer t = new Timer() { 
	            	  public void run() { 
	            	    // do something after a delay 
	            		  map.removePopup(popup);
	            		  poligono.resetPopup();
	            		  wfs.redraw();
	            	  } 
	            	}; 
	            t.schedule(5000); 
	        }
	    });
	    
	    wfs.addVectorFeatureUnselectedListener(new VectorFeatureUnselectedListener(){
	        public void onFeatureUnselected(FeatureUnselectedEvent eventObject) {
	            GWT.log("onFeatureUnselected");
	            map.removePopup(popup);
	            poligono.resetPopup();
	        }
	    }); */
	    
	   // RootPanel.get().getElement().getFirstChildElement().getStyle().setZIndex(0); //force the map to fall behind popups

	}
// -------------------------------------------------------------------------- Aca iria el pop up de las CAMPAÑAS
	public void eliminarPopUp(final Map map){
		map.removePopup(popup);
        poligono.resetPopup();
	}
// --------------------------------------------------------------------------- INICIO PopUp Campaña
	public PopsUps(final Map map, final Mapa mapaBase, final Marker marker, final String codigoWFS, final String codigoZona){
		final Vector v = new Vector("");
		centro = new LonLat(marker.getLonLat().lon(), marker.getLonLat().lat());
		AsyncCallback<List<String>> callbacks = new AsyncCallback<List<String>>() {
			public void onSuccess(List<String> result) {
				//centro = new LonLat(marker.getLonLat().lon(), marker.getLonLat().lat());
				Point point = new Point(marker.getLonLat().lon(),marker.getLonLat().lat());
				poligono = new VectorFeature(point);
				v.addFeature(poligono);
			
				map.addLayer(v);
				    
				SelectFeature selectFeature = new SelectFeature(v);
				    
				map.addControl(selectFeature);
				selectFeature.activate();
			}
			public void onFailure(Throwable caught) {
				Window.alert("Error al solicitar Zona");
			}
		};
		Servicio.ObtenerCampania(codigoWFS, codigoZona, callbacks);
		
	
		v.addVectorFeatureSelectedListener(new VectorFeatureSelectedListener() {
	        public void onFeatureSelected(FeatureSelectedEvent eventObject) {
	        	//centro = new LonLat(marker.getLonLat().lon(), marker.getLonLat().lat());
	        	//String datos = "<u>Código</u>: " + codigoWFS + "<br><u>Nombre de la Zona</u>: " + codigoZona + "<br><i>Nombre campaña</i>: " + v.getName() + "<br>";
	            popup2 = new FramedCloud("Datos Campaña",centro, null, "asd", null, true);
	            popup2.setPanMapIfOutOfView(true);
	            poligono.setPopup(popup2);
	            map.addPopup(popup2);
	            
	            Timer t = new Timer() { 
	            	  public void run() { 
	            	    // do something after a delay 
	            		  map.removePopup(popup);
	            		  poligono.resetPopup();
	            		  v.redraw();
	            	  } 
	            	}; 
	            t.schedule(5000); 

	        }
	    });
	    
	    v.addVectorFeatureUnselectedListener(new VectorFeatureUnselectedListener(){
	        public void onFeatureUnselected(FeatureUnselectedEvent eventObject) {
	            GWT.log("onFeatureUnselected");
	            map.removePopup(popup2);
	            poligono.resetPopup();
	        }
	    });
	    
	    RootPanel.get().getElement().getFirstChildElement().getStyle().setZIndex(0); //force the map to fall behind popups

	}
}