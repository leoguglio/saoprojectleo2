package org.saoproject.web.client.paneles.colorpicker.dialog;

import com.google.gwt.event.shared.EventHandler;

public interface IDialogClosedHandler extends EventHandler {
	void dialogClosed(DialogClosedEvent event);
}
