package org.saoproject.web.client.paneles.colorpicker;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;/*
import com.subshell.gwt.canvas.client.colorpicker.ColorPickerDialog;
import com.subshell.gwt.canvas.client.dialog.DialogClosedEvent;
import com.subshell.gwt.canvas.client.dialog.IDialogClosedHandler;*/

public class CanvasEntryPoint implements EntryPoint {
	private TextBox colorText;

	public void onModuleLoad() {
		HorizontalPanel panel = new HorizontalPanel();
		RootPanel.get("main").add(panel);

		colorText = new TextBox();
		colorText.setEnabled(false);
		setColor("efc8a2"); // starting color
		panel.add(colorText);
		
		Button pickColorButton = new Button("Select...", new ClickHandler() {
			public void onClick(ClickEvent event) {
				pickColor();
			}
		});
		panel.add(pickColorButton);
	}

	private void pickColor() {
	/*	final ColorPickerDialog dlg = new ColorPickerDialog();
		dlg.setColor(colorText.getText());
		dlg.addDialogClosedHandler(new IDialogClosedHandler() {
			public void dialogClosed(DialogClosedEvent event) {
				if (!event.isCanceled()) {
					setColor(dlg.getColor());
				}
			}
		});
		dlg.center();*/
	}
	
	private void setColor(String color) {
		colorText.setText(color);
		colorText.getElement().getStyle().setBackgroundColor(color);
	}
}
