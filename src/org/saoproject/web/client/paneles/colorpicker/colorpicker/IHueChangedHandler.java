package org.saoproject.web.client.paneles.colorpicker.colorpicker;

import com.google.gwt.event.shared.EventHandler;

public interface IHueChangedHandler extends EventHandler {
	void hueChanged(HueChangedEvent event);
}
