package org.saoproject.web.client.paneles;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.saoproject.web.client.Configs;
import org.saoproject.web.client.Tallas.Plant;
import org.saoproject.web.client.paneles.colorpicker.colorpicker.ColorPickerDialog;
import org.saoproject.web.client.paneles.colorpicker.dialog.DialogClosedEvent;
import org.saoproject.web.client.paneles.colorpicker.dialog.IDialogClosedHandler;
import org.saoproject.web.client.paneles.item.ColorButtonItem;

import com.google.gwt.dom.client.Style.Cursor;
import com.google.gwt.editor.client.Editor.Path;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;


public class ButtonColor  extends FocusPanel  
{
	
	public String colorCapa;
	private ColorButtonItem parent;
	private com.sencha.gxt.widget.core.client.Window colorWindow;

	private HorizontalPanel panel;

	private String disabledColor ="202020";
	private ColorPickerDialog dlg;


	public ButtonColor(ColorButtonItem it)
	{	
		super();
		setSize("10px","10px");
		parent=it;
		
		int r=(int)((Math.random()*1000)%255);
		String red= Integer.toHexString(r);
		int g=(int)((Math.random()*1000)%255);
		String green= Integer.toHexString(g);
		int b=(int)((Math.random()*1000)%255);
		String blue= Integer.toHexString(b);
		colorCapa= ""+red+green+blue;
	
		Logger.getLogger("ButtonColor").log(Level.INFO, colorCapa);
		
		panel = new HorizontalPanel();
		panel.setSize("10px", "10px");
		panel.addStyleName(Configs.CSS_COLORBUTTONITEM);	
		panel.setBorderWidth(1);
		panel.getElement().getStyle().setBackgroundColor("#"+disabledColor); 		
		panel.getElement().getStyle().setCursor(Cursor.POINTER);
		
		addStyleName(Configs.CSS_COLORBUTTONITEM);
		
		add(panel);
		addClickHandler(new ClickHandler() 
		{		
			@Override
			public void onClick(ClickEvent event)
			{
				if(parent.isSelected())
					createComplex();				
			}
		});
	 }
	
	
	protected void createComplex() 
	{
		//this.setBorders("");
		//setIcon(IconsMap.INSTANCE.color());
		//setIconAlign(IconAlign.RIGHT);		
	
		if(colorWindow==null)
		{
			colorWindow = new  com.sencha.gxt.widget.core.client.Window();
			colorWindow.setHeadingText("Color de la Capa "+ parent.getNombreCapa());			
			colorWindow.setResizable(false);
		
			
			dlg = new ColorPickerDialog();
			dlg.setColor(colorCapa);
			dlg.addDialogClosedHandler(new IDialogClosedHandler() {
				public void dialogClosed(DialogClosedEvent event) {
					if (!event.isCanceled()) {
						seleccion(dlg.getColor());
						setColor(dlg.getColor());
						
						colorWindow.hide();
					}
				}
			});		
			colorWindow.setSize("235px","270px");
			colorWindow.add(dlg);
		}
		colorWindow.show();
		
		
	}

	protected void seleccion(String event) 
	{
		//this.adds
		Logger.getLogger("GIS-Log").log(Level.INFO, "ButtonStyle:"+this.getElement().getStyle());
		Logger.getLogger("GIS-Log").log(Level.INFO, colorCapa);
		
		if(colorCapa.equals(disabledColor ))
		{
			int r=(int)((Math.random()*1000)%255);
			String red= Integer.toHexString(r);
			int g=(int)((Math.random()*1000)%255);
			String green= Integer.toHexString(g);
			int b=(int)((Math.random()*1000)%255);
			String blue= Integer.toHexString(b);
			colorCapa= ""+red+green+blue;
		}
		else
			colorCapa = event;
		
 		panel.getElement().getStyle().setBackgroundColor("#"+colorCapa);
 		parent.notificarCambio(colorCapa);	
		
	}

	public String getColor(){
		return this.colorCapa;
	}

	public void setColor(String color) {
		
		panel.getElement().getStyle().setBackgroundColor("#"+color); 		
	 	
	}
	
	
	
	  /*	TextButton pickButton = new TextButton("Elegir");
	  	pickButton.addClickHandler(new ClickHandler(){
	  	
			@Override
			public void onClick(ClickEvent event) {
				colorCapa = dlg.getColor();					
			}
	  	}); 
		colorWindow.add(pickButton);
	  	
	  	colorWindow.setOnEsc(true);*/
		
	  /* final PlaceProperties properties = GWT.create(PlaceProperties.class);
	    
	    ColumnConfig<Plant, String> colorColumn = new ColumnConfig<Plant, String>(properties.color(), 220, "Color");
	    colorColumn.setColumnTextStyle(SafeStylesUtils.fromTrustedString("padding: 2px 3px;")); 
	    
	    ColorPaletteBaseAppearance appearance = GWT.create(ColorPaletteAppearance.class);
	    appearance.setColumnCount(10);
	    
	    ColorPaletteCell colorPalette = new ColorPaletteCell(appearance, Configs.DEFAULT_COLORS, Configs.DEFAULT_COLORS)
	    {
			@Override
			public boolean handlesSelection() {
			  return true;
		}};
		
		colorPalette.addSelectionHandler(new SelectionHandler<String>() 
		{
	     	@Override
	     	public void onSelection(SelectionEvent<String> event) 
	     	{
	     		seleccion(event);		
	     	} 
	    });
	    colorColumn.setCell(colorPalette);
	    
	    List<ColumnConfig<Plant, ?>> l = new ArrayList<ColumnConfig<Plant, ?>>();
	    
	    l.add(colorColumn);
	   
	    ColumnModel<Plant> cm = new ColumnModel<Plant>(l);
	 
	    final ListStore store = new ListStore<Plant>(properties.key());
	    
	    Plant p1 = new Plant();
	    List<Plant> plants = new ArrayList<Plant>();
	    p1.setColor(Configs.DEFAULT_COLORS[Random.nextInt(4)]);
	    plants.add(p1);
	    
	    store.addAll(plants);
	    final Grid<Plant> grid = new Grid<Plant>(store, cm);
	    grid.setBorders(true);
	    grid.getView().setTrackMouseOver(false);
	    grid.setHeight(150); 
	    
	    c.add(grid);  */  
	    
		//colorWindow.setWidth(220);
		//colorWindow.setHeight(170)	
	
	

}
