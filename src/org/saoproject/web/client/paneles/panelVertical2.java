package org.saoproject.web.client.paneles;

import java.util.List;

import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.Map;
import org.gwtopenmaps.openlayers.client.control.NavigationHistory;
import org.gwtopenmaps.openlayers.client.layer.Vector;
import org.gwtopenmaps.openlayers.client.layer.WFS;
import org.gwtopenmaps.openlayers.client.layer.WFSParams;
import org.gwtopenmaps.openlayers.client.util.JSObject;
import org.project.giisco.client.Mapa;
import org.project.giisco.client.Tabla;
import org.project.giisco.giu.client.BarraMapa;
import org.project.giisco.wfs.client.WFSManipulation;
import org.saoproject.web.client.GreetingService;
import org.saoproject.web.client.GreetingServiceAsync;
import org.saoproject.web.shared.Direcciones;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class panelVertical2 {
	
	// Crea la lista Item que se encuentra en el lado vertical
	private GreetingServiceAsync Servicio = GWT.create(GreetingService.class);
	public String URLDIR = Direcciones.URLDIR;
	private WFS wfsZona;
	private VerticalPanel vPanel;
    private PopsUps pop;
	private List<WFS> listaWFS_Zonas;

	public ScrollPanel createListItem(String clave, final List<WFS> listWFS, final Mapa mapaBase,
	final Map map, final BarraMapa barra, final Tabla tb, final NavigationHistory nav) {
			
		    Servicio = GWT.create(GreetingService.class);
		    vPanel = new VerticalPanel();
		    vPanel.setSpacing(8);

		    final String claveItem= clave;

			AsyncCallback<List<WFSManipulation>> callback= new AsyncCallback<List<WFSManipulation>>() {
				public void onSuccess(List<WFSManipulation> result) {
					for (int indice=0; indice<result.size();indice++) {
						final CheckBox check;
						final WFSManipulation wfs = result.get(indice);

						WFSParams wfsParams =new WFSParamsExt();

						if (wfs.getName().indexOf(claveItem)>-1) {	         				   
							final WFS wfsLayer = wfs.setWfsLayer(wfsParams);
							listWFS.add(wfsLayer);
							check= new CheckBox(wfs.getName());
//-------------------------------------------------- Evento click del check de las zonas!
							check.addClickHandler(new ClickHandler() {
								public void onClick(ClickEvent arg0) {
									int i=0; 
									boolean encontrado=false;
									wfsZona=listWFS.get(0);
									if (check.getValue()) {	
										while (i<listWFS.size() && (!encontrado)) {
											if (listWFS.get(i).getName().matches(check.getText())) {
												wfsZona= listWFS.get(i);
												encontrado=true;
												if (!barra.getMapaCreado()){
													Vector vectorLayer = new Vector("Vectorlayer");
												    //map.addLayer(vectorLayer);
													//mapaBase.agregarCapasMapa(map, vectorLayer);
// Agrega los popups a las zonas señaladas! 												    
													vectorLayer = (Vector) wfsZona;
													//pop = new PopsUps(map, vectorLayer);
													
													mapaBase.mapAsWidget().getElement().getFirstChildElement().getStyle().setZIndex(0);
												}

												if (!wfsZona.getName().equals("zonas")) {
													AsyncCallback<List<String>> callback= new AsyncCallback<List<String>>() {
														public void onSuccess(List<String> result) {
															tb.AgregarFila(result);
															Double lat1 = (Double.parseDouble(result.get(2))+Double.parseDouble(result.get(6)))/2;
															map.setCenter(new LonLat((Double.parseDouble(result.get(3))+Double.parseDouble(result.get(7)))/2,lat1),19);
														}
														public void onFailure(Throwable caught) {
															Window.alert("Error al solicitar Zona");}
													};
													Servicio.ObtenerZona(wfsZona.getName().substring(5), callback);
												}
											}
											i=i+1;
										}
									}
									else {  
										encontrado=false;
										while (i<listWFS.size() && (!encontrado)) {
											if (listWFS.get(i).getName().matches(check.getText())){
												wfsZona= listWFS.get(i);
												encontrado=true;
											    if(mapaBase.contieneLayer(map, wfsZona))
														mapaBase.eliminarLayer(map,wfsZona);
												    else{}
														//mapaAgregado.eliminarLayer(map2, wfsZona);
											}
											i=i+1;
										}
										if (wfsZona.getName().equals("zonas")){}
										else {
											tb.EliminarFila(wfsZona.getName());
										}
									}
								}
							});
							vPanel.add(check);
						}
					}
					listaWFS_Zonas = listWFS;
				}
				public void onFailure(Throwable caught) {
					Window.alert("Error al cargar");
				}
			};
			Servicio.obtenerWFS(URLDIR +"/geoserver/ows?service=WFS&request=GetCapabilities", Direcciones.workSpace, callback);
  			
			ScrollPanel panel = new ScrollPanel();
			panel.add(vPanel);
			panel.setHeight("100px");
		    return panel;
		  }
// --------------------------------------------------------------------------------------- mostrarZona
	@SuppressWarnings("deprecation")
	public void mostrarZona(String codigoZona,final Mapa mapaBase, final Map map, final Tabla tb){
		Boolean encontrado = true;
		int indice = 0;
		
		if (codigoZona.endsWith(" ")) 
			codigoZona = codigoZona.substring(0,2);
		
		while (encontrado && (indice <= vPanel.getWidgetCount())){
			final CheckBox check = (CheckBox) vPanel.getWidget(indice);
			if (check.getText().substring(5).equals(codigoZona)){
				check.setValue(true, true);	
				check.setChecked(true);												// nuevo
				encontrado=false;
			} else
				indice++;
		}
		encontrado = true;
		indice =0;
		while (encontrado && (indice <= listaWFS_Zonas.size())){
			WFS wfs = listaWFS_Zonas.get(indice);
		
			if (wfs.getName().substring(5).equals(codigoZona)){
				mapaBase.agregarCapasMapa(map,wfs);
// INICIO agregar zona a la tabla				
			    	Servicio = GWT.create(GreetingService.class);
					AsyncCallback<List<String>> callback= new AsyncCallback<List<String>>() {
						public void onSuccess(List<String> result) {
							tb.AgregarFila(result);
						}
						public void onFailure(Throwable caught) {
							Window.alert("Error al solicitar Zona");}
					};
					Servicio.ObtenerZona(wfs.getName().substring(5), callback);
// FIN agregar zona a la tabla				
// Agrega los popups a las zonas buscadas! 
				Vector vectorLayer = (Vector) wfs;
				//PopsUps pop = new PopsUps(map, vectorLayer);
				encontrado=false;
			} else
				indice++;
		}
	}	
// --------------------------------------------------------------------------------------- WFSPAramsExt	
	public class WFSParamsExt extends WFSParams {
        protected WFSParamsExt(JSObject jsObject) {
                super(jsObject);
        }
        public WFSParamsExt(){
                this(JSObject.createJSObject());
        }
	}

//-----------------------------	 
	@SuppressWarnings("deprecation")
	public void ModPanelZona (String codigoZona,final Mapa mapaBase, final Map map){
		Boolean encontrado = true;
		int indice = 0;
		codigoZona = codigoZona.substring(5, codigoZona.length());
		
		while (encontrado && (indice <= vPanel.getWidgetCount())){
			final CheckBox check = (CheckBox) vPanel.getWidget(indice);
			if (check.getText().substring(5).equals(codigoZona)){
				check.setValue(false, false);
				check.setChecked(false);												// nuevo
				encontrado=false;
			} else
				indice++;
		}
	
		
		
	}
 
	
}