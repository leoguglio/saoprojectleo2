package org.saoproject.web.client.paneles;

import org.project.giisco.client.Mapa;
import org.saoproject.web.client.Configs;

import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;


public class MapaPanel {
	private VerticalPanel dockPanel;
	
	public void mapaPanelAsWidget(Widget barraMapa, Mapa mapaBase){
		    dockPanel= new VerticalPanel();
		    dockPanel.addStyleName(Configs.CSS_DOCKPANEL);
	        dockPanel.setBorderWidth(1);
	     
	        dockPanel.add(barraMapa);
	        dockPanel.setCellHeight(barraMapa, "10px");
	      
	        Widget widgetedMap =  mapaBase.mapAsWidget();
	        dockPanel.add(widgetedMap); 
	      
	 }
	public VerticalPanel getPanelMapa(){
		return dockPanel;
	}
	
	
}
