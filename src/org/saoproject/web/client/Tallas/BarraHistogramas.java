package org.saoproject.web.client.Tallas;

import java.util.ArrayList;
import java.util.List;

import org.datos.tallas.client.DatosTallas;
import org.project.giisco.client.DynamicChart;
import org.saoproject.web.client.Configs;
import org.saoproject.web.client.GreetingService;
import org.saoproject.web.client.GreetingServiceAsync;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor.Path;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.safecss.shared.SafeStylesUtils;
import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.core.client.util.ToggleGroup;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import com.sencha.gxt.theme.base.client.colorpalette.ColorPaletteBaseAppearance;
import com.sencha.gxt.widget.core.client.ColorPaletteCell;
import com.sencha.gxt.widget.core.client.ColorPaletteCell.ColorPaletteAppearance;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.Radio;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.toolbar.SeparatorToolItem;

public class BarraHistogramas
{
	
	GreetingServiceAsync Servicio = GWT.create(GreetingService.class);

	String  c;
	String campaniaSelec="", zonaSelec="", estacionSelec="", especieSelec="";
	
	private ListBox lbcampanias;
	private ListBox lbzonas;
	private FlowPanel toolBar;	
	private ListBox lbestaciones;
	private ListBox lbespecies;
	private TextButton btnadd;
	private int colorr=0,colorg=0,colorb=0;
	private Radio radio1;
	
	private static final String[] COLORS = new String[] {"161616", "002241", "006874", "820700", 
		"bb9039", "f3f100","FF00FF","EE82EE","DA70D6","BA55D3","9370DB","8A2BE2","9400D3","9932CC","8B008B","800080",
		"4B0082","483D8B","6A5ACD","7B68EE","00008B","0000FF","1E90FF","87CEEB","B0E0E6","00CED1","48D1CC",
		"7FFFD4","66CDAA","808000","9ACD32","006400","008000","2E8B57","32CD32","00FA9A","FF0000","800000","FFFF00","FFD700"};
	

	interface PlaceProperties extends PropertyAccess<Plant>
	{	    
	    @Path("name")
	    ModelKeyProvider<Plant> key(); 
	    ValueProvider<Plant, String> color();
	}
			
	public FlowPanel iniciar(final List<DatosTallas> dTallas,
							final List<String> campanias, 
							final List<String> zonas,
							final List<String> especies, 
							final List<String> listaEstac,
							final DynamicChart line)
	{
				
		lbcampanias= new ListBox();
		campaniaSelec=campanias.get(0);
		for (int i=0; i<campanias.size();i++)
		  	lbcampanias.addItem(campanias.get(i));
		
		lbzonas = new ListBox();		
		lbestaciones = new ListBox();
		lbespecies = new ListBox();
									 
		toolBar = new FlowPanel();
		Label lblCampanias=new Label("Campaña :");
		Label lblZonas=new Label("Zona :");
		Label lblEstacion=new Label("Estación :");
		Label lblEspecies=new Label("Especies :");
		
		{
			lblCampanias.addStyleName(Configs.CSS_FLOATLEFT);
			toolBar.add(lblCampanias);			
			lbcampanias.addStyleName(Configs.CSS_FLOATLEFT);
			toolBar.add(lbcampanias);
			
			SeparatorToolItem separator=new SeparatorToolItem();
			separator.addStyleName(Configs.CSS_FLOATLEFT);
			toolBar.add(separator);
		}
		
		{
			lblZonas.addStyleName(Configs.CSS_FLOATLEFT);
			toolBar.add(lblZonas);		
			lbzonas.addStyleName(Configs.CSS_FLOATLEFT);
			toolBar.add(lbzonas);
			
			SeparatorToolItem separator=new SeparatorToolItem();
			separator.addStyleName(Configs.CSS_FLOATLEFT);
			toolBar.add(separator);
		}
		
		{
			lblEstacion.addStyleName(Configs.CSS_FLOATLEFT);
			toolBar.add(lblEstacion);
			lblCampanias.addStyleName(Configs.CSS_FLOATLEFT);
			toolBar.add(lbestaciones);
			
			SeparatorToolItem separator=new SeparatorToolItem();
			separator.addStyleName(Configs.CSS_FLOATLEFT);
			toolBar.add(separator);
		}
		
		{
			lblEspecies.addStyleName(Configs.CSS_FLOATLEFT);
			toolBar.add(lblEspecies);
			lblCampanias.addStyleName(Configs.CSS_FLOATLEFT);
			toolBar.add(lbespecies);
			
			SeparatorToolItem separator=new SeparatorToolItem();
			separator.addStyleName(Configs.CSS_FLOATLEFT);
			toolBar.add(separator); 
		}		
		toolBar.addStyleName(Configs.CSS_TOOLBARHISTOGRAMAS);
			    
		lbcampanias.addChangeHandler(new ChangeHandler() 
		{
			public void onChange(ChangeEvent event) {
				lbestaciones.clear();
				lbzonas.clear();
				lbespecies.clear();
				btnadd.setEnabled(false);
				campaniaSelec=lbcampanias.getItemText(lbcampanias.getSelectedIndex());
				ObtenerZonaTalla(lbcampanias.getItemText(lbcampanias.getSelectedIndex()));
			}
		});
							
		btnadd = new TextButton("Agregar");
		btnadd.addSelectHandler(new SelectHandler() 
		{
			@Override
			public void onSelect(SelectEvent event)
			{
				AsyncCallback<List<DatosTallas>> callback= new AsyncCallback<List<DatosTallas>>() 
				{				
					public void onSuccess(List<DatosTallas> result)
					{ 	
						line.agregarLinea(result,campaniaSelec + " " + zonaSelec+" "+ especieSelec + " ("+estacionSelec +")" ,colorr,colorg,colorb,radio1.getValue());
					} 
					
					public void onFailure(Throwable caught)
					{
					 		    Window.alert("Error al cargar");
					}
				};
				Servicio.ObtenerTallas(campaniaSelec, zonaSelec, especieSelec,estacionSelec,callback);
			}
		});
		btnadd.setEnabled(false);
		toolBar.add(btnadd);
			
		//------------------------------- barra colores ---------------------------
		
		TextButton btncolor = new TextButton("Color");
		   
	    final com.sencha.gxt.widget.core.client.Window complex = new  com.sencha.gxt.widget.core.client.Window();
	    complex.setBodyBorder(false);
	    complex.setHeadingText("Color de la Campaña");
	    complex.setWidth(220);
	    complex.setHeight(180);
	    complex.setResizable(false);
	    complex.setResize(false);
	   	   
        radio1 = new Radio();
	    radio1.setBoxLabel("Area");
	 
	    Radio radio2 = new Radio();
	    radio2.setBoxLabel("Linea");
	 
	    
	    HorizontalPanel hp = new HorizontalPanel();
	    hp.add(radio1);
	    hp.add(radio2);
	    hp.setHeight("27px");
	    hp.setWidth("100%");
	    
	    
	    ToggleGroup toggle = new ToggleGroup();
	    toggle.add(radio1);
	    toggle.add(radio2);
	    toggle.addValueChangeHandler(new ValueChangeHandler<HasValue<Boolean>>()
	    {		 
		      public void onValueChange(ValueChangeEvent<HasValue<Boolean>> event) 
		      {		
		      }
	    });
	
        final VerticalLayoutContainer c = new VerticalLayoutContainer();
	
        complex.add(c);
		complex.setOnEsc(true);
		
	    final PlaceProperties properties = GWT.create(PlaceProperties.class);
	    
	    ColumnConfig<Plant, String> colorColumn = new ColumnConfig<Plant, String>(properties.color(), 220, "Color");
	    colorColumn.setColumnTextStyle(SafeStylesUtils.fromTrustedString("padding: 2px 3px;")); 
	    
	    ColorPaletteBaseAppearance appearance = GWT.create(ColorPaletteAppearance.class);
	    appearance.setColumnCount(10);
	    ColorPaletteCell colorPalette = new ColorPaletteCell(appearance, COLORS, COLORS) {
	        @Override
	        public boolean handlesSelection() {
	          return true;
	        }};
	   colorPalette.addSelectionHandler(new SelectionHandler<String>() {
	     	@Override
		public void onSelection(SelectionEvent<String> event) {
				colorr = parseInt(event.getSelectedItem().substring(0,2),16);
				colorg = parseInt(event.getSelectedItem().substring(2,4),16);
	    	  	colorb = parseInt(event.getSelectedItem().substring(4,6),16);
	     	} });
	      colorColumn.setCell(colorPalette);
	    
	    List<ColumnConfig<Plant, ?>> l = new ArrayList<ColumnConfig<Plant, ?>>();
	    
	    l.add(colorColumn);
	   
	    ColumnModel<Plant> cm = new ColumnModel<Plant>(l);
	 
	    final ListStore store = new ListStore<Plant>(properties.key());
	    
	    Plant p1 = new Plant();
	    List<Plant> plants = new ArrayList<Plant>();
	    p1.setColor(COLORS[Random.nextInt(4)]);
	    plants.add(p1);
	    
	    store.addAll(plants);
	    final Grid<Plant> grid = new Grid<Plant>(store, cm);
	    grid.setBorders(true);
	    grid.getView().setTrackMouseOver(false);
	    grid.setHeight(150);
	    
	    
	    c.add(hp);
	    c.add(grid);
	    
	   	//.---------------------------------	   
	  
		SelectHandler sh3 = new SelectHandler() {			   	 
		@Override
		public void onSelect(SelectEvent event) {   

					complex.setWidth(220);
					complex.setHeight(170);
					complex.show();
					
					}};

		btncolor.addSelectHandler(sh3);
		toolBar.add(btncolor);

		
					
		return toolBar;
}

	private void ObtenerZonaTalla(final String campania){
		
		AsyncCallback<List<String>> callback= new AsyncCallback<List<String>>() {
			public void onSuccess(List<String> result) { 	
				if (result.size()==0)
	  			{
	  				Window.alert("No Existen datos de esa Campaña");
	  			}
	  			else
	  			{
	  			    if (lbzonas.getItemCount()==0){ 
	  			    	 lbzonas.addItem(" ");
		  				 for (int i=0; i<result.size();i++)
			  				lbzonas.addItem(result.get(i));
			  				lbzonas.addChangeHandler(new ChangeHandler() {
			  					
								
							public void onChange(ChangeEvent event) {
								lbestaciones.clear();
				  				lbespecies.clear();
				  				btnadd.setEnabled(false);
				  				zonaSelec=lbzonas.getItemText(lbzonas.getSelectedIndex());
								ObtenerEstacionTalla(campania,lbzonas.getItemText(lbzonas.getSelectedIndex()));
							}});
		  				}
		  				//toolBar.syncSize();
	  			}	
	  				
				    
			}
			public void onFailure(Throwable caught) {
	  		    Window.alert("Error al solicitar Zona");
	  		}
		};Servicio.ObtenerZonasCampania(campania, callback);
	}
	private void ObtenerEstacionTalla(final String campania,final String zona) {
		AsyncCallback<List<String>> callback= new AsyncCallback<List<String>>() {
			public void onSuccess(List<String> result) { 	
				if  (result.size()!=0)
				{		
	  				 if (lbestaciones.getItemCount() ==0){
					 lbestaciones.addItem(" ");
	  				 for (int i=0; i<result.size();i++)
	  					lbestaciones.addItem(result.get(i));
	  				 	lbestaciones.addChangeHandler(new ChangeHandler() {
		
					
						public void onChange(ChangeEvent event) {
							lbespecies.clear();
							btnadd.setEnabled(false);
							estacionSelec=lbestaciones.getItemText(lbestaciones.getSelectedIndex());
							ObtenerEspecieTalla(campania,zona,lbestaciones.getItemText(lbestaciones.getSelectedIndex()));

						}});
		  				
	  				 }
	  				//toolBar.syncSize();
	  			}	
	  				
				    
			}
			public void onFailure(Throwable caught) {
	  		    Window.alert("Error al solicitar Zona");
	  		}
		};Servicio.ObtenerEstacionesCampania(campania, zona, callback);
	  		 
		
		
		
	}
private void ObtenerEspecieTalla(final String campania,final String zona,final String estacion) {
		AsyncCallback<List<String>> callback= new AsyncCallback<List<String>>() {
			public void onSuccess(List<String> result) { 	
  				

				if (result.size()!=0)
	  			{	if (lbespecies.getItemCount()==0)
	  				  { 
						lbespecies.addItem(" ");
		  				 for (int i=0; i<result.size();i++)
		  	  				lbespecies.addItem(result.get(i));
		  				 	lbespecies.addChangeHandler(new ChangeHandler() {
		  						
		  						
							public void onChange(ChangeEvent event) {
								if (!(lbespecies.getItemText(lbespecies.getSelectedIndex()).equals(""))){
									especieSelec=lbespecies.getItemText(lbespecies.getSelectedIndex());
									btnadd.setEnabled(true);
								}
								else{
									btnadd.setEnabled(false);
								}
								
							}});
	  				 	}
		  				//toolBar.syncSize();
	  			}	
	  				
				    
			}
			public void onFailure(Throwable caught) {
	  		    Window.alert("Error al solicitar Zona");
	  		}
		};Servicio.ObtenerEspeciesCampania(campania, zona,estacion, callback);
	  		 
	}

private int parseInt(String substring, int i) {
	return parseIntHEX(substring.substring(0,1))*1 + parseIntHEX(substring.substring(0,1))*i ;
	
}

private int parseIntHEX(String substring)
{
	if (substring.equals("a"))return 10;
	if (substring.equals("A"))return 10;
	if (substring.equals("b"))return 11;
	if (substring.equals("B"))return 11;
	if (substring.equals("c"))return 12;
	if (substring.equals("C"))return 12;
	if (substring.equals("d"))return 13;
	if (substring.equals("D"))return 13;
	if (substring.equals("e"))return 14;
	if (substring.equals("E"))return 14;
	if (substring.equals("f"))return 15;
	if (substring.equals("F"))return 15;
	else return Integer.parseInt(substring);
}
}

