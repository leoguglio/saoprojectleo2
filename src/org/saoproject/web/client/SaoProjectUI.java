package org.saoproject.web.client;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.datos.tallas.client.DatosTallas;
import org.gwtopenmaps.openlayers.client.Icon;
import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.Map;
import org.gwtopenmaps.openlayers.client.Pixel;
import org.gwtopenmaps.openlayers.client.Size;
import org.gwtopenmaps.openlayers.client.control.Control;
import org.gwtopenmaps.openlayers.client.control.LayerSwitcher;
import org.gwtopenmaps.openlayers.client.control.MousePosition;
import org.gwtopenmaps.openlayers.client.control.NavToolbar;
import org.gwtopenmaps.openlayers.client.control.Navigation;
import org.gwtopenmaps.openlayers.client.control.NavigationHistory;
import org.gwtopenmaps.openlayers.client.control.OverviewMap;
import org.gwtopenmaps.openlayers.client.control.PanZoomBar;
import org.gwtopenmaps.openlayers.client.control.SelectFeature;
import org.gwtopenmaps.openlayers.client.control.SelectFeatureOptions;
import org.gwtopenmaps.openlayers.client.event.MapZoomListener;
import org.gwtopenmaps.openlayers.client.feature.VectorFeature;
import org.gwtopenmaps.openlayers.client.format.KML;
import org.gwtopenmaps.openlayers.client.layer.Vector;
import org.gwtopenmaps.openlayers.client.layer.WFS;
import org.gwtopenmaps.openlayers.client.popup.AnchoredBubble;
import org.gwtopenmaps.openlayers.client.popup.Popup;
import org.project.giisco.client.DynamicChart;
import org.project.giisco.client.GenericChart;
import org.project.giisco.client.Mapa;
import org.project.giisco.client.Tabla;
import org.project.giisco.giu.client.BarraMapa;
import org.project.giisco.historial.client.Historial;
import org.project.giisco.wfs.client.WFSManipulation;
import org.saoproject.web.client.Tallas.BarraHistogramas;
import org.saoproject.web.client.Tallas.Plant;
import org.saoproject.web.client.callbacks.ParallelCallback;
import org.saoproject.web.client.callbacks.ParentCallback;
import org.saoproject.web.client.icons.IconsMap;
import org.saoproject.web.client.idioma.StringsDepot;
import org.saoproject.web.client.loaders.form.FormViejos.FormCSV;
import org.saoproject.web.client.loaders.form.FormViejos.FormCampania3;
import org.saoproject.web.client.loaders.form.FormViejos.FormCampania4;
import org.saoproject.web.client.loaders.form.FormViejos.FormZona;
import org.saoproject.web.client.loaders.form.formularios.FormCampania;
import org.saoproject.web.client.loaders.form.formularios.FormDatosBio;
import org.saoproject.web.client.loaders.form.formularios.FormEspecie;
import org.saoproject.web.client.loaders.form.formularios.FormEstacion;
import org.saoproject.web.client.loaders.form.formularios.FormTalla;
import org.saoproject.web.client.loaders.form.formularios.FormZonas;
import org.saoproject.web.client.loaders.ui.CsvLoaderPanel;
import org.saoproject.web.client.paneles.MapaPanel;
import org.saoproject.web.client.paneles.ScrollCampania;
import org.saoproject.web.client.paneles.ScrollZonas;
import org.saoproject.web.shared.Direcciones;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor.Path;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.FramedPanel;
import com.sencha.gxt.widget.core.client.TabItemConfig;
import com.sencha.gxt.widget.core.client.TabPanel;
import com.sencha.gxt.widget.core.client.box.ProgressMessageBox;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer.VerticalLayoutData;
import com.sencha.gxt.widget.core.client.info.Info;

public class SaoProjectUI implements EntryPoint
{
	interface PlaceProperties extends PropertyAccess<Plant>
	{
	    ValueProvider<Plant, String> color();
	    @Path("name")
	    ModelKeyProvider<Plant> key();
	}	
 	
//Protected Fields
	protected  static Object retorno = null;
	
//Public Fields
	public GreetingServiceAsync greetService = GWT.create(GreetingService.class);
	public Tabla tb = new Tabla();
	public Widget tablaZona = tb.TablaZonaAsWidget();
	public Tabla tb2 = new Tabla();		
	public Widget tablaZona2 = tb2.tablaCampaniaAsWidget();
	
	public static final int CODE_LIST_WFS = 0;
	public static final int CODE_LIST_STRING = 1;	
	public static final int CODE_LIST_TALLAS = 2;	
	
	public static Mapa mapaBase;	
	public Map map;
	
    public static MapaPanel MAPA_Panel;
    public static final Double MAPA_longitud=-63.99, MAPA_latitud=-41.15;      
    public static int MAPA_zoom = 7;
    public NavigationHistory historialDeNavegacion = new NavigationHistory();
	public static Logger logger = Logger.getLogger(Configs.DEBUG_MAIN_LOGGER_NAME);
    public Historial historial = new Historial();

//Private Fields
	//Client screen size containers
	private int clientHeight;
	private int clientWidth;	
	private int idioma = -1;  	  
	private String golfo = "Name";   
	private List<WFS> listWFS= new ArrayList<WFS>();
    private List<LayerGroup> estaciones = new ArrayList<LayerGroup>();    
    private List<WFSManipulation> layersGeo= new ArrayList<WFSManipulation>();    
    private Vector vectorLayer = new Vector("Vector layer");  
    private StringsDepot stringsDepot;

    //Contenedores
    private TabPanel contentPanelCapas = new TabPanel();
	private VerticalLayoutContainer dockPanelHist;	
	private FramedPanel globalFrame;
	private FlowPanel flowHeaderContainer;
	private FlowPanel flowBodyContainer;
	private FlowPanel flowLeftBarContainer;
	private FlowPanel flowMapContainer;
	private static VerticalPanel exportableContainer;
	private TabPanel tabDatosTabulados;
	private TabPanel mainTabPanel;	
	private TabPanel tabTablas;
	private ScrollZonas scrZonas;
    private Popup popup;
    private BorderLayoutContainer con;   
    private BarraMapa barra;
    private FormCampania formCamp;
    private FormZona formZona;
    private FormEstacion formEstacion;
    private FormEspecie formEspecie;
    private FormDatosBio formDatosBio;
    private FormTalla formTalla;
    private FormCampania formCampania;

	private static PanZoomBar toolBarZoomPanning;
    
//Public Interface	
    
    /**
	 *  JBoss Entry point method
	 */
	public void onModuleLoad() 
	{
	    //Logueamos inicio del sistema		
	    SaoProjectUI.logger.log(Level.SEVERE, "Init System");
	    
	    //Creamos los servlets para conectar con los EJB
		GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
    	ServiceDefTarget endpoint = (ServiceDefTarget) greetingService;
		endpoint.setServiceEntryPoint(GWT.getModuleBaseURL() + "greet");
		
		//Iniciamos el deposito de strings para multilenguaje
		stringsDepot = new StringsDepot();		
		
		//Obtenemos el ancho y alto de la pantalla menos un 10% para ajustarse
		//a posibles barras de navegacion extendidas que consumen la pantalla
		//por ejemplo la barra de favoritos de chrome
		clientHeight=(int)((Window.getClientHeight()));
		clientWidth=(int)((Window.getClientWidth()*0.9));
	
		//Utilizamos el componente mapa para crear un nuevo mapa base
		//El constructor solo inicializa, luego hay q hacer getInstance()
		mapaBase = new Mapa(MAPA_longitud, 
				MAPA_latitud,
				MAPA_zoom,
				golfo, 
				"openstreetmap",
				"http://maps.opengeo.org/geowebcache/service/wms",
				true,
				"100%",
				""+(int)(clientHeight)+"px");	
		
		//Obetenemos una instancia del mapa seteado y seteamos parametros iniciales
		map= mapaBase.getMapInstance();		
		map.zoomTo(0);	
		
		//DEBUG: Logeuamos los valores de escala y zoom
		map.addMapZoomListener(new MapZoomListener()
		{

			@Override
			public void onMapZoom(MapZoomEvent eventObject) 
			{
				 SaoProjectUI.logger.log(Level.INFO, "Map Scale:"+ map.getScale());
				 SaoProjectUI.logger.log(Level.INFO, "Map Zoom:"+map.getZoom());
			}			
		});		
		
		//Creamos la barra de panning y zoom y la agregamos al mapa
		toolBarZoomPanning=new PanZoomBar();
		Control[] controles =  new Control[] {historialDeNavegacion, toolBarZoomPanning,new MousePosition(),new NavToolbar(),new LayerSwitcher()};
		historialDeNavegacion.activate();
		mapaBase.agregarControlesMapa(map, controles);
		map.addControl(new OverviewMap());
		
		//-------ESTO SE PUEDE PONER EN CADA ITERACION DE LA BARRA DE CARGA
		seleccionIdioma();
	    //barraDeCarga();
			logger.log(Level.WARNING,"After selecIdioma");
		initMapa();
			logger.log(Level.WARNING,"After initMapa();");
		cargarContenedores();	    
			logger.log(Level.WARNING,"After cargarContenedores();");
        comunicarConBeans();
        	logger.log(Level.WARNING,"After comunicarConBeans();");
        
        //Agregamos un handler que re-estructure el tamaño de loscontenedores
        //al cambiar el tamaño de la pantalla 
        Window.addResizeHandler(new ResizeHandler()
        {
        	//Usamos un timer para no volver a renderizar por cada evento resize
        	//Sería muy costoso
        	  Timer resizeTimer = new Timer() 
        	  {  
        	    @Override
        	    public void run() {
        	      recalcularTamanios();
        	    }
        	  };
        	  
        	  @Override
        	  public void onResize(ResizeEvent event)
        	  {
        	    resizeTimer.cancel();
        	    resizeTimer.schedule(250);
        	  }
        });
        
        		
	}   
    
	
	public void comunicarConBeans() 
	{   	
		ParallelCallback<List<String>> Callback0 = new ParallelCallback<List<String>>();  
		ParallelCallback<List<String>> Callback1 = new ParallelCallback<List<String>>();
		ParallelCallback<List<String>> Callback2 = new ParallelCallback<List<String>>();
		ParallelCallback<List<String>> Callback3 = new ParallelCallback<List<String>>();
		ParallelCallback<List<DatosTallas>> Callback4 = new ParallelCallback<List<DatosTallas>>();
		
		//final ParentCallback parent = new ParentCallback(Callback0, Callback1) {
		ParentCallback parent = new ParentCallback
					(Callback0,
					 Callback1,
					 Callback2,
					 Callback3,
					 Callback4)
		{  
			public void handleSuccess() 
			{  
				cargarDatos((List<DatosTallas>)getCallbackData(4),
						(List<String>)getCallbackData(0),
						(List<String>)getCallbackData(1),
						(List<String>)getCallbackData(2), 
						(List<String>)getCallbackData(3));
				//cargarEstaciones2((List<String>)getCallbackData(0),(List<String>) getCallbackData(1));
			}
		};  		
		greetService.ObtenerZonas(Callback1);	
		greetService.ObtenerCampanias(Callback0);//---
		greetService.ObtenerEspecies(Callback2);
		greetService.ObtenerEstaciones(Callback3);	
		greetService.ObtenerTallas("BonIII-98 ", "ES ", "Op","P4 ", Callback4);
	}
	
	public void cargarDatos(
				 List<DatosTallas> dTallas, 
				 List<String> campanias, 
				 List<String> zonas, 
				 List<String> especies,
				 List<String>listaEstac)
	 {
		        	
		DynamicChart line = new DynamicChart();
		BarraHistogramas BH = new BarraHistogramas();
		FlowPanel toolBar = BH.iniciar(dTallas,campanias, zonas, especies,listaEstac, line); 
		
		dockPanelHist = new VerticalLayoutContainer();
	    dockPanelHist.setWidth("100%");	
		dockPanelHist.add(toolBar,new VerticalLayoutData(((int)this.clientWidth*0.8), 30, new Margins(0))); 
		dockPanelHist.add(line.asWidgetOTRO(dTallas,campanias,zonas,especies,listaEstac));
		dockPanelHist.setSize(((int)this.clientWidth*0.8)+"px", ((int)this.clientHeight)+"px");
		
		tabDatosTabulados = new TabPanel();
	 	//tabDatosTabulados.addStyleName(Configs.CSS_MAPCONTAINER);
	    tabDatosTabulados.add(tablaZona, stringsDepot.Zonas.get(idioma));
	    tabDatosTabulados.add(tablaZona2, stringsDepot.Campanas.get(idioma));
	    
	    tabTablas = new TabPanel();
	   
	    tabDatosTabulados.setSize("100%", "100%");
	    mainTabPanel = new TabPanel();	 	    
	    {
		    TabItemConfig mapTab = new TabItemConfig(stringsDepot.Mapa.get(idioma));
		  	mapTab.setIcon(IconsMap.INSTANCE.agregarMapa());
		  	MAPA_Panel.getPanelMapa().addStyleName(Configs.CSS_TABMAPA);
		    mainTabPanel.add( MAPA_Panel.getPanelMapa(),mapTab);
		    exportableContainer=MAPA_Panel.getPanelMapa();
		}
	    
	    {
		    TabItemConfig tabCfgTablas = new TabItemConfig(stringsDepot.Tabla.get(idioma));
		  	tabCfgTablas.setIcon(IconsMap.INSTANCE.tabla());
		  	tabDatosTabulados.addStyleName(Configs.CSS_TABTABLAS);
		  	tabDatosTabulados.setSize(((int)this.clientWidth*0.8)+"px", ((int)this.clientHeight)+"px");
			mainTabPanel.add(tabDatosTabulados,tabCfgTablas);
		}
	    
	    {
		    TabItemConfig histogramTabs = new TabItemConfig(stringsDepot.Histogramas.get(idioma));
		  	histogramTabs.setIcon(IconsMap.INSTANCE.chart());
		  	dockPanelHist.addStyleName(Configs.CSS_TABHISTOGRAMAS);
		    mainTabPanel.add(dockPanelHist,histogramTabs);
		}
	    
	    {
	    	TabItemConfig cargaArchivo = new TabItemConfig("Cargar");
	    	cargaArchivo.setIcon(IconsMap.INSTANCE.carga());
	        TabPanel tabCargarDatos = new TabPanel();
	        tabCargarDatos.add(new FormCampania().asWidget(),"Campañas");
	        tabCargarDatos.add(new FormEstacion().asWidget(),"Estaciones");
	        tabCargarDatos.add(new FormZonas().asWidget(),"Zonas");
	        tabCargarDatos.add(new FormDatosBio().asWidget(),"Datos biológicos");
	        tabCargarDatos.add(new FormEspecie().asWidget(),"Especies");
	        tabCargarDatos.add(new FormTalla().asWidget(),"Tallas");
	        mainTabPanel.add(tabCargarDatos,cargaArchivo);
	    	
	    }
	    GenericChart bm = new GenericChart();
	  
	    VerticalLayoutContainer dockPanelHist2 = new VerticalLayoutContainer();
	    dockPanelHist2.setWidth("100%");
	    dockPanelHist2.add(bm.asWidget());
	    
	    mainTabPanel.addStyleName(Configs.CSS_FOLDERPPAL);
        flowMapContainer.add(mainTabPanel);
        			
	    Banner banner = new Banner("G.I.S. - Instituto Pesquera y Marina Almirante Storni", "./img/ibmp-small.png");
	    banner.addStyleName(Configs.CSS_BANNER);
	    banner.addStyleName(Configs.CSS_TOPCONTAINER);
	    flowHeaderContainer.add(banner);    
	    	  	      
        RootPanel.get().add(globalFrame);
	    	
	}

	 
  
	public void seleccionIdioma()
	{
		idioma =0;
	}

//Private Methods
    private  void barraDeCarga()
	{		
		final ProgressMessageBox box =
			new ProgressMessageBox			
					(stringsDepot.msj1.get(idioma),
					 stringsDepot.msj2.get(idioma));
		
	    box.setProgressText("Init...");
	    box.show();
	
	    final Timer t = new Timer() 
	    {
	      float i;
	      public void run() 
	      {
	        box.updateProgress(i / 100,  "{0}% Completado");
	        i += 5;
	        if (i > 100) {
	          cancel();
	          Info.display("Mensaje", "Las capas fueron cargadas");
	          box.hide();
	        }
	      }
	    };
	    t.scheduleRepeating(200);
	   
	}
	 
	  
	private void cargarContenedores()
	{
		globalFrame= new FramedPanel();
		globalFrame.setHeadingText("G.I.S. - Instituto Pesquera y Marina Almirante Storni");
		globalFrame.addStyleName(Configs.CSS_GLOBALCONTAINER);
		globalFrame.setWidth(clientWidth+"px");
		globalFrame.setHeight(clientHeight+"px");
		//GlobalContainer, agregar Header y Body
		{
		   
			flowBodyContainer= new FlowPanel();
			flowBodyContainer.addStyleName(Configs.CSS_BODYCONTAINER);
			globalFrame.add(flowBodyContainer);
			
			
			//BODY Container, agregar mapa y barra
			{
				flowLeftBarContainer= new FlowPanel();
				flowLeftBarContainer.addStyleName(Configs.CSS_LEFTBARCONTAINER);
				flowHeaderContainer= new FlowPanel();
				flowHeaderContainer.addStyleName(Configs.CSS_TOPCONTAINER);				
				flowLeftBarContainer.add(flowHeaderContainer);				
				flowBodyContainer.add(flowLeftBarContainer);					
				flowMapContainer= new FlowPanel();
				flowMapContainer.addStyleName(Configs.CSS_MAPCONTAINER);
				flowBodyContainer.add(flowMapContainer);					
			}
		}		
		logger.log(Level.INFO,"Divs Created");
		
	    TabItemConfig configZonas = new TabItemConfig("Zonas");
	  	configZonas.setIcon(IconsMap.INSTANCE.agregarMapa());
	  	logger.log(Level.INFO,"After AgregarMapa");
	  	
	  	scrZonas=new ScrollZonas("zona",listWFS,mapaBase,map,barra,tb,historial);
	  	scrZonas.addStyleName(Configs.CSS_SCROLL_CAPAS);
	  	scrZonas.setHeight((int)(clientHeight)+"px");
	  	scrZonas.removeHorizontalScrollbar();
	  	contentPanelCapas.add( scrZonas,configZonas);
	  		  	
	      AsyncCallback<List<WFSManipulation>> callback1= 
	    	new AsyncCallback<List<WFSManipulation>>() 
	    	{

	    		public void onFailure(Throwable caught) {
				 //    Window.alert("Error al cargar Capas");
				}
	
				public void onSuccess(List<WFSManipulation> result) 
	    		{			    
		    		layersGeo = result;
				  	crearScrollCampania();					    
	    		}
		};
		greetService.obtenerWFS(Direcciones.URLDIR+"/geoserver/ows?service=WFS&request=GetCapabilities",Direcciones.workSpace, callback1);
		logger.log(Level.INFO,"WFS Obtained");
	}
	
	protected void crearScrollCampania() 
	{
		 ScrollCampania tree= 
		    	new ScrollCampania("Campaña",
		    			stringsDepot.Estacion.get(idioma),
		    			0,			    			
		    			estaciones,
		    			mapaBase,
		    			map,
		    			barra,
		    			tb2,
		    			layersGeo,
		    			idioma,
		    			stringsDepot,
		    			historial);	
		    
		    ContentPanel contPanelCamp = new ContentPanel();
		    contPanelCamp.setAnimCollapse(false);
		    contPanelCamp.setHeadingText(stringsDepot.Campanas.get(idioma));

		    TabItemConfig configCampania = new TabItemConfig("Campañas");
		  	configCampania.setIcon(IconsMap.INSTANCE.agregarMapa());
		  	contentPanelCapas.add( tree,configCampania);		 
			contentPanelCapas.addStyleName(Configs.CSS_SCROLL_CAPAS);
		    
		    flowLeftBarContainer.add(contentPanelCapas);		      
		
	}

	/**
	 * Inicializa el contenedor del mapa y los controles para manejarlo
	 */
	private void initMapa()
	{
	    con = new BorderLayoutContainer();
	    con.setBorders(true);
	        
		SelectFeatureOptions selectFeatureOptions = new SelectFeatureOptions();
		selectFeatureOptions.onSelect(new SelectFeature.SelectFeatureListener()
		{
	
			public void onFeatureSelected(VectorFeature vectorFeature) 
			{
					popup = new AnchoredBubble("info",
						new LonLat(0,0),
						new Size(100,100),
						"<p>" + new KML().write(vectorFeature) + "</p>",
						new Icon("", new Size(0,0), new Pixel(0,0)),
						true
					);
					mapaBase.agregarPopup(map,popup);
				
				}
		});
	
		SelectFeature selectFeature = new SelectFeature(vectorLayer, selectFeatureOptions);
		
		mapaBase.agregarControlMapa(map, selectFeature);
	    mapaBase.agregarControlesMapa(map, new Control[] { new Navigation()});
	    
	    MAPA_Panel = new MapaPanel();
	    barra = new BarraMapa(map, mapaBase,idioma,historialDeNavegacion,historial);
		        
	}


   protected void recalcularTamanios() 
   {
	   logger.log(Level.WARNING,"Inside recalcTamanios();");
       
	   try
	   {
			clientHeight=(int)((Window.getClientHeight()));
			clientWidth=(int)((Window.getClientWidth()));
	   }
	   catch(Exception e)
	   {
		   logger.log(Level.INFO,"Error");		     
			
	   }
		int globalHeight = (int)((clientHeight));
		int globalWidth = (int)((clientWidth*0.9));
		
		//1% consumido por decoradores laterales
		int insideGlobalFrameWidth= (int)(globalWidth-(globalWidth*0.01));		
		//5% consumido por la barra de titulo
		int insideGlobalFrameHeight= (int)(globalHeight-(globalHeight*0.05));
		
		logger.log(Level.INFO,"Resizing global frame");		     
		//90% del tamaño del browser
		globalFrame.setWidth(globalWidth+"px");
		globalFrame.setHeight(globalHeight+"px");		
		
		logger.log(Level.INFO,"Resizing LeftBar");
		//Contenedor del menu izquierdo: 90% del alto de la pantalla 20% del ancho
		//flowLeftBarContainer.setSize("20%", "90%");
		
		logger.log(Level.INFO,"Resizing banner frame");
		//Contenedor del header del menu izquierdo 10% para el banner 20% del ancho
		//flowHeaderContainer.setSize("20%", "10%");
		
		logger.log(Level.INFO,"Resizing map frame");
		//Contenedor del mapa con su barra de herramientas 100% de alto, 80% del ancho
		flowMapContainer.setSize("80%", "100%");
		
		logger.log(Level.INFO,"Resizing other containers");
		MAPA_Panel.getPanelMapa().setSize(((int)this.clientWidth*0.8)+"px", ((int)this.clientHeight)+"px");
		tabDatosTabulados.setSize(((int)this.clientWidth*0.8)+"px", ((int)this.clientHeight)+"px");
		dockPanelHist.setSize(((int)this.clientWidth*0.8)+"px", ((int)this.clientHeight)+"px");
		scrZonas.setHeight((int)(clientHeight)+"px");	
		
		 logger.log(Level.WARNING,"Exiting recalcTamanios();");
	        
   }


public static String  getMapElemet() 
{	
	/*Mapa temporal= new Mapa(mapaBase);
	Map temp=temporal.getMapInstance();
	temp.removeControl(temp.getControlsByClass(NavToolbar.class.toString()));
	exportableContainer= new VerticalPanel();
	exportableContainer.add( temporal.mapAsWidget());*/
	mapaBase.getMap().removeControl(toolBarZoomPanning);
	String elemento=mapaBase.mapAsWidget().getElement().getInnerHTML();
	//mapaBase.getMap().addControl(toolBarZoomPanning);
	toolBarZoomPanning=new PanZoomBar();
	mapaBase.getMap().addControl(toolBarZoomPanning);
	return  elemento;
}
 
	
}