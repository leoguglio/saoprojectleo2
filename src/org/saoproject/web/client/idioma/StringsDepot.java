package org.saoproject.web.client.idioma;

import java.util.Arrays;
import java.util.List;


public class StringsDepot {
	

	
	public List<String> Campanas;
	public List<String> ListCapas;
	public List<String> Zonas;
	public List<String> Mapa;
	public List<String> Tabla;
	public List<String> Histogramas;
	public List<String> CargarDatos;
	public List<String> msj1;
	public List<String> msj2;
	public List<String> Campana;
	public List<String> estacion;




	public StringsDepot()
	{	
	
		Campanas= Arrays.asList(new String[]{"Campañas","Census"});
		ListCapas= Arrays.asList(new String[]{"Listado de Capas","Layers"});
		Zonas= Arrays.asList(new String[]{"Zonas","Zone"});
		Mapa=Arrays.asList(new String[]{"Mapa" , "Map"}); 
		Tabla=Arrays.asList(new String[]{"Tablas" , "Grids"}); 
		Histogramas= Arrays.asList(new String[]{"Histogramas","Charts"});
		CargarDatos = Arrays.asList(new String[]{"Cargar Datos","Load Data"});
		msj1 = Arrays.asList(new String[]{"Por Favor, espere", "Please, wait"});
		msj2= Arrays.asList(new String[]{"Cargando capas...","Loading Layers..."});
		Campana = Arrays.asList(new String[]{ "Campaña","Census"});
		estacion= Arrays.asList(new String[]{"estacion","station"});
	
	}
}
