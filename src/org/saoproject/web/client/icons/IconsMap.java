package org.saoproject.web.client.icons;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface IconsMap extends ClientBundle {
	public IconsMap INSTANCE = GWT.create(IconsMap.class);
 
	@Source("color.png")
    ImageResource color();
	
	@Source("pdfexport.png")
    ImageResource pdfExport();
	
	@Source("bandera_inglesa.png")
    ImageResource bandInglesa();
	
	@Source("bandera_espana.png")
    ImageResource bandEspana();
	
	@Source("carga.png")
    ImageResource carga();
	
	@Source("bar_chart.png")
    ImageResource barChart();
	
	@Source("scale.png")
    ImageResource scale();
	
	@Source("chart.png")
    ImageResource chart();
	
	@Source("Table.png")
    ImageResource tabla();
	
	@Source("zoom-in.png")
    ImageResource zoomIn();

    @Source("zoom-out.png")
    ImageResource zoomOut();

    @Source("draw-feature.png")
    ImageResource drawFeature();

    @Source("rotate.png")
    ImageResource rotate();

    @Source("drag.png")
    ImageResource drag();

    @Source("resize.png")
    ImageResource resize();

    @Source("shape.png")
    ImageResource shape();

    @Source("gp-icon-16x16.png")
    ImageResource geoPortalInfo();

    @Source("draw-point.png")
    ImageResource drawPointFeature();

    @Source("draw-line.png")
    ImageResource drawLineFeature();

    @Source("clear-map.png")
    ImageResource clearMap();

    @Source("zoom-last.png")
    ImageResource zoomPrevious();

    @Source("zoom-next.png")
    ImageResource zoomNext();

    @Source("ruler.png")
    ImageResource measure();

    @Source("ruler_square.png")
    ImageResource measureArea();
    
    @Source("mapa.png")
    ImageResource agregarMapa();
}
