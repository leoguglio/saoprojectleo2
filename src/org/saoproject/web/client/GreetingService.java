package org.saoproject.web.client;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


import org.datos.tallas.client.DatosTallas;
import org.gwtopenmaps.openlayers.client.layer.WFS;
import org.project.giisco.wfs.client.WFSManipulation;
import org.saoproject.web.shared.exc.ExcepcionLongitud;
import org.saoproject.web.shared.exc.ExcepcionNulo;
import org.saoproject.web.shared.exc.ExcepcionTipoCampo;
import org.saoproject.web.shared.exc.ExcepcionValidacion;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;





/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("greet")
public interface GreetingService extends RemoteService {
	//String crearMapa (String width, String height, int numZoom, String projection, double minX, double minY, double maxX, double maxY) 
	//throws IllegalArgumentException;
	
	String obtenerUrl(String layer) throws IllegalArgumentException;
	//List<WFS> obtenerWFS (String url) throws IllegalArgumentException;
	List<WFSManipulation> obtenerWFS (String url, String workSpace) throws IllegalArgumentException;
	//ResultSet ObtenerTallas(String campania, String zona,String estacion, String especie) throws IllegalArgumentException;
	List<WFS> obtenerWFSA (String url, String workSpace) throws IllegalArgumentException;
	List<String> ObtenerCampanias() throws IllegalArgumentException;
	List<String> ObtenerZonas() throws IllegalArgumentException;
	List<String> ObtenerEspecies() throws IllegalArgumentException;
	List<String> ObtenerEstaciones() throws IllegalArgumentException;
	List<String> ObtenerZona(String idZona) throws IllegalArgumentException;
	List<String> ObtenerCampania(String idCamp, String codigoZona);
	List<String> ObtenerCampaniaCenpat(String idCamp);
	List<String> ObtenerListadoZonas();
	void		 leerArchivoExcel(String archivoDestino);
	 List<String> ObtenerDistribucion(String Campana, String zona, int cantidad);
	List<WFSManipulation> callServer(String callMensaje, String[] callParametros);
	List<String> callServerString(String callMensaje, String[] callParametros);
	List<DatosTallas> callServerTallas(String callMensaje,
			String[] callParametros);
	List<DatosTallas> ObtenerTallas(String campania, String zona, String especie, String estacion) throws IllegalArgumentException;
	List<String> ObtenerZonasCampania(String campania) throws IllegalArgumentException;
	List<String> ObtenerEstacionesCampania(String campania,String zona) throws IllegalArgumentException;
	List<String> ObtenerEspeciesCampania(String campania, String zona,String estacion);
	List<List<String>> readFileZona(String nameFile,String tabla) throws  IOException, ExcepcionLongitud, ExcepcionNulo, ExcepcionTipoCampo, ExcepcionValidacion;
	void guardarDatosArchivo(String tabla, List<List<String>> datos) throws ExcepcionValidacion;
	void guardarCampania(List<String> datos) throws ExcepcionLongitud, ExcepcionNulo, ExcepcionValidacion, ExcepcionTipoCampo;
	void guardarEstacion(List<String> datos) throws ExcepcionLongitud, ExcepcionNulo, ExcepcionValidacion, ExcepcionTipoCampo;
	void guardarZona(List<String> datos) throws ExcepcionLongitud, ExcepcionNulo, ExcepcionValidacion, ExcepcionTipoCampo;
	void guardarDatosBio(List<String> datos) throws ExcepcionLongitud, ExcepcionNulo, ExcepcionValidacion, ExcepcionTipoCampo;
	void guardarEspecie(List<String> datos) throws ExcepcionLongitud, ExcepcionNulo, ExcepcionValidacion, ExcepcionTipoCampo;
	void guardarTalla(List<String> datos) throws ExcepcionLongitud, ExcepcionNulo, ExcepcionValidacion, ExcepcionTipoCampo;

}