package org.saoproject.web.client.loaders.ui;

import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.i18n.client.NumberFormat;  
import com.google.gwt.user.client.Window;  
import com.google.gwt.user.client.ui.HorizontalPanel;  
import com.google.gwt.user.client.ui.Label;  
import org.moxieapps.gwt.uploader.client.Uploader;  
import org.moxieapps.gwt.uploader.client.events.*;  
  
/** 
 * Simple Text Link and Progress text label example of GWT Uploader 
 */  
  
  
public class FileUploadPanel extends ScrollPanel
{

    private Label progressLabel;  
    private Uploader uploader;  

	public  FileUploadPanel() 
	{	 
		super();		
	//	this.setHeadingText("Carga de Archivos");
//		this.setButtonAlign(BoxLayoutPack.CENTER);				
	    this.add(getPanel());	 
	}
	
	
	public VerticalPanel getPanel()
	{
        progressLabel = new Label();  
        progressLabel.setStyleName("progressLabel");  
  
        uploader = new Uploader();  
        uploader.setUploadURL("/SaoProjectUI/FileUpload")  
            .setButtonText("<span class=\"buttonText\">Click para subir</span>")  
            .setButtonTextStyle(".buttonText {font-family: Arial, sans-serif; font-size: 14px; color: #BB4B44}")  
            .setFileSizeLimit("50 MB")  
            .setButtonWidth(150)  
            .setButtonHeight(22)  
            .setButtonCursor(Uploader.Cursor.HAND)  
            .setButtonAction(Uploader.ButtonAction.SELECT_FILE)  
            .setUploadProgressHandler(new UploadProgressHandler() {  
                public boolean onUploadProgress(UploadProgressEvent uploadProgressEvent) {  
                    progressLabel.setText(NumberFormat.getPercentFormat().format(  
                        (double)uploadProgressEvent.getBytesComplete() / (double)uploadProgressEvent.getBytesTotal()  
                    ));  
                    return true;  
                }  
            })  
            .setUploadSuccessHandler(new UploadSuccessHandler() {  
                public boolean onUploadSuccess(UploadSuccessEvent uploadSuccessEvent) {  
                    resetText();  
                    StringBuilder sb = new StringBuilder();  
                    sb.append("File ").append(uploadSuccessEvent.getFile().getName())  
                        .append(" (")  
                        .append(NumberFormat.getDecimalFormat().format(uploadSuccessEvent.getFile().getSize() / 1024))  
                        .append(" KB)")  
                        .append(" archivo subido con éxito en ")  
                        .append(NumberFormat.getDecimalFormat().format(  
                            uploadSuccessEvent.getFile().getAverageSpeed() / 1024  
                        ))  
                        .append(" Kb/second");  
                    progressLabel.setText(sb.toString());  
                    String fileName=uploadSuccessEvent.getFile().getName();
                    generateTable(fileName);
                    return true;  
                }  
            })  
            .setFileDialogCompleteHandler(new FileDialogCompleteHandler() {  
                public boolean onFileDialogComplete(FileDialogCompleteEvent fileDialogCompleteEvent) {  
                    if (fileDialogCompleteEvent.getTotalFilesInQueue() > 0 && uploader.getStats().getUploadsInProgress() <= 0) {  
                        progressLabel.setText("0%");  
                        uploader.setButtonText("<span class=\"buttonText\">Subiendo...</span>");  
                        uploader.startUpload();  
                    }  
                    return true;  
                }  
            })  
            .setFileQueueErrorHandler(new FileQueueErrorHandler() {  
                public boolean onFileQueueError(FileQueueErrorEvent fileQueueErrorEvent) {  
                    resetText();  
                    Window.alert("Subir el archivo " + fileQueueErrorEvent.getFile().getName() + " failed due to [" +  
                        fileQueueErrorEvent.getErrorCode().toString() + "]: " + fileQueueErrorEvent.getMessage()  
                    );  
                    return true;  
                }  
            })  
            .setUploadErrorHandler(new UploadErrorHandler() {  
                public boolean onUploadError(UploadErrorEvent uploadErrorEvent) {  
                    resetText();  
                    Window.alert("Subir el archivo " + uploadErrorEvent.getFile().getName() + " falló debido a [" +  
                        uploadErrorEvent.getErrorCode().toString() + "]: " + uploadErrorEvent.getMessage()  
                    );  
                    return true;  
                }  
            });  
  
        VerticalPanel verticalPanel = new VerticalPanel();  
        verticalPanel.add(uploader);  
        verticalPanel.add(progressLabel);  
        verticalPanel.setCellHorizontalAlignment(uploader, HorizontalPanel.ALIGN_LEFT);  
        verticalPanel.setCellHorizontalAlignment(progressLabel, HorizontalPanel.ALIGN_LEFT);
        
        return verticalPanel;
	}
	
	 protected void generateTable(String fileName)
	 {
	
		
	}


	private void resetText() {  
	        progressLabel.setText("");  
	        uploader.setButtonText("<span class=\"buttonText\">Click para subir</span>");  
	    }  
}