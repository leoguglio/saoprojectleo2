package org.saoproject.web.client.loaders.form.elementos;

public class ElemTalla {
	private int id;
	private String talla_id;

	private String campania_id;
	private String zona_id;
	private String estacion_id;
	private String especie_id;
	private String nroTalla;
	private String cantidad;
	private static int COUNTER = 0;
	
	public ElemTalla (){
		setId(Integer.valueOf(COUNTER++));
		setTalla_id(new Integer(getId()).toString());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCampania_id() {
		return campania_id;
	}

	public void setCampania_id(String campania_id) {
		this.campania_id = campania_id;
	}

	public String getZona_id() {
		return zona_id;
	}

	public void setZona_id(String zona_id) {
		this.zona_id = zona_id;
	}

	public String getEstacion_id() {
		return estacion_id;
	}

	public void setEstacion_id(String estacion_id) {
		this.estacion_id = estacion_id;
	}

	public String getEspecie_id() {
		return especie_id;
	}

	public void setEspecie_id(String especie_id) {
		this.especie_id = especie_id;
	}

	public String getNroTalla() {
		return nroTalla;
	}

	public void setNroTalla(String nroTalla) {
		this.nroTalla = nroTalla;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	
	public String getTalla_id() {
		return talla_id;
	}

	public void setTalla_id(String talla_id) {
		this.talla_id = talla_id;
	}


}
