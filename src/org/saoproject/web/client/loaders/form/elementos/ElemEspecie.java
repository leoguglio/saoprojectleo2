package org.saoproject.web.client.loaders.form.elementos;

public class ElemEspecie {
	private int id;
	private String codigo;
	private String nombre;
	private String nomCientifico;
	private static int COUNTER = 0;
	
	public ElemEspecie(){
		setId(Integer.valueOf(COUNTER++));
	
	}

	private void setId(int value) {
		id = value;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNomCientifico() {
		return nomCientifico;
	}

	public void setNomCientifico(String nomCientifico) {
		this.nomCientifico = nomCientifico;
	}
	
	

}
