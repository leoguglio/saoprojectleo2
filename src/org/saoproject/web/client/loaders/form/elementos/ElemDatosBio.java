package org.saoproject.web.client.loaders.form.elementos;

public class ElemDatosBio {
	private int id;
	private String id_datosBio;
	private String estacion_id;
	private String especie_id;
	private String abundancia;
	private String densidad;
	private String distribucion;
	private static int COUNTER = 0;

	
	
	public String getId_datosBio() {
		return id_datosBio;
	}

	public void setId_datosBio(String id_datosBio) {
		this.id_datosBio = id_datosBio;
	}

	
	public ElemDatosBio(){
		setId(Integer.valueOf(COUNTER++));
		setId_datosBio(new Integer(getId()).toString());
	}
	
	public void setId (int id){
		this.id=id;
	}

	public int getId() {
		return id;
	}

	public String getEstacion_id() {
		return estacion_id;
	}

	public void setEstacion_id(String estacion_id) {
		this.estacion_id = estacion_id;
	}

	public String getEspecie_id() {
		return especie_id;
	}

	public void setEspecie_id(String especie_id) {
		this.especie_id = especie_id;
	}

	public String getAbundancia() {
		return abundancia;
	}

	public void setAbundancia(String abundancia) {
		this.abundancia = abundancia;
	}

	public String getDensidad() {
		return densidad;
	}

	public void setDensidad(String densidad) {
		this.densidad = densidad;
	}

	public String getDistribucion() {
		return distribucion;
	}

	public void setDistribucion(String distribucion) {
		this.distribucion = distribucion;
	}


	
}