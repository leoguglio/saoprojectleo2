package org.saoproject.web.client.loaders.form.elementos;

public class ElemCampania{
	private int id;
	private String codigo;
	private String anio;
	private String nombre;
	private String tipo_campania;
	private String tipo_muestreo;
	private String nro_estaciones;
	private String fecha;
	
	private static int COUNTER = 0;
	
	public ElemCampania (){
		this.setId(Integer.valueOf(COUNTER++));
	}
	
	public ElemCampania(String cod,String ano,String nom, String tipoC, String tipoM,String nro, String fecha){
		setId(Integer.valueOf(COUNTER++));
		setCodigo(cod);
		setAnio(ano);
		setNombre(nom);
		setTipo_campania(tipoC);
		setTipo_muestreo(tipoM);
		setNro_estaciones(nro);
		setFecha(fecha);
	}

	
	private void setId(int id) {
		this.id = id;
		
	}


	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getAnio() {
		return anio;
	}
	public void setAnio(String anio) {
		this.anio = anio;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipo_campania() {
		return tipo_campania;
	}
	public void setTipo_campania(String tipo_campania) {
		this.tipo_campania = tipo_campania;
	}
	public String getTipo_muestreo() {
		return tipo_muestreo;
	}
	public void setTipo_muestreo(String tipo_muestreo) {
		this.tipo_muestreo = tipo_muestreo;
	}
	public String getNro_estaciones() {
		return nro_estaciones;
	}
	public void setNro_estaciones(String nro_estaciones) {
		this.nro_estaciones = nro_estaciones;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

}
