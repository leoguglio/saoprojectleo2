package org.saoproject.web.client.loaders.form.elementos;

public class ElemEstacion {
	private int id;
	private String campania_id;
	private String zona_id;
	private String codigo_estacion;
	private String lat,lon;
	private static int COUNTER = 0;
	public ElemEstacion(){
		setId(Integer.valueOf(COUNTER++));
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCampania_id() {
		return campania_id;
	}
	public void setCampania_id(String campania_id) {
		this.campania_id = campania_id;
	}
	public String getZona_id() {
		return zona_id;
	}
	public void setZona_id(String zona_id) {
		this.zona_id = zona_id;
	}
	public String getCodigo_estacion() {
		return codigo_estacion;
	}
	public void setCodigo_estacion(String codigo_estacion) {
		this.codigo_estacion = codigo_estacion;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLon() {
		return lon;
	}
	public void setLon(String lon) {
		this.lon = lon;
	}
	
}
