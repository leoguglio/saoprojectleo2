package org.saoproject.web.client.loaders.form.elementos;

public class ElemZona {

	private int id;
	private String name, codigo;
	private String lat1, lat2, lat3, lat4, lat5, lat6;
	private String lon1, lon2, lon3, lon4, lon5, lon6;

	private static int COUNTER = 0;

	public ElemZona() {
		this.setId(Integer.valueOf(COUNTER++));
	}

	public ElemZona(String codigo) {
		this.setCodigo(codigo);
	}

	public ElemZona(String codigo, String name, String lat1, String lon1,
			String lat2, String lon2, String lat3, String lon3, String lat4,
			String lon4, String lat5, String lon5, String lat6, String lon6) {
		this();
		this.setCodigo(codigo);
		this.setName(name);
		this.setLat1(lat1);
		this.setLat2(lat2);
		this.setLat3(lat3);
		this.setLat4(lat4);
		this.setLat5(lat5);
		this.setLat6(lat6);
		this.setLon1(lon1);
		this.setLon2(lon2);
		this.setLon3(lon3);
		this.setLon4(lon4);
		this.setLon5(lon5);
		this.setLon6(lon6);

	}

	public void setId(int id) {
		this.id = id;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setName(String nombre) {
		this.name = nombre;
	}

	public void setLat1(String lat1) {
		this.lat1 = lat1;
	}

	public void setLon1(String lon1) {
		this.lon1 = lon1;
	}

	public void setLat2(String lat2) {
		this.lat2 = lat2;
	}

	public void setLon2(String lon2) {
		this.lon2 = lon2;
	}

	public void setLat3(String lat3) {
		this.lat3 = lat3;
	}

	public void setLon3(String lon3) {
		this.lon3 = lon3;
	}

	public void setLat4(String lat4) {
		this.lat4 = lat4;
	}

	public void setLon4(String lon4) {
		this.lon4 = lon4;
	}

	public void setLat5(String lat5) {
		this.lat5 = lat5;
	}

	public void setLon5(String lon5) {
		this.lon5 = lon5;
	}

	public void setLon6(String lon6) {
		this.lon6 = lon6;
	}

	public void setLat6(String lat6) {
		this.lat6 = lat6;
	}

	// getters de los parametros

	public int getId() {
		return id;
	}

	public String getCodigo() {
		return codigo;
	}

	public String getName() {
		return name;
	}

	public String getLat1() {
		return lat1;
	}

	public String getLon1() {
		return lon1;
	}

	public String getLat2() {
		return lat2;
	}

	public String getLon2() {
		return lon2;
	}

	public String getLat3() {
		return lat3;
	}

	public String getLon3() {
		return lon3;
	}

	public String getLat4() {
		return lat4;
	}

	public String getLon4() {
		return lon4;
	}

	public String getLat5() {
		return lat5;
	}

	public String getLon5() {
		return lon5;
	}

	public String getLon6() {
		return lon6;
	}

	public String getLat6() {
		return lat6;
	}

}