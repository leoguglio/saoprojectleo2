package org.saoproject.web.client.loaders.form.propertiesAccess;

import org.saoproject.web.client.loaders.form.elementos.ElemZona;

import com.google.gwt.editor.client.Editor.Path;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

public interface PropertiesZona extends PropertyAccess<ElemZona> {
	  @Path("id")
	  ModelKeyProvider<ElemZona> key();
	  
	  ValueProvider<ElemZona, String> codigo();
	  
	  ValueProvider<ElemZona, String> name();
	   
	  ValueProvider<ElemZona, String> lat1();
	   
	  ValueProvider<ElemZona, String> lon1();
	   
	  ValueProvider<ElemZona, String> lat2();
	   
	  ValueProvider<ElemZona, String> lon2();
	   
	  ValueProvider<ElemZona, String> lat3();
	   
	  ValueProvider<ElemZona, String> lon3();
	   
	  ValueProvider<ElemZona, String> lat4();
	   
	  ValueProvider<ElemZona, String> lon4();
	  
	  ValueProvider<ElemZona, String> lat5();
	   
	  ValueProvider<ElemZona, String> lon5();
	   
	  ValueProvider<ElemZona, String> lat6();
	   
	  ValueProvider<ElemZona, String> lon6();
	   
}