package org.saoproject.web.client.loaders.form.propertiesAccess;

import org.saoproject.web.client.loaders.form.elementos.ElemEstacion;

import com.google.gwt.editor.client.Editor.Path;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

public interface PropertiesEstacion extends PropertyAccess<ElemEstacion>{
	@Path("id")
	ModelKeyProvider<ElemEstacion> key();
	
	ValueProvider<ElemEstacion, String> campania_id();
	ValueProvider<ElemEstacion, String> zona_id();
	ValueProvider<ElemEstacion, String> codigo_estacion();
	ValueProvider<ElemEstacion, String> lat();
	ValueProvider<ElemEstacion, String> lon();
}
