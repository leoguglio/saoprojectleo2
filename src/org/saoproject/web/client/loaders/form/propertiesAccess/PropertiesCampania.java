package org.saoproject.web.client.loaders.form.propertiesAccess;


import org.saoproject.web.client.loaders.form.elementos.ElemCampania;

import com.google.gwt.editor.client.Editor.Path;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

public interface PropertiesCampania extends PropertyAccess<ElemCampania> {
	  @Path("id")	
	  ModelKeyProvider<ElemCampania> key();
	  
	  ValueProvider<ElemCampania, String> codigo();
	  
	  ValueProvider<ElemCampania, String> anio();
	   
	  ValueProvider<ElemCampania, String> nombre();
	   
	  ValueProvider<ElemCampania, String> tipo_campania();
	   
	  ValueProvider<ElemCampania, String> tipo_muestreo();
	   
	  ValueProvider<ElemCampania, String> nro_estaciones();
	   
	  ValueProvider<ElemCampania, String> fecha();
	  
}