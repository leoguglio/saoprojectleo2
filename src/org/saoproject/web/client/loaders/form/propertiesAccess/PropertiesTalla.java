package org.saoproject.web.client.loaders.form.propertiesAccess;

import org.saoproject.web.client.loaders.form.elementos.ElemTalla;

import com.google.gwt.editor.client.Editor.Path;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

public interface PropertiesTalla extends PropertyAccess<ElemTalla>{
	@Path("id")
	ModelKeyProvider<ElemTalla> key();
	ValueProvider<ElemTalla, String> id_talla();
	ValueProvider<ElemTalla, String> campania_id();
	ValueProvider<ElemTalla, String> zona_id();
	ValueProvider<ElemTalla, String> estacion_id();
	ValueProvider<ElemTalla, String> especie_id();
	ValueProvider<ElemTalla, String> nroTalla();
	ValueProvider<ElemTalla, String> cantidad();
	
	
}
