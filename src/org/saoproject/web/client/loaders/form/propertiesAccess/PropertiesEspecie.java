package org.saoproject.web.client.loaders.form.propertiesAccess;

import org.saoproject.web.client.loaders.form.elementos.ElemEspecie;

import com.google.gwt.editor.client.Editor.Path;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

public interface PropertiesEspecie extends PropertyAccess<ElemEspecie>{
	@Path("codigo")
	ModelKeyProvider<ElemEspecie> key();
	
	ValueProvider<ElemEspecie, String> codigo();
	ValueProvider<ElemEspecie, String> nombre();
	ValueProvider<ElemEspecie, String> nomCientifico();

}
