package org.saoproject.web.client.loaders.form.propertiesAccess;

import org.saoproject.web.client.loaders.form.elementos.ElemDatosBio;

import com.google.gwt.editor.client.Editor.Path;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

public interface PropertiesDatosBio extends PropertyAccess<ElemDatosBio>{
	@Path("id")
	ModelKeyProvider<ElemDatosBio> key();
	
	ValueProvider<ElemDatosBio, String> id_datosBio();
	ValueProvider<ElemDatosBio, String> estacion_id();
	ValueProvider<ElemDatosBio, String> especie_id();
	ValueProvider<ElemDatosBio, String> abundancia();
	ValueProvider<ElemDatosBio, String> densidad();
	ValueProvider<ElemDatosBio, String> distribucion();
}
