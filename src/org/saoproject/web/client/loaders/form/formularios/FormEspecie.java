package org.saoproject.web.client.loaders.form.formularios;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.saoproject.web.client.Configs;
import org.saoproject.web.client.GreetingService;
import org.saoproject.web.client.GreetingServiceAsync;
import org.saoproject.web.client.loaders.form.elementos.ElemDatosBio;
import org.saoproject.web.client.loaders.form.elementos.ElemEspecie;
import org.saoproject.web.client.loaders.form.propertiesAccess.PropertiesEspecie;
import org.saoproject.web.shared.exc.ExcepcionLongitud;
import org.saoproject.web.shared.exc.ExcepcionNulo;
import org.saoproject.web.shared.exc.ExcepcionTipoCampo;
import org.saoproject.web.shared.exc.ExcepcionValidacion;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell.TriggerAction;
import com.sencha.gxt.core.client.IdentityValueProvider;
import com.sencha.gxt.core.client.Style.SelectionMode;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.FieldSet;
import com.sencha.gxt.widget.core.client.form.SimpleComboBox;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.grid.CheckBoxSelectionModel;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;

public class FormEspecie extends SimpleForm{
	private static final PropertiesEspecie props = GWT
	.create(PropertiesEspecie.class);
	private ListStore<ElemEspecie> store;

	
	private VerticalPanel pContenedor;
	private VerticalPanel pTextField;
	private HorizontalPanel pBotones;
	private List<String> listaCampos;	
	private List<List<String>> listaDatos;
	private GreetingServiceAsync fileService; 
	private TextField codigo, nombre, nombreCientifico;
	
	public FormEspecie() {
		createFormEspecie();
	}
	private void createFormEspecie(){
		pTextField = new VerticalPanel();
		    
		FieldSet fieldSet = new FieldSet();
		fieldSet.setHeadingText("Ingreso de datos");
		fieldSet.setCollapsible(false);
		fieldSet.add(pTextField);
		
		
		
		codigo = new TextField();
		pTextField.add(new FieldLabel(codigo, "Código"));
		nombre = new TextField();
		pTextField.add(new FieldLabel(nombre, "Nombre"));
		nombreCientifico = new TextField();
		pTextField.add(new FieldLabel(nombreCientifico, "nombreCientifico"));

		
		
		pBotones = new HorizontalPanel();

		pBotones.add(new TextButton("Guardar", new SelectHandler(){
		    @Override
		    public void onSelect(SelectEvent event){
		    	//Seteo de los validadores gráficos
//				MaxLengthValidator valid10 = new MaxLengthValidator(10);
//				valid10.setMessages(new LongitudMaxima());
//				MaxLengthValidator valid20 = new MaxLengthValidator(20);
//				valid20.setMessages(new LongitudMaxima());
//				EmptyValidator<String> validNulo = new EmptyValidator<String>();
//				validNulo.setMessages(new MensajeVacio());
//				codigo.addValidator(valid10);
//				codigo.addValidator(validNulo);
//				anio.addValidator(valid10);
//				anio.addValidator(validNulo);
//				nombre.addValidator(valid20);
//				nombre.addValidator(validNulo);
//
		    	listaCampos = new ArrayList<String>();
				listaCampos.add(codigo.getValue());
				listaCampos.add(nombre.getValue());					
			    listaCampos.add(nombreCientifico.getValue());

				guardarDatos();
		    	
		    }}));	
		    pBotones.add(new TextButton("Cancelar"));
		    
		    pContenedor = new VerticalPanel();
		    pContenedor.add(fieldSet);
		    pContenedor.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		    pContenedor.add(pBotones);
		    this.add(pContenedor);
		    this.add(cargarCSV());
		    
    }
	
	//Se guardan los datos ingresados en el formulario validando su correctitud
	private void guardarDatos(){
				
			 	if (fileService == null)
			 		fileService = GWT.create(GreetingService.class);
				
				AsyncCallback<Void> callback1 = new AsyncCallback<Void>()     {
					@Override
					public void onFailure(Throwable caught) {
//						if (caught instanceof ExcepcionValidacion)
//							Window.alert("La longitud excede el maximo permitido");
						
						try {
					         throw caught;
					       } catch (ExcepcionLongitud e) {
					         //Excepcion ya que el campo excede el maximo admitido 
					    	   Window.alert("El campo" +e.getCampo()+" excede la longitud máxima");
					       } catch (ExcepcionNulo e) {
					    	   //Excepcion porque el campo no admite nulos
					    	   Window.alert("El campo "+e.getCampo()+" no puede ser nulo");
					       } catch (ExcepcionTipoCampo e) {
					    	   //Excepcion cuando el tipo de dato esperado para el campo no coincide con el ingresado
					    	   Window.alert("El tipo del campo es incorrecto");
					       } catch (IOException e) {
					    	   //Excepcion que ocurre cuando hay un error de E/S
					    	   Window.alert("Ocurrio un error al leer el archivo");
					       } catch (ExcepcionValidacion e) {
					    	   //Excepcion que ocurre cuando se da un error de validacion general
					    	   Window.alert("Ocurrio error de validación: "+e.getError());
					       } catch (Exception e) {
					    	   //Excepcion que ocurre cuando falla la comunación con el servidor
					    	   Window.alert("Ocurrio error de comunicación con el servidor ");
					       } catch (Throwable e) {
					    	   //Error general
								Window.alert("Ocurrio un Error");
						} 
					}


					@Override
					public void onSuccess(Void result) {
						Window.alert("Datos guardados");
						
					}	
				};
				fileService.guardarEspecie(listaCampos,callback1);
	}
	
	
	protected void cargarArchivo(String filePath) {
		if(fileService==null)  
			GWT.create(GreetingService.class);

		AsyncCallback<List<List<String>>> callback1 = new AsyncCallback<List<List<String>>>() {
			public void onFailure(Throwable caught) {
			     try {
			         throw caught;
			       } catch (ExcepcionLongitud e) {
			         // this client is not compatible with the server; cleanup and refresh the 
			         // browser
			    	   Window.alert("El campo excede la longitud máxima");
			       } catch (ExcepcionNulo e) {
			    	   Window.alert("El  campo no puede ser nulo");
			         // the call didn't complete cleanly
			       } catch (ExcepcionTipoCampo e) {
			    	   Window.alert("El tipo del campo es incorrecto");
			         // one of the 'throws' from the original method
			       } catch (IOException e) {
			    	   Window.alert("Ocurrio un error al leer el archivo");
			         // one of the 'throws' from the original method
			       } catch (ExcepcionValidacion e) {
			    	   Window.alert("Ocurrio error de validación"+e.getError());
			         // last resort -- a very unexpected exception
			       } catch (Throwable e) {
			    	   Window.alert("Ocurrio error al procesar el archivo");
					// TODO Auto-generated catch block
		//	    	   e.printStackTrace();
				}
		
				//Window.alert("Error al leer archivo");
			}
		
			@Override
			public void onSuccess(List<List<String>> result) {
				System.out.println("El archivo leido es: "+result.toString());
				for (int i = 0; i < result.size(); i++) {
					ElemEspecie especieNueva = new ElemEspecie();
		
					List<String> lt = result.get(i);
					especieNueva.setCodigo(lt.get(0));
					especieNueva.setNombre(lt.get(1));
					especieNueva.setNomCientifico(lt.get(2));
						
					store.add(0,especieNueva);
					
				}
				listaDatos = result;
			}
		};
		fileService
				.readFileZona(Configs.FILE_UPLOAD_PATH + filePath,"ESPECIES",callback1);
				
	}
	
	public void guardarDatosArchivo(String tabla){
		if (fileService == null) 
			fileService = GWT.create(GreetingService.class);
		
		AsyncCallback<Void> callback1 = new AsyncCallback<Void>()     {
			public void onFailure(Throwable caught) {
				try {
					throw caught;
				} catch (ExcepcionValidacion e){
					Window.alert("Ocurrio un error de validacion: "+e.getError());
				} catch (Throwable e){
					Window.alert("Ocurrio un error al cargar datos al servidor");
				}
			}

			@Override
			public void onSuccess(Void result) {
				Window.alert("Datos guardados");
				
			}	
		};
		fileService.guardarDatosArchivo(tabla, listaDatos,callback1);
	}
	
	//Metodo que carga el formulario para la carga desde un archivo .csv
	private Widget cargarCSV (){
		FlowPanel panel = new FlowPanel();

		panel.add(new TextButton("Reset", new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				store.rejectChanges();
				store.clear();
			}
		}));
		
		panel.add(new TextButton("Guardar", new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				store.commitChanges();
				guardarDatosArchivo("ESPECIES");
			}
		}));

		panel.add(uploadLabel());
		panel.add(initTablaEspecie());
		return panel.asWidget();
	}
	
	//Metodo que inicia la tabla correspondiente
	private Widget initTablaEspecie() {
		List<ColumnConfig<ElemEspecie, ?>> tabla = new ArrayList<ColumnConfig<ElemEspecie, ?>>();
		IdentityValueProvider<ElemEspecie> identity = new IdentityValueProvider<ElemEspecie>();
		CheckBoxSelectionModel<ElemEspecie> sm = new CheckBoxSelectionModel<ElemEspecie>(identity);
		sm.setSelectionMode(SelectionMode.SINGLE);
		tabla.add(sm.getColumn());

		 ColumnConfig<ElemEspecie, String> col1 = new ColumnConfig<ElemEspecie, String>(
					props.codigo(), 3, "Código");
		 ColumnConfig<ElemEspecie, String> col2 = new ColumnConfig<ElemEspecie, String>(
				props.nombre(), 3, "Nombre");
		 ColumnConfig<ElemEspecie, String> col3 = new ColumnConfig<ElemEspecie, String>(
				props.nomCientifico(), 3, "Nombre Científico");
		

		tabla.add(col1);
		tabla.add(col2);
		tabla.add(col3);
		
		ColumnModel<ElemEspecie> cm = new ColumnModel<ElemEspecie>(tabla);
		
		store = new ListStore<ElemEspecie>(props.key());

		Grid<ElemEspecie> grid = new Grid<ElemEspecie>(store, cm);
		grid.getView().setForceFit(true);
		grid.setLoadMask(true);
		grid.setBorders(true);
		grid.getView().setAutoExpandColumn(col1);
		grid.getView().setAutoExpandColumn(col2);
		grid.getView().setAutoExpandColumn(col3);
		grid.getView().setColumnLines(true);
		grid.setSelectionModel(sm);
		grid.addStyleName(Configs.CSS_UPLOADTABLE);
		
		ContentPanel contentPanel = new ContentPanel();
		contentPanel.setHeaderVisible(false);
		contentPanel.add(grid);
		contentPanel.forceLayout();
		return contentPanel.asWidget();
		

	}

}
	
