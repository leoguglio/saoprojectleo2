package org.saoproject.web.client.loaders.form.formularios;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.saoproject.web.client.Configs;
import org.saoproject.web.client.GreetingService;
import org.saoproject.web.client.GreetingServiceAsync;
import org.saoproject.web.client.callbacks.ParallelCallback;
import org.saoproject.web.client.callbacks.ParentCallback;
import org.saoproject.web.client.loaders.form.elementos.ElemDatosBio;
import org.saoproject.web.client.loaders.form.propertiesAccess.PropertiesDatosBio;
import org.saoproject.web.shared.exc.ExcepcionLongitud;
import org.saoproject.web.shared.exc.ExcepcionNulo;
import org.saoproject.web.shared.exc.ExcepcionTipoCampo;
import org.saoproject.web.shared.exc.ExcepcionValidacion;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell.TriggerAction;
import com.sencha.gxt.core.client.IdentityValueProvider;
import com.sencha.gxt.core.client.Style.SelectionMode;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.FieldSet;
import com.sencha.gxt.widget.core.client.form.SimpleComboBox;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.grid.CheckBoxSelectionModel;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;

public class FormDatosBio extends SimpleForm{
	private static final PropertiesDatosBio props = GWT
	.create(PropertiesDatosBio.class);
	private ListStore<ElemDatosBio> store;

	
	private VerticalPanel pContenedor;
	private VerticalPanel pTextField;
	private HorizontalPanel pBotones;
	private List<String> listaCampos;	
	private List<List<String>> listaDatos;
	private GreetingServiceAsync fileService; 
	private TextField abun,densidad,distrib;
	
	public FormDatosBio() {
		//Se cargan los datos de estaciones y especies desde la BD
		obtenerDatos();
	}
	private void createFormDatosBio(List<String> estaciones, List<String> especies){
		pTextField = new VerticalPanel();
		    
		FieldSet fieldSet = new FieldSet();
		fieldSet.setHeadingText("Ingreso de datos");
		fieldSet.setCollapsible(false);
		fieldSet.add(pTextField);
		
	    final LabelProvider<String> lb = new LabelProvider<String>() {

            @Override
            public String getLabel(String item) {
                return item;
            }

        };
        

		
	    final SimpleComboBox<String> combo_estaciones = new SimpleComboBox<String>(lb);
	    for (String elem: estaciones)
			combo_estaciones.add(lb.getLabel(elem));
	    combo_estaciones.setAllowTextSelection(false);
	    combo_estaciones.setEditable(false);
	    combo_estaciones.setEmptyText("Seleccionar estacion...");
	    combo_estaciones.setTriggerAction(TriggerAction.ALL);

		pTextField.add(new FieldLabel(combo_estaciones,"Zona"));
		
		
		final SimpleComboBox<String> combo_especies = new SimpleComboBox<String>(lb);
		for (String elem: especies)
		combo_especies.add(lb.getLabel(elem));
		combo_especies.setAllowTextSelection(false);
		combo_especies.setEditable(false);
		combo_especies.setEmptyText("Seleccionar especie...");
		combo_especies.setTriggerAction(TriggerAction.ALL);
		
		pTextField.add(new FieldLabel(combo_especies,"Especie"));
		
		abun = new TextField();
		pTextField.add(new FieldLabel(abun, "Abundancia"));
		densidad = new TextField();
		pTextField.add(new FieldLabel(densidad, "Densidad"));
		distrib = new TextField();
		pTextField.add(new FieldLabel(distrib, "Distribución"));

		
		
		pBotones = new HorizontalPanel();

		pBotones.add(new TextButton("Guardar", new SelectHandler(){
		    @Override
		    public void onSelect(SelectEvent event){
		    	//Seteo de los validadores gráficos
//				MaxLengthValidator valid10 = new MaxLengthValidator(10);
//				valid10.setMessages(new LongitudMaxima());
//				MaxLengthValidator valid20 = new MaxLengthValidator(20);
//				valid20.setMessages(new LongitudMaxima());
//				EmptyValidator<String> validNulo = new EmptyValidator<String>();
//				validNulo.setMessages(new MensajeVacio());
//				codigo.addValidator(valid10);
//				codigo.addValidator(validNulo);
//				anio.addValidator(valid10);
//				anio.addValidator(validNulo);
//				nombre.addValidator(valid20);
//				nombre.addValidator(validNulo);
//
		    	listaCampos = new ArrayList<String>();
     		    listaCampos.add(combo_estaciones.getValue());
			    listaCampos.add(combo_especies.getValue());
				listaCampos.add(abun.getValue());
				listaCampos.add(densidad.getValue());					
			    listaCampos.add(distrib.getValue());

				guardarDatos();
		    	
		    }}));	
		    pBotones.add(new TextButton("Cancelar"));
		    
		    pContenedor = new VerticalPanel();
		    pContenedor.add(fieldSet);
		    pContenedor.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		    pContenedor.add(pBotones);
		    this.add(pContenedor);
		    this.add(cargarCSV());
		    
    }
	
	//Se guardan los datos ingresados en el formulario validando su correctitud
	private void guardarDatos(){
				
			 	if (fileService == null)
			 		fileService = GWT.create(GreetingService.class);
				
				AsyncCallback<Void> callback1 = new AsyncCallback<Void>()     {
					@Override
					public void onFailure(Throwable caught) {
//						if (caught instanceof ExcepcionValidacion)
//							Window.alert("La longitud excede el maximo permitido");
						
						try {
					         throw caught;
					       } catch (ExcepcionLongitud e) {
					         //Excepcion ya que el campo excede el maximo admitido 
					    	   Window.alert("El campo" +e.getCampo()+" excede la longitud máxima");
					       } catch (ExcepcionNulo e) {
					    	   //Excepcion porque el campo no admite nulos
					    	   Window.alert("El campo "+e.getCampo()+" no puede ser nulo");
					       } catch (ExcepcionTipoCampo e) {
					    	   //Excepcion cuando el tipo de dato esperado para el campo no coincide con el ingresado
					    	   Window.alert("El tipo del campo es incorrecto");
					       } catch (IOException e) {
					    	   //Excepcion que ocurre cuando hay un error de E/S
					    	   Window.alert("Ocurrio un error al leer el archivo");
					       } catch (ExcepcionValidacion e) {
					    	   //Excepcion que ocurre cuando se da un error de validacion general
					    	   Window.alert("Ocurrio error de validación: "+e.getError());
					       } catch (Exception e) {
					    	   //Excepcion que ocurre cuando falla la comunación con el servidor
					    	   Window.alert("Ocurrio error de comunicación con el servidor ");
					       } catch (Throwable e) {
					    	   //Error general
								Window.alert("Ocurrio un Error");
						} 
					}


					@Override
					public void onSuccess(Void result) {
						Window.alert("Datos guardados");
						
					}	
				};
				fileService.guardarDatosBio(listaCampos,callback1);
	}
	
	private void obtenerDatos(){
		ParallelCallback<List<String>> Callback0 = new ParallelCallback<List<String>>();  
		ParallelCallback<List<String>> Callback1 = new ParallelCallback<List<String>>();

		if (fileService == null)
			fileService = GWT.create(GreetingService.class);
		
		//final ParentCallback parent = new ParentCallback(Callback0, Callback1) {
		ParentCallback parent = new ParentCallback
					(Callback0,
					 Callback1)
		{  
			public void handleSuccess() 
			{  
				createFormDatosBio((List<String>)getCallbackData(0),
						(List<String>)getCallbackData(1));
			}
		};  		
		fileService.ObtenerEstaciones(Callback0);
		fileService.ObtenerEspecies(Callback1);			
	}
	
	protected void cargarArchivo(String filePath) {
		if(fileService==null)  
			GWT.create(GreetingService.class);

		AsyncCallback<List<List<String>>> callback1 = new AsyncCallback<List<List<String>>>() {
			public void onFailure(Throwable caught) {
			     try {
			         throw caught;
			       } catch (ExcepcionLongitud e) {
			         // this client is not compatible with the server; cleanup and refresh the 
			         // browser
			    	   Window.alert("El campo excede la longitud máxima");
			       } catch (ExcepcionNulo e) {
			    	   Window.alert("El  campo no puede ser nulo");
			         // the call didn't complete cleanly
			       } catch (ExcepcionTipoCampo e) {
			    	   Window.alert("El tipo del campo es incorrecto");
			         // one of the 'throws' from the original method
			       } catch (IOException e) {
			    	   Window.alert("Ocurrio un error al leer el archivo");
			         // one of the 'throws' from the original method
			       } catch (ExcepcionValidacion e) {
			    	   Window.alert("Ocurrio error de validación"+e.getError());
			         // last resort -- a very unexpected exception
			       } catch (Throwable e) {
			    	   Window.alert("Ocurrio error al procesar el archivo");
					// TODO Auto-generated catch block
		//	    	   e.printStackTrace();
				}
		
				//Window.alert("Error al leer archivo");
			}
		
			@Override
			public void onSuccess(List<List<String>> result) {
		
				for (int i = 0; i < result.size(); i++) {
					ElemDatosBio datosNuevo = new ElemDatosBio();
		
					List<String> lt = result.get(i);
					datosNuevo.setEstacion_id(lt.get(1));
					datosNuevo.setEspecie_id(lt.get(2));
					datosNuevo.setAbundancia(lt.get(3));
					datosNuevo.setDensidad(lt.get(4));
					datosNuevo.setDistribucion(lt.get(5));
						
					store.add(0,datosNuevo);
					
				}
				listaDatos = result;
			}
		};
		fileService
				.readFileZona(Configs.FILE_UPLOAD_PATH + filePath,"DATOS_BIOLOGICOS",callback1);
				
	}
	
	public void guardarDatosArchivo(String tabla){
		if (fileService == null) 
			fileService = GWT.create(GreetingService.class);
		
		AsyncCallback<Void> callback1 = new AsyncCallback<Void>()     {
			public void onFailure(Throwable caught) {
				try {
					throw caught;
				} catch (ExcepcionValidacion e){
					Window.alert("Ocurrio un error de validacion: "+e.getError());
				} catch (Throwable e){
					Window.alert("Ocurrio un error al cargar datos al servidor");
				}
			}

			@Override
			public void onSuccess(Void result) {
				Window.alert("Datos guardados");
				
			}	
		};
		fileService.guardarDatosArchivo(tabla, listaDatos,callback1);
	}
	
	//Metodo que carga el formulario para la carga desde un archivo .csv
	private Widget cargarCSV (){
		FlowPanel panel = new FlowPanel();

		panel.add(new TextButton("Reset", new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				store.rejectChanges();
				store.clear();
			}
		}));
		
		panel.add(new TextButton("Guardar", new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				store.commitChanges();
				guardarDatosArchivo("DATOS_BIOLOGICOS");
			}
		}));

		panel.add(uploadLabel());
		panel.add(initTablaDatosBio());
		return panel.asWidget();
	}
	
	//Metodo que inicia la tabla correspondiente
	private Widget initTablaDatosBio() {
		List<ColumnConfig<ElemDatosBio, ?>> tabla = new ArrayList<ColumnConfig<ElemDatosBio, ?>>();
		IdentityValueProvider<ElemDatosBio> identity = new IdentityValueProvider<ElemDatosBio>();
		CheckBoxSelectionModel<ElemDatosBio> sm = new CheckBoxSelectionModel<ElemDatosBio>(identity);
		sm.setSelectionMode(SelectionMode.SINGLE);
		tabla.add(sm.getColumn());

		 ColumnConfig<ElemDatosBio, String> col1 = new ColumnConfig<ElemDatosBio, String>(
					props.id_datosBio(), 3, "ID");
		 ColumnConfig<ElemDatosBio, String> col2 = new ColumnConfig<ElemDatosBio, String>(
				props.estacion_id(), 3, "Id Estacion");
		 ColumnConfig<ElemDatosBio, String> col3 = new ColumnConfig<ElemDatosBio, String>(
				props.especie_id(), 3, "Id Especie");
		 ColumnConfig<ElemDatosBio, String> col4 = new ColumnConfig<ElemDatosBio, String>(
				props.abundancia(), 3, "Abundancia");
		 ColumnConfig<ElemDatosBio, String> col5 = new ColumnConfig<ElemDatosBio, String>(
				props.densidad(), 3, "Densidad");
		 ColumnConfig<ElemDatosBio, String> col6 = new ColumnConfig<ElemDatosBio, String>(
					props.distribucion(), 3, "Distribución");
		

		tabla.add(col1);
		tabla.add(col2);
		tabla.add(col3);
		tabla.add(col4);
		tabla.add(col5);
		tabla.add(col6);
		
		ColumnModel<ElemDatosBio> cm = new ColumnModel<ElemDatosBio>(tabla);
		
		store = new ListStore<ElemDatosBio>(props.key());

		Grid<ElemDatosBio> grid = new Grid<ElemDatosBio>(store, cm);
		grid.getView().setForceFit(true);
		grid.setLoadMask(true);
		grid.setBorders(true);
		grid.getView().setAutoExpandColumn(col1);
		grid.getView().setAutoExpandColumn(col2);
		grid.getView().setAutoExpandColumn(col3);
		grid.getView().setAutoExpandColumn(col4);
		grid.getView().setAutoExpandColumn(col5);
		grid.getView().setAutoExpandColumn(col6);
		grid.getView().setColumnLines(true);
		grid.setSelectionModel(sm);
		grid.addStyleName(Configs.CSS_UPLOADTABLE);
		
		ContentPanel contentPanel = new ContentPanel();
		contentPanel.setHeaderVisible(false);
		contentPanel.add(grid);
		contentPanel.forceLayout();
		return contentPanel.asWidget();
		

	}

}
	
