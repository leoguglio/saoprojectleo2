package org.saoproject.web.client.loaders.form.formularios;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.saoproject.web.client.Configs;
import org.saoproject.web.client.GreetingService;
import org.saoproject.web.client.GreetingServiceAsync;
import org.saoproject.web.client.callbacks.ParallelCallback;
import org.saoproject.web.client.callbacks.ParentCallback;
import org.saoproject.web.client.loaders.form.elementos.ElemEstacion;
import org.saoproject.web.client.loaders.form.propertiesAccess.PropertiesEstacion;
import org.saoproject.web.shared.Tipos;
import org.saoproject.web.shared.exc.ExcepcionLongitud;
import org.saoproject.web.shared.exc.ExcepcionNulo;
import org.saoproject.web.shared.exc.ExcepcionTipoCampo;
import org.saoproject.web.shared.exc.ExcepcionValidacion;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell.TriggerAction;
import com.sencha.gxt.core.client.IdentityValueProvider;
import com.sencha.gxt.core.client.Style.SelectionMode;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.FieldSet;
import com.sencha.gxt.widget.core.client.form.SimpleComboBox;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.grid.CheckBoxSelectionModel;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;

public class FormEstacion extends SimpleForm{
	private static final PropertiesEstacion props = GWT
	.create(PropertiesEstacion.class);
	private ListStore<ElemEstacion> store;

	private HorizontalPanel pPrincipal;
	private VerticalPanel pContenedor;
	private VerticalPanel pTextField;
	private HorizontalPanel pBotones;
	private PanelCoord pCoordenadas;
	private List<String> listaCampos;	
	private List<List<String>> listaDatos;
	private GreetingServiceAsync fileService; 
	private TextField nro;
	
	public FormEstacion() {
		//Se cargan los datos de estaciones desde la BD
		obtenerDatos();
	}
	private void cargarFormulario(List<String> campanias, List<String> zonas){
		pTextField = new VerticalPanel();
		    
		FieldSet fieldSet = new FieldSet();
		fieldSet.setHeadingText("Ingreso de datos");
		fieldSet.setCollapsible(false);
		fieldSet.add(pTextField);
		
	    final LabelProvider<String> lb = new LabelProvider<String>() {

            @Override
            public String getLabel(String item) {
                return item;
            }

        };
        

		
	    final SimpleComboBox<String> combo_campanias = new SimpleComboBox<String>(lb);
	    for (String elem: campanias)
			combo_campanias.add(lb.getLabel(elem));
	    combo_campanias.setAllowTextSelection(false);
	    combo_campanias.setEditable(false);
	    combo_campanias.setEmptyText("Seleccionar campaña...");
	    combo_campanias.setTriggerAction(TriggerAction.ALL);

		pTextField.add(new FieldLabel(combo_campanias,"Campaña"));
		
		
		final SimpleComboBox<String> combo_zonas = new SimpleComboBox<String>(lb);
		for (String elem: zonas)
		combo_zonas.add(lb.getLabel(elem));
		combo_zonas.setAllowTextSelection(false);
		combo_zonas.setEditable(false);
		combo_zonas.setEmptyText("Seleccionar zona...");
		combo_zonas.setTriggerAction(TriggerAction.ALL);
		
		pTextField.add(new FieldLabel(combo_zonas,"Zona"));
		
		nro = new TextField();
		pTextField.add(new FieldLabel(nro, "Codigo estacion"));
		
		pBotones = new HorizontalPanel();

		pBotones.add(new TextButton("Guardar", new SelectHandler(){
		    @Override
		    public void onSelect(SelectEvent event){
		    	//Seteo de los validadores gráficos
//				MaxLengthValidator valid10 = new MaxLengthValidator(10);
//				valid10.setMessages(new LongitudMaxima());
//				MaxLengthValidator valid20 = new MaxLengthValidator(20);
//				valid20.setMessages(new LongitudMaxima());
//				EmptyValidator<String> validNulo = new EmptyValidator<String>();
//				validNulo.setMessages(new MensajeVacio());
//				codigo.addValidator(valid10);
//				codigo.addValidator(validNulo);
//				anio.addValidator(valid10);
//				anio.addValidator(validNulo);
//				nombre.addValidator(valid20);
//				nombre.addValidator(validNulo);
//
		    	listaCampos = new ArrayList<String>();
			    listaCampos.add(combo_campanias.getValue());
			    listaCampos.add(combo_zonas.getValue());
				listaCampos.add(nro.getValue().toString());

				guardarDatos();
		    	
		    }}));	
		    pBotones.add(new TextButton("Cancelar"));
		    
		    pContenedor = new VerticalPanel();
		    pContenedor.add(fieldSet);
		    pContenedor.add(new PanelCoord(1));
		    pContenedor.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		    pContenedor.add(pBotones);
		    this.add(pContenedor);
		    this.add(cargarCSV());
		    
    }
	
	//Se guardan los datos ingresados en el formulario validando su correctitud
	private void guardarDatos(){
				
			 	if (fileService == null)
			 		fileService = GWT.create(GreetingService.class);
				
				AsyncCallback<Void> callback1 = new AsyncCallback<Void>()     {
					@Override
					public void onFailure(Throwable caught) {
//						if (caught instanceof ExcepcionValidacion)
//							Window.alert("La longitud excede el maximo permitido");
						
						try {
					         throw caught;
					       } catch (ExcepcionLongitud e) {
					         //Excepcion ya que el campo excede el maximo admitido 
					    	   Window.alert("El campo" +e.getCampo()+" excede la longitud máxima");
					       } catch (ExcepcionNulo e) {
					    	   //Excepcion porque el campo no admite nulos
					    	   Window.alert("El campo "+e.getCampo()+" no puede ser nulo");
					       } catch (ExcepcionTipoCampo e) {
					    	   //Excepcion cuando el tipo de dato esperado para el campo no coincide con el ingresado
					    	   Window.alert("El tipo del campo es incorrecto");
					       } catch (IOException e) {
					    	   //Excepcion que ocurre cuando hay un error de E/S
					    	   Window.alert("Ocurrio un error al leer el archivo");
					       } catch (ExcepcionValidacion e) {
					    	   //Excepcion que ocurre cuando se da un error de validacion general
					    	   Window.alert("Ocurrio error de validación: "+e.getError());
					       } catch (Exception e) {
					    	   //Excepcion que ocurre cuando falla la comunación con el servidor
					    	   Window.alert("Ocurrio error de comunicación con el servidor ");
					       } catch (Throwable e) {
					    	   //Error general
								Window.alert("Ocurrio un Error");
						} 
					}


					@Override
					public void onSuccess(Void result) {
						Window.alert("Datos guardados");
						
					}	
				};
				fileService.guardarEstacion(listaCampos,callback1);
	}
	
	//Metodo encargado de cargar datos necesarios en combos mediante RPC
	private void obtenerDatos (){
		if (fileService==null)
			fileService = GWT.create(GreetingService.class);

		ParallelCallback<List<String>> Callback0 = new ParallelCallback<List<String>>();  
		ParallelCallback<List<String>> Callback1 = new ParallelCallback<List<String>>();
		
		//final ParentCallback parent = new ParentCallback(Callback0, Callback1) {
		ParentCallback parent = new ParentCallback
					(Callback0,
					 Callback1)
		{  
			public void handleSuccess() 
			{  
				cargarFormulario((List<String>)getCallbackData(0),
						(List<String>)getCallbackData(1));
			}
		};  		
		fileService.ObtenerCampanias(Callback0);//---
		fileService.ObtenerZonas(Callback1);				
		}

	//Metodo que lee el archivo .csv validando los datos ingresados
	protected void cargarArchivo(String filePath) {
		if(fileService==null)  
			GWT.create(GreetingService.class);

		AsyncCallback<List<List<String>>> callback1 = new AsyncCallback<List<List<String>>>() {
			public void onFailure(Throwable caught) {
			     try {
			         throw caught;
			       } catch (ExcepcionLongitud e) {
			         // this client is not compatible with the server; cleanup and refresh the 
			         // browser
			    	   Window.alert("El campo excede la longitud máxima");
			       } catch (ExcepcionNulo e) {
			    	   Window.alert("El  campo no puede ser nulo");
			         // the call didn't complete cleanly
			       } catch (ExcepcionTipoCampo e) {
			    	   Window.alert("El tipo del campo es incorrecto");
			         // one of the 'throws' from the original method
			       } catch (IOException e) {
			    	   Window.alert("Ocurrio un error al leer el archivo");
			         // one of the 'throws' from the original method
			       } catch (ExcepcionValidacion e) {
			    	   Window.alert("Ocurrio error de validación"+e.getError());
			         // last resort -- a very unexpected exception
			       } catch (Throwable e) {
			    	   Window.alert("Ocurrio error al procesar el archivo");
					// TODO Auto-generated catch block
		//	    	   e.printStackTrace();
				}
		
				//Window.alert("Error al leer archivo");
			}
		
			@Override
			public void onSuccess(List<List<String>> result) {
		
				for (int i = 0; i < result.size(); i++) {
					ElemEstacion estacionNueva = new ElemEstacion();
		
					List<String> lt = result.get(i);
					estacionNueva.setCampania_id(lt.get(0));
					estacionNueva.setZona_id(lt.get(1));
					estacionNueva.setCodigo_estacion(lt.get(2));
					estacionNueva.setLat(lt.get(3));
					estacionNueva.setLon(lt.get(4));
						
					store.add(0,estacionNueva);
					
				}
				listaDatos = result;
			}
		};
		fileService
				.readFileZona(Configs.FILE_UPLOAD_PATH + filePath,"ESTACIONES",callback1);
				
	}
	
	public void guardarDatosArchivo(String tabla){
		if (fileService == null) 
			fileService = GWT.create(GreetingService.class);
		
		AsyncCallback<Void> callback1 = new AsyncCallback<Void>()     {
			public void onFailure(Throwable caught) {
				try {
					throw caught;
				} catch (ExcepcionValidacion e){
					Window.alert("Ocurrio un error de validacion: "+e.getError());
				} catch (Throwable e){
					Window.alert("Ocurrio un error al cargar datos al servidor");
				}
			}

			@Override
			public void onSuccess(Void result) {
				Window.alert("Datos guardados");
				
			}	
		};
		fileService.guardarDatosArchivo(tabla, listaDatos,callback1);
	}
	
	//Metodo que carga el formulario para la carga desde un archivo .csv
	private Widget cargarCSV (){
		FlowPanel panel = new FlowPanel();

		panel.add(new TextButton("Reset", new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				store.rejectChanges();
				store.clear();
			}
		}));
		
		panel.add(new TextButton("Guardar", new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				store.commitChanges();
				guardarDatosArchivo("ESTACIONES");
			}
		}));

		panel.add(uploadLabel());
		panel.add(initTablaEstacion());
		return panel.asWidget();
	}
	
	//Metodo que inicia la tabla correspondiente
	private Widget initTablaEstacion() {
		List<ColumnConfig<ElemEstacion, ?>> tabla = new ArrayList<ColumnConfig<ElemEstacion, ?>>();
		IdentityValueProvider<ElemEstacion> identity = new IdentityValueProvider<ElemEstacion>();
		CheckBoxSelectionModel<ElemEstacion> sm = new CheckBoxSelectionModel<ElemEstacion>(identity);
		sm.setSelectionMode(SelectionMode.SINGLE);
		tabla.add(sm.getColumn());

		 ColumnConfig<ElemEstacion, String> col1 = new ColumnConfig<ElemEstacion, String>(
					props.codigo_estacion(), 3, "Codigo Estacion");
		 ColumnConfig<ElemEstacion, String> col2 = new ColumnConfig<ElemEstacion, String>(
				props.campania_id(), 3, "Id Camapaña");
		 ColumnConfig<ElemEstacion, String> col3 = new ColumnConfig<ElemEstacion, String>(
				props.zona_id(), 3, "Id Zona");
		 ColumnConfig<ElemEstacion, String> col4 = new ColumnConfig<ElemEstacion, String>(
				props.lat(), 3, "Latitud");
		 ColumnConfig<ElemEstacion, String> col5 = new ColumnConfig<ElemEstacion, String>(
				props.lon(), 3, "Longitud");
		

		tabla.add(col1);
		tabla.add(col2);
		tabla.add(col3);
		tabla.add(col4);
		tabla.add(col5);
		
		ColumnModel<ElemEstacion> cm = new ColumnModel<ElemEstacion>(tabla);
		
		store = new ListStore<ElemEstacion>(props.key());

		Grid<ElemEstacion> grid = new Grid<ElemEstacion>(store, cm);
		grid.getView().setForceFit(true);
		grid.setLoadMask(true);
		grid.setBorders(true);
		grid.getView().setAutoExpandColumn(col1);
		grid.getView().setAutoExpandColumn(col2);
		grid.getView().setAutoExpandColumn(col3);
		grid.getView().setAutoExpandColumn(col4);
		grid.getView().setAutoExpandColumn(col5);
		grid.getView().setColumnLines(true);
		grid.setSelectionModel(sm);
		grid.addStyleName(Configs.CSS_UPLOADTABLE);
		
		ContentPanel contentPanel = new ContentPanel();
		contentPanel.setHeaderVisible(false);
		contentPanel.add(grid);
		contentPanel.forceLayout();
		return contentPanel.asWidget();
		

	}


}
