package org.saoproject.web.client.loaders.form.formularios;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.saoproject.web.client.Configs;
import org.saoproject.web.client.GreetingService;
import org.saoproject.web.client.GreetingServiceAsync;
import org.saoproject.web.client.loaders.form.LongitudMaxima;
import org.saoproject.web.client.loaders.form.MensajeVacio;
import org.saoproject.web.client.loaders.form.FormViejos.FormCSV;
import org.saoproject.web.client.loaders.form.elementos.ElemCampania;
import org.saoproject.web.client.loaders.form.propertiesAccess.PropertiesCampania;
import org.saoproject.web.shared.Tipos;
import org.saoproject.web.shared.exc.ExcepcionLongitud;
import org.saoproject.web.shared.exc.ExcepcionNulo;
import org.saoproject.web.shared.exc.ExcepcionTipoCampo;
import org.saoproject.web.shared.exc.ExcepcionValidacion;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell.TriggerAction;
import com.sencha.gxt.core.client.IdentityValueProvider;
import com.sencha.gxt.core.client.Style.SelectionMode;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.FieldSet;
import com.sencha.gxt.widget.core.client.form.FormPanel;
import com.sencha.gxt.widget.core.client.form.SimpleComboBox;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.form.validator.EmptyValidator;
import com.sencha.gxt.widget.core.client.form.validator.MaxLengthValidator;
import com.sencha.gxt.widget.core.client.form.validator.MaxLengthValidator.MaxLengthMessages;
import com.sencha.gxt.widget.core.client.grid.CheckBoxSelectionModel;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
	
public class FormCampania extends SimpleForm{	 
	private static final PropertiesCampania props = GWT
		.create(PropertiesCampania.class);
	private ListStore<ElemCampania> store;

	private VerticalPanel pContenedor;
	private VerticalPanel pTextField;
	private HorizontalPanel pBotones;
	private List<String> listaCampos;	
	private List<List<String>> listaDatos;
	private TextField codigo, anio, nombre, nro;
	private TextField fecha;
	private GreetingServiceAsync fileService; 
	
	public FormCampania() {
		//Se cargan los datos de estaciones desde la BD
		obtenerDatos();
	}
	private void cargarFormulario(List<String> estaciones){
		pTextField = new VerticalPanel();
		    
		FieldSet fieldSet = new FieldSet();
		fieldSet.setHeadingText("Ingreso de datos");
		fieldSet.setCollapsible(false);
		fieldSet.add(pTextField);
		
//		fp.setWidget(pContenedor);
		/*
		 * Creacion y seteo de los validadores para el formulario
		 */
//		MaxLengthValidator valid10 = new MaxLengthValidator(10);
//		valid10.setMessages(new LongitudMaxima());
//		MaxLengthValidator valid20 = new MaxLengthValidator(20);
//		valid20.setMessages(new LongitudMaxima());
//		EmptyValidator<String> validNulo = new EmptyValidator<String>();
//		validNulo.setMessages(new MensajeVacio());
		
	    final LabelProvider<String> lb = new LabelProvider<String>() {

            @Override
            public String getLabel(String item) {
                return item;
            }

        };
        

		
		codigo = new TextField();
//		codigo.setEmptyText("Codigo...");
//		codigo.addValidator(new Validator<String>() {
//		        @Override
//		        public String validate(Field<?> field, String value) {
//		            return value.matches("[\\sa-zA-Z.'-àáâãäåçèéêëìíîïðòóôõöùúûüýÿ]+") 
//		                                 ? null : "not a valid value";
//		        }
//		    });
//		codigo.addValidator(valid10);
//		codigo.addValidator(validNulo);
		pTextField.add(codigo);
		pTextField.add(new FieldLabel(codigo, "Codigo"));
		 
		anio = new TextField();
//		anio.addValidator(valid10);
//		anio.addValidator(validNulo);
		pTextField.add(new FieldLabel(anio, "Año"));
		    
		nombre = new TextField();
//		nombre.addValidator(valid20);
//		nombre.addValidator(validNulo);
		pTextField.add(new FieldLabel(nombre, "Nombre campaña"));

		
	    final SimpleComboBox<String> combo_tipoCamp = new SimpleComboBox<String>(lb);
	    for (String elem: Tipos.tipo_campania)
			combo_tipoCamp.add(lb.getLabel(elem));
	    combo_tipoCamp.setAllowTextSelection(false);
	    combo_tipoCamp.setEditable(false);
	    combo_tipoCamp.setEnabled(true);
		combo_tipoCamp.setValue(lb.getLabel(Tipos.tipo_campania.get(0)));
	    combo_tipoCamp.setTriggerAction(TriggerAction.ALL);

		pTextField.add(new FieldLabel(combo_tipoCamp,"Tipo campaña"));
		
		
		final SimpleComboBox<String> combo_tipoMuestreo = new SimpleComboBox<String>(lb);
		for (String elem: Tipos.tipo_muestreo)
			combo_tipoMuestreo.add(lb.getLabel(elem));
		combo_tipoMuestreo.setAllowTextSelection(false);
		combo_tipoMuestreo.setEditable(false);
		combo_tipoMuestreo.setEnabled(true);
		combo_tipoMuestreo.setValue(lb.getLabel(Tipos.tipo_muestreo.get(0)));
		combo_tipoMuestreo.setTriggerAction(TriggerAction.ALL);
		
		pTextField.add(new FieldLabel(combo_tipoMuestreo,"Tipo muestreo"));
		
		nro = new TextField();
		pTextField.add(new FieldLabel(nro, "Cantidad estaciones"));

		fecha = new TextField();
		pTextField.add(new FieldLabel(fecha, "Fecha"));
		
		final SimpleComboBox<String> combo_estaciones = new SimpleComboBox<String>(lb);
		for (String elem: estaciones)
			combo_estaciones.add(lb.getLabel(elem));
		combo_tipoMuestreo.setAllowTextSelection(false);
		combo_tipoMuestreo.setEditable(false);
		combo_tipoMuestreo.setEnabled(true);
		combo_tipoMuestreo.setValue(lb.getLabel(Tipos.tipo_muestreo.get(0)));
		combo_tipoMuestreo.setTriggerAction(TriggerAction.ALL);

		pTextField.add(new FieldLabel(combo_estaciones,"Estaciones"));
		
		
		pBotones = new HorizontalPanel();

		pBotones.add(new TextButton("Guardar", new SelectHandler(){
		    @Override
		    public void onSelect(SelectEvent event){
		    	//Seteo de los validadores gráficos
				MaxLengthValidator valid10 = new MaxLengthValidator(10);
				valid10.setMessages(new LongitudMaxima());
				MaxLengthValidator valid20 = new MaxLengthValidator(20);
				valid20.setMessages(new LongitudMaxima());
				EmptyValidator<String> validNulo = new EmptyValidator<String>();
				validNulo.setMessages(new MensajeVacio());
				codigo.addValidator(valid10);
				codigo.addValidator(validNulo);
				anio.addValidator(valid10);
				anio.addValidator(validNulo);
				nombre.addValidator(valid20);
				nombre.addValidator(validNulo);
				combo_tipoMuestreo.addValidator(validNulo);
			    combo_tipoCamp.addValidator(validNulo);

		    	listaCampos = new ArrayList<String>();
			    listaCampos.add(codigo.getValue().toString());
			    listaCampos.add(anio.getValue().toString());
			    listaCampos.add(nombre.getValue().toString());
     		    listaCampos.add(combo_tipoCamp.getValue());
			    listaCampos.add(combo_tipoMuestreo.getValue());
				listaCampos.add(nro.getValue().toString());
				listaCampos.add(fecha.getValue().toString());					

				guardarDatos();
		    	
		    }}));	
		    pBotones.add(new TextButton("Cancelar"));
		    
		    pContenedor = new VerticalPanel();
		    pContenedor.add(fieldSet);
		    pContenedor.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		    pContenedor.add(pBotones);
		    this.add(pContenedor);
		    this.add(cargarCSV());
		    
    }
	
	//Se guardan los datos ingresados en el formulario validando su correctitud
	private void guardarDatos(){
				
			 	if (fileService == null)
			 		fileService = GWT.create(GreetingService.class);
				
				AsyncCallback<Void> callback1 = new AsyncCallback<Void>()     {
					@Override
					public void onFailure(Throwable caught) {
//						if (caught instanceof ExcepcionValidacion)
//							Window.alert("La longitud excede el maximo permitido");
						
						try {
					         throw caught;
					       } catch (ExcepcionLongitud e) {
					         //Excepcion ya que el campo excede el maximo admitido 
					    	   Window.alert("El campo" +e.getCampo()+" excede la longitud máxima");
					       } catch (ExcepcionNulo e) {
					    	   //Excepcion porque el campo no admite nulos
					    	   Window.alert("El campo "+e.getCampo()+" no puede ser nulo");
					       } catch (ExcepcionTipoCampo e) {
					    	   //Excepcion cuando el tipo de dato esperado para el campo no coincide con el ingresado
					    	   Window.alert("El tipo del campo es incorrecto");
					       } catch (IOException e) {
					    	   //Excepcion que ocurre cuando hay un error de E/S
					    	   Window.alert("Ocurrio un error al leer el archivo");
					       } catch (ExcepcionValidacion e) {
					    	   //Excepcion que ocurre cuando se da un error de validacion general
					    	   Window.alert("Ocurrio error de validación: "+e.getError());
					       } catch (Exception e) {
					    	   //Excepcion que ocurre cuando falla la comunación con el servidor
					    	   Window.alert("Ocurrio error de comunicación con el servidor ");
					       } catch (Throwable e) {
					    	   //Error general
								Window.alert("Ocurrio un Error");
						} 
					}


					@Override
					public void onSuccess(Void result) {
						Window.alert("Datos guardados");
						
					}	
				};
				fileService.guardarCampania(listaCampos,callback1);
	}
	
	//Metodo encargado de cargar datos necesarios en combos mediante RPC
	private void obtenerDatos (){
		if (fileService==null)
			fileService = GWT.create(GreetingService.class);

			AsyncCallback<List<String>> callback1 = new AsyncCallback<List<String>>(){
				@Override
				public void onFailure(Throwable caught) {
				}

				@Override
				public void onSuccess(List<String> result) {
					cargarFormulario(result);
				}	
			};
			fileService.ObtenerEstaciones(callback1);
		}

	//Metodo que lee el archivo .csv validando los datos ingresados
	protected void cargarArchivo(String filePath) {
		if(fileService==null)  
			GWT.create(GreetingService.class);

		AsyncCallback<List<List<String>>> callback1 = new AsyncCallback<List<List<String>>>() {
			public void onFailure(Throwable caught) {
			     try {
			         throw caught;
			       } catch (ExcepcionLongitud e) {
			         // this client is not compatible with the server; cleanup and refresh the 
			         // browser
			    	   Window.alert("El campo excede la longitud máxima");
			       } catch (ExcepcionNulo e) {
			    	   Window.alert("El  campo no puede ser nulo");
			         // the call didn't complete cleanly
			       } catch (ExcepcionTipoCampo e) {
			    	   Window.alert("El tipo del campo es incorrecto");
			         // one of the 'throws' from the original method
			       } catch (IOException e) {
			    	   Window.alert("Ocurrio un error al leer el archivo");
			         // one of the 'throws' from the original method
			       } catch (ExcepcionValidacion e) {
			    	   Window.alert("Ocurrio error de validación"+e.getError());
			         // last resort -- a very unexpected exception
			       } catch (Throwable e) {
			    	   Window.alert("Ocurrio error al procesar el archivo");
					// TODO Auto-generated catch block
		//	    	   e.printStackTrace();
				}
		
				//Window.alert("Error al leer archivo");
			}
		
			@Override
			public void onSuccess(List<List<String>> result) {
		
				for (int i = 0; i < result.size(); i++) {
					ElemCampania campNueva = new ElemCampania();
		
					List<String> lt = result.get(i);
					campNueva.setCodigo(lt.get(0));
					campNueva.setAnio(lt.get(1));
					campNueva.setNombre(lt.get(2));
					campNueva.setTipo_campania(lt.get(3));
					campNueva.setTipo_muestreo(lt.get(4));
					campNueva.setNro_estaciones(lt.get(5));
					campNueva.setFecha(lt.get(6));
						
					store.add(0,campNueva);
					
				}
				listaDatos = result;
			}
		};
		fileService
				.readFileZona(Configs.FILE_UPLOAD_PATH + filePath,"CAMPANIAS",callback1);
				
	}
	
	public void guardarDatosArchivo(String tabla){
		if (fileService == null) 
			fileService = GWT.create(GreetingService.class);
		
		AsyncCallback<Void> callback1 = new AsyncCallback<Void>()     {
			public void onFailure(Throwable caught) {
				try {
					throw caught;
				} catch (ExcepcionValidacion e){
					Window.alert("Ocurrio un error de validacion: "+e.getError());
				} catch (Throwable e){
					Window.alert("Ocurrio un error al cargar datos al servidor");
				}
			}

			@Override
			public void onSuccess(Void result) {
				Window.alert("Datos guardados");
				
			}	
		};
		fileService.guardarDatosArchivo(tabla, listaDatos,callback1);
	}
	
	//Metodo que carga el formulario para la carga desde un archivo .csv
	private Widget cargarCSV (){
		FlowPanel panel = new FlowPanel();

		panel.add(new TextButton("Reset", new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				store.rejectChanges();
				store.clear();
			}
		}));
		
		panel.add(new TextButton("Guardar", new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				store.commitChanges();
				guardarDatosArchivo("CAMPANIAS");
			}
		}));

		panel.add(uploadLabel());
		panel.add(initTablaCampania());
		return panel.asWidget();
	}
	
	//Metodo que inicia la tabla correspondiente
	private Widget initTablaCampania() {
		List<ColumnConfig<ElemCampania, ?>> tabla = new ArrayList<ColumnConfig<ElemCampania, ?>>();
		IdentityValueProvider<ElemCampania> identity = new IdentityValueProvider<ElemCampania>();
		CheckBoxSelectionModel<ElemCampania> sm = new CheckBoxSelectionModel<ElemCampania>(identity);
		sm.setSelectionMode(SelectionMode.MULTI);
		tabla.add(sm.getColumn());

		 ColumnConfig<ElemCampania, String> col1 = new ColumnConfig<ElemCampania, String>(
				props.codigo(), 3, "Código");
		
		 ColumnConfig<ElemCampania, String> col2 = new ColumnConfig<ElemCampania, String>(
				props.anio(), 3, "Año ");
		 ColumnConfig<ElemCampania, String> col3 = new ColumnConfig<ElemCampania, String>(
				props.nombre(), 3, "Nombre");
		 ColumnConfig<ElemCampania, String> col4 = new ColumnConfig<ElemCampania, String>(
				props.tipo_campania(), 3, "Tipo");
		 ColumnConfig<ElemCampania, String> col5 = new ColumnConfig<ElemCampania, String>(
				props.tipo_muestreo(), 3, "Tipo Muestreo");
		 ColumnConfig<ElemCampania, String> col6 = new ColumnConfig<ElemCampania, String>(
				props.nro_estaciones(), 3, "Número estaciones");
		 ColumnConfig<ElemCampania, String> col7 = new ColumnConfig<ElemCampania, String>(
				props.fecha(), 3, "Fecha campaña");
		

		tabla.add(col1);
		tabla.add(col2);
		tabla.add(col3);
		tabla.add(col4);
		tabla.add(col5);
		tabla.add(col6);
		tabla.add(col7);
		
		ColumnModel<ElemCampania> cm = new ColumnModel<ElemCampania>(tabla);
		
		store = new ListStore<ElemCampania>(props.key());

		Grid<ElemCampania> grid = new Grid<ElemCampania>(store, cm);
		grid.getView().setForceFit(true);
		grid.setLoadMask(true);
		grid.setBorders(true);
		grid.getView().setAutoExpandColumn(col1);
		grid.getView().setAutoExpandColumn(col2);
		grid.getView().setAutoExpandColumn(col3);
		grid.getView().setAutoExpandColumn(col4);
		grid.getView().setAutoExpandColumn(col5);
		grid.getView().setAutoExpandColumn(col6);
		grid.getView().setAutoExpandColumn(col7);
		grid.getView().setColumnLines(true);
		grid.setSelectionModel(sm);
		grid.addStyleName(Configs.CSS_UPLOADTABLE);
		
		ContentPanel contentPanel = new ContentPanel();
		contentPanel.setHeaderVisible(false);
		contentPanel.add(grid);
		contentPanel.forceLayout();
		return contentPanel.asWidget();
		

	}

}