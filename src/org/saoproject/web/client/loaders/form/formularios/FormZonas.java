package org.saoproject.web.client.loaders.form.formularios;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.saoproject.web.client.Configs;
import org.saoproject.web.client.GreetingService;
import org.saoproject.web.client.GreetingServiceAsync;
import org.saoproject.web.client.loaders.form.LongitudMaxima;
import org.saoproject.web.client.loaders.form.MensajeVacio;
import org.saoproject.web.client.loaders.form.FormViejos.FormCSV;
import org.saoproject.web.client.loaders.form.elementos.ElemCampania;
import org.saoproject.web.client.loaders.form.elementos.ElemZona;
import org.saoproject.web.client.loaders.form.propertiesAccess.PropertiesCampania;
import org.saoproject.web.client.loaders.form.propertiesAccess.PropertiesZona;
import org.saoproject.web.shared.Tipos;
import org.saoproject.web.shared.exc.ExcepcionLongitud;
import org.saoproject.web.shared.exc.ExcepcionNulo;
import org.saoproject.web.shared.exc.ExcepcionTipoCampo;
import org.saoproject.web.shared.exc.ExcepcionValidacion;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell.TriggerAction;
import com.sencha.gxt.core.client.IdentityValueProvider;
import com.sencha.gxt.core.client.Style.SelectionMode;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.FieldSet;
import com.sencha.gxt.widget.core.client.form.FormPanel;
import com.sencha.gxt.widget.core.client.form.SimpleComboBox;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.form.validator.EmptyValidator;
import com.sencha.gxt.widget.core.client.form.validator.MaxLengthValidator;
import com.sencha.gxt.widget.core.client.form.validator.MaxLengthValidator.MaxLengthMessages;
import com.sencha.gxt.widget.core.client.grid.CheckBoxSelectionModel;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;

public class FormZonas extends SimpleForm{
	private static final PropertiesZona props = GWT
	.create(PropertiesZona.class);
private ListStore<ElemZona> store;

private VerticalPanel pContenedor;
private VerticalPanel pTextField;
private HorizontalPanel pBotones;
private List<String> listaCampos;	
private List<List<String>> listaDatos;
private TextField codigo, descripcion;
private GreetingServiceAsync fileService; 

public FormZonas() {
	//Se cargan los datos de estaciones desde la BD
	obtenerDatos();
}
private void cargarFormulario(List<String> estaciones){
	pTextField = new VerticalPanel();
	    
	FieldSet fieldSet = new FieldSet();
	fieldSet.setHeadingText("Ingreso de datos");
	fieldSet.setCollapsible(false);
	fieldSet.add(pTextField);
	
//	fp.setWidget(pContenedor);
	/*
	 * Creacion y seteo de los validadores para el formulario
	 */
//	MaxLengthValidator valid10 = new MaxLengthValidator(10);
//	valid10.setMessages(new LongitudMaxima());
//	MaxLengthValidator valid20 = new MaxLengthValidator(20);
//	valid20.setMessages(new LongitudMaxima());
//	EmptyValidator<String> validNulo = new EmptyValidator<String>();
//	validNulo.setMessages(new MensajeVacio());
    

	
	codigo = new TextField();
//	codigo.setEmptyText("Codigo...");
//	codigo.addValidator(new Validator<String>() {
//	        @Override
//	        public String validate(Field<?> field, String value) {
//	            return value.matches("[\\sa-zA-Z.'-àáâãäåçèéêëìíîïðòóôõöùúûüýÿ]+") 
//	                                 ? null : "not a valid value";
//	        }
//	    });
//	codigo.addValidator(valid10);
//	codigo.addValidator(validNulo);
	pTextField.add(codigo);
	pTextField.add(new FieldLabel(codigo, "Codigo"));
	 
	descripcion = new TextField();
//	anio.addValidator(valid10);
//	anio.addValidator(validNulo);
	pTextField.add(new FieldLabel(descripcion, "Descripcion"));
	    
	
	
	pBotones = new HorizontalPanel();

	pBotones.add(new TextButton("Guardar", new SelectHandler(){
	    @Override
	    public void onSelect(SelectEvent event){
	    	//Seteo de los validadores gráficos
			MaxLengthValidator valid10 = new MaxLengthValidator(10);
			valid10.setMessages(new LongitudMaxima());
			MaxLengthValidator valid20 = new MaxLengthValidator(20);
			valid20.setMessages(new LongitudMaxima());
			EmptyValidator<String> validNulo = new EmptyValidator<String>();
			validNulo.setMessages(new MensajeVacio());
			codigo.addValidator(valid10);
			codigo.addValidator(validNulo);


	    	listaCampos = new ArrayList<String>();
		    listaCampos.add(codigo.getValue().toString());
		    listaCampos.add(descripcion.getValue().toString());

			guardarDatos();
	    	
	    }}));	
	    pBotones.add(new TextButton("Cancelar"));
	    
	    pContenedor = new VerticalPanel();
	    pContenedor.add(fieldSet);
	    pContenedor.add(new PanelCoord(6));
	    pContenedor.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
	    pContenedor.add(pBotones);
	    this.add(pContenedor);
	    this.add(cargarCSV());
	    
}

//Se guardan los datos ingresados en el formulario validando su correctitud
private void guardarDatos(){
			
		 	if (fileService == null)
		 		fileService = GWT.create(GreetingService.class);
			
			AsyncCallback<Void> callback1 = new AsyncCallback<Void>()     {
				@Override
				public void onFailure(Throwable caught) {
//					if (caught instanceof ExcepcionValidacion)
//						Window.alert("La longitud excede el maximo permitido");
					
					try {
				         throw caught;
				       } catch (ExcepcionLongitud e) {
				         //Excepcion ya que el campo excede el maximo admitido 
				    	   Window.alert("El campo" +e.getCampo()+" excede la longitud máxima");
				       } catch (ExcepcionNulo e) {
				    	   //Excepcion porque el campo no admite nulos
				    	   Window.alert("El campo "+e.getCampo()+" no puede ser nulo");
				       } catch (ExcepcionTipoCampo e) {
				    	   //Excepcion cuando el tipo de dato esperado para el campo no coincide con el ingresado
				    	   Window.alert("El tipo del campo es incorrecto");
				       } catch (IOException e) {
				    	   //Excepcion que ocurre cuando hay un error de E/S
				    	   Window.alert("Ocurrio un error al leer el archivo");
				       } catch (ExcepcionValidacion e) {
				    	   //Excepcion que ocurre cuando se da un error de validacion general
				    	   Window.alert("Ocurrio error de validación: "+e.getError());
				       } catch (Exception e) {
				    	   //Excepcion que ocurre cuando falla la comunación con el servidor
				    	   Window.alert("Ocurrio error de comunicación con el servidor ");
				       } catch (Throwable e) {
				    	   //Error general
							Window.alert("Ocurrio un Error");
					} 
				}


				@Override
				public void onSuccess(Void result) {
					Window.alert("Datos guardados");
					
				}	
			};
			fileService.guardarZona(listaCampos,callback1);
}

//Metodo encargado de cargar datos necesarios en combos mediante RPC
private void obtenerDatos (){
	if (fileService==null)
		fileService = GWT.create(GreetingService.class);

		AsyncCallback<List<String>> callback1 = new AsyncCallback<List<String>>(){
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(List<String> result) {
				cargarFormulario(result);
			}	
		};
		fileService.ObtenerEstaciones(callback1);
	}

//Metodo que lee el archivo .csv validando los datos ingresados
protected void cargarArchivo(String filePath) {
	if(fileService==null)  
		GWT.create(GreetingService.class);

	AsyncCallback<List<List<String>>> callback1 = new AsyncCallback<List<List<String>>>() {
		public void onFailure(Throwable caught) {
		     try {
		         throw caught;
		       } catch (ExcepcionLongitud e) {
		         // this client is not compatible with the server; cleanup and refresh the 
		         // browser
		    	   Window.alert("El campo excede la longitud máxima");
		       } catch (ExcepcionNulo e) {
		    	   Window.alert("El  campo no puede ser nulo");
		         // the call didn't complete cleanly
		       } catch (ExcepcionTipoCampo e) {
		    	   Window.alert("El tipo del campo es incorrecto");
		         // one of the 'throws' from the original method
		       } catch (IOException e) {
		    	   Window.alert("Ocurrio un error al leer el archivo");
		         // one of the 'throws' from the original method
		       } catch (ExcepcionValidacion e) {
		    	   Window.alert("Ocurrio error de validación"+e.getError());
		         // last resort -- a very unexpected exception
		       } catch (Throwable e) {
		    	   Window.alert("Ocurrio error al procesar el archivo");
				// TODO Auto-generated catch block
	//	    	   e.printStackTrace();
			}
	
			//Window.alert("Error al leer archivo");
		}
	
		@Override
		public void onSuccess(List<List<String>> result) {
	
			for (int i = 0; i < result.size(); i++) {
				ElemZona zonaNueva = new ElemZona();
	
				List<String> lt = result.get(i);
				zonaNueva.setCodigo(lt.get(0));
				zonaNueva.setName(lt.get(1));
				zonaNueva.setLat1(lt.get(2));
				zonaNueva.setLon1(lt.get(3));
				zonaNueva.setLat2(lt.get(4));
				zonaNueva.setLon2(lt.get(5));
				zonaNueva.setLat3(lt.get(6));
				zonaNueva.setLon3(lt.get(7));
				zonaNueva.setLat4(lt.get(8));
				zonaNueva.setLon4(lt.get(9));
				zonaNueva.setLat5(lt.get(10));
				zonaNueva.setLon5(lt.get(11));
				zonaNueva.setLat6(lt.get(12));
				zonaNueva.setLon6(lt.get(13));
					
				store.add(0,zonaNueva);
				
			}
			listaDatos = result;
		}
	};
	fileService
			.readFileZona(Configs.FILE_UPLOAD_PATH + filePath,"ZONAS",callback1);
			
}

public void guardarDatosArchivo(String tabla){
	if (fileService == null) 
		fileService = GWT.create(GreetingService.class);
	
	AsyncCallback<Void> callback1 = new AsyncCallback<Void>()     {
		public void onFailure(Throwable caught) {
			try {
				throw caught;
			} catch (ExcepcionValidacion e){
				Window.alert("Ocurrio un error de validacion: "+e.getError());
			} catch (Throwable e){
				Window.alert("Ocurrio un error al cargar datos al servidor");
			}
		}

		@Override
		public void onSuccess(Void result) {
			Window.alert("Datos guardados");
			
		}	
	};
	fileService.guardarDatosArchivo(tabla, listaDatos,callback1);
}

//Metodo que carga el formulario para la carga desde un archivo .csv
private Widget cargarCSV (){
	FlowPanel panel = new FlowPanel();

	panel.add(new TextButton("Reset", new SelectHandler() {

		@Override
		public void onSelect(SelectEvent event) {
			store.rejectChanges();
			store.clear();
		}
	}));
	
	panel.add(new TextButton("Guardar", new SelectHandler() {

		@Override
		public void onSelect(SelectEvent event) {
			store.commitChanges();
			guardarDatosArchivo("ZONAS");
		}
	}));

	panel.add(uploadLabel());
	panel.add(initTablaZona());
	return panel.asWidget();
}

//Metodo que inicia la tabla correspondiente
private Widget initTablaZona() {
	List<ColumnConfig<ElemZona, ?>> tabla = new ArrayList<ColumnConfig<ElemZona, ?>>();
	IdentityValueProvider<ElemZona> identity = new IdentityValueProvider<ElemZona>();
	CheckBoxSelectionModel<ElemZona> sm = new CheckBoxSelectionModel<ElemZona>(identity);
	sm.setSelectionMode(SelectionMode.MULTI);
	tabla.add(sm.getColumn());

	 ColumnConfig<ElemZona, String> col1 = new ColumnConfig<ElemZona, String>(
			props.codigo(), 3, "Código");
	
	 ColumnConfig<ElemZona, String> col2 = new ColumnConfig<ElemZona, String>(
			props.name(), 3, "Descripcion ");
	
	 ColumnConfig<ElemZona, String> col3 = new ColumnConfig<ElemZona, String>(
				props.lat1(), 3, "Lat1");
	 ColumnConfig<ElemZona, String> col4 = new ColumnConfig<ElemZona, String>(
				props.lon1(), 3, "Lon1");
	 ColumnConfig<ElemZona, String> col5 = new ColumnConfig<ElemZona, String>(
				props.lat2(), 3, "Lat2");
	 ColumnConfig<ElemZona, String> col6 = new ColumnConfig<ElemZona, String>(
				props.lon2(), 3, "Lon2");
	 ColumnConfig<ElemZona, String> col7 = new ColumnConfig<ElemZona, String>(
				props.lat3(), 3, "Lat3");
	 ColumnConfig<ElemZona, String> col8 = new ColumnConfig<ElemZona, String>(
				props.lon3(), 3, "Lon3");
	 ColumnConfig<ElemZona, String> col9 = new ColumnConfig<ElemZona, String>(
				props.lat4(), 3, "Lat4");
	 ColumnConfig<ElemZona, String> col10 = new ColumnConfig<ElemZona, String>(
				props.lon4(), 3, "Lon4");
	 ColumnConfig<ElemZona, String> col11 = new ColumnConfig<ElemZona, String>(
				props.lat5(), 3, "Lat5");
	 ColumnConfig<ElemZona, String> col12 = new ColumnConfig<ElemZona, String>(
				props.lon5(), 3, "Lon5");
	 ColumnConfig<ElemZona, String> col13 = new ColumnConfig<ElemZona, String>(
				props.lat6(), 3, "Lat6");
	 ColumnConfig<ElemZona, String> col14 = new ColumnConfig<ElemZona, String>(
				props.lon6(), 3, "Lon6");

	tabla.add(col1);
	tabla.add(col2);
	tabla.add(col3);
	tabla.add(col4);
	tabla.add(col5);
	tabla.add(col6);
	tabla.add(col7);
	tabla.add(col8);
	tabla.add(col9);
	tabla.add(col10);
	tabla.add(col11);
	tabla.add(col12);
	tabla.add(col13);
	tabla.add(col14);
	
	ColumnModel<ElemZona> cm = new ColumnModel<ElemZona>(tabla);
	
	store = new ListStore<ElemZona>(props.key());

	Grid<ElemZona> grid = new Grid<ElemZona>(store, cm);
	grid.getView().setForceFit(true);
	grid.setLoadMask(true);
	grid.setBorders(true);
	grid.getView().setAutoExpandColumn(col1);
	grid.getView().setAutoExpandColumn(col2);
	grid.getView().setColumnLines(true);
	grid.setSelectionModel(sm);
	grid.addStyleName(Configs.CSS_UPLOADTABLE);
	
	ContentPanel contentPanel = new ContentPanel();
	contentPanel.setHeaderVisible(false);
	contentPanel.add(grid);
	contentPanel.forceLayout();
	return contentPanel.asWidget();
	

}

}
