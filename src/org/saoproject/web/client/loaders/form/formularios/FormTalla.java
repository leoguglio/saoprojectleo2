package org.saoproject.web.client.loaders.form.formularios;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.saoproject.web.client.Configs;
import org.saoproject.web.client.GreetingService;
import org.saoproject.web.client.GreetingServiceAsync;
import org.saoproject.web.client.callbacks.ParallelCallback;
import org.saoproject.web.client.callbacks.ParentCallback;
import org.saoproject.web.client.loaders.form.elementos.ElemEstacion;
import org.saoproject.web.client.loaders.form.elementos.ElemTalla;
import org.saoproject.web.client.loaders.form.propertiesAccess.PropertiesTalla;
import org.saoproject.web.shared.Tipos;
import org.saoproject.web.shared.exc.ExcepcionLongitud;
import org.saoproject.web.shared.exc.ExcepcionNulo;
import org.saoproject.web.shared.exc.ExcepcionTipoCampo;
import org.saoproject.web.shared.exc.ExcepcionValidacion;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell.TriggerAction;
import com.sencha.gxt.core.client.IdentityValueProvider;
import com.sencha.gxt.core.client.Style.SelectionMode;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.FieldSet;
import com.sencha.gxt.widget.core.client.form.SimpleComboBox;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.grid.CheckBoxSelectionModel;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;

public class FormTalla extends SimpleForm{
	private static final PropertiesTalla props = GWT
		.create(PropertiesTalla.class);
	private ListStore<ElemTalla> store;
	
	private VerticalPanel pContenedor;
	private VerticalPanel pFieldSet;
	private HorizontalPanel pBotones;
	private List<String> listaCampos;
	private List<List<String>> listaDatos;
	private GreetingServiceAsync greetService;
	private TextField codigo,nro_talla,cantidad;
	
	public FormTalla() {
			obtenerDatos();

	}
	
	
	private void createFormTalla (List<String> camp, List<String> zonas, List<String> estaciones, List<String> especies){
		
//		campania_id
//		zona_id
//		estacion_id
//		especia_id
//		nrotalla
//		cantidad
		pFieldSet = new VerticalPanel();
	    
	    FieldSet fieldSet = new FieldSet();
	    fieldSet.setHeadingText("Ingreso de datos");
	    fieldSet.setCollapsible(false);
	    fieldSet.add(pFieldSet);
	    
	    LabelProvider<String> lb = new LabelProvider<String>() {

            @Override
            public String getLabel(String item) {
                return item;
            }

        };

	    codigo = new TextField();
	    codigo.setAllowBlank(false);
	    pFieldSet.add(new FieldLabel(codigo, "ID"));

        //COMBO DE SELECCION DE CAMPAÑAS
        final SimpleComboBox<String> combo_campanias = new SimpleComboBox<String>(lb);

	    for (String elem:camp){
	    	combo_campanias.add(lb.getLabel(elem));
	    }
	    combo_campanias.setEmptyText("Seleccionar campaña...");
		combo_campanias.setAllowTextSelection(false);
		combo_campanias.setEditable(false);
		combo_campanias.setTriggerAction(TriggerAction.ALL);

	    combo_campanias.setAllowTextSelection(false);
	    pFieldSet.add(combo_campanias);
	    pFieldSet.add(new FieldLabel(combo_campanias,"Campaña"));
	 
	    //COMBO DE SELECCION DE ZONAS
	    final SimpleComboBox<String> combo_zonas = new SimpleComboBox<String>(lb);
	    
	    for (String elem:zonas){
	    	combo_zonas.add(lb.getLabel(elem));
	    }
	    combo_zonas.setEmptyText("Seleccionar zona...");
		combo_zonas.setAllowTextSelection(false);
		combo_zonas.setEditable(false);
		combo_zonas.setTriggerAction(TriggerAction.ALL);

	    pFieldSet.add(new FieldLabel(combo_zonas, "Zona"));

	    //COMBO DE SELECCION DE ESTACIONES
	    final SimpleComboBox<String> combo_estaciones = new SimpleComboBox<String>(lb);
	    
	    for (String elem:estaciones){
	    	combo_estaciones.add(lb.getLabel(elem));
	    }
	    combo_estaciones.setEmptyText("Seleccionar estacion...");
		combo_estaciones.setAllowTextSelection(false);
		combo_estaciones.setEditable(false);
		combo_estaciones.setTriggerAction(TriggerAction.ALL);

	    pFieldSet.add(new FieldLabel(combo_estaciones, "Estación"));

	    //COMBO DE SELECCION DE ESPECIES
	    SimpleComboBox<String> combo_especies = new SimpleComboBox<String>(lb);
	    
	    for (String elem:especies){
	    	combo_especies.add(lb.getLabel(elem));
	    }
	    combo_especies.setEmptyText("Seleccionar especie...");
		combo_especies.setAllowTextSelection(false);
		combo_especies.setEditable(false);
		combo_especies.setTriggerAction(TriggerAction.ALL);

	    pFieldSet.add(new FieldLabel(combo_especies, "Especie"));

	    nro_talla = new TextField();
	    nro_talla.setAllowBlank(true);
	    pFieldSet.add(new FieldLabel(nro_talla, "Número talla"));

	    cantidad = new TextField();
	    cantidad.setAllowBlank(true);
	    pFieldSet.add(new FieldLabel(cantidad, "Cantidad"));
	    
	    pBotones = new HorizontalPanel();
		pBotones.add(new TextButton("Guardar", new SelectHandler(){
		    @Override
		    public void onSelect(SelectEvent event){
		    	//Seteo de los validadores gráficos
//				MaxLengthValidator valid10 = new MaxLengthValidator(10);
//				valid10.setMessages(new LongitudMaxima());
//				MaxLengthValidator valid20 = new MaxLengthValidator(20);
//				valid20.setMessages(new LongitudMaxima());
//				EmptyValidator<String> validNulo = new EmptyValidator<String>();
//				validNulo.setMessages(new MensajeVacio());
//				codigo.addValidator(valid10);
//				codigo.addValidator(validNulo);
//				anio.addValidator(valid10);
//				anio.addValidator(validNulo);
//				nombre.addValidator(valid20);
//				nombre.addValidator(validNulo);
//
		    	listaCampos = new ArrayList<String>();
			    listaCampos.add(codigo.getValue().toString());
			    listaCampos.add(combo_campanias.getValue().toString());
			    listaCampos.add(combo_estaciones.getValue().toString());
     		    listaCampos.add(combo_zonas.getValue());
			    listaCampos.add(nro_talla.getValue().toString());
     		    listaCampos.add(cantidad.getValue());

				guardarDatos();
		    	
		    }}));	
	    pBotones.add(new TextButton("Cancelar"));
	    
	    pContenedor = new VerticalPanel();
	    pContenedor.add(fieldSet);
	    pContenedor.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
	    pContenedor.add(pBotones);
	    this.add(pContenedor);
	    this.add(cargarCSV());
	}
	
	private void obtenerDatos(){
		ParallelCallback<List<String>> Callback0 = new ParallelCallback<List<String>>();  
		ParallelCallback<List<String>> Callback1 = new ParallelCallback<List<String>>();
		ParallelCallback<List<String>> Callback2 = new ParallelCallback<List<String>>();
		ParallelCallback<List<String>> Callback3 = new ParallelCallback<List<String>>();
		if (greetService == null)
			greetService = GWT.create(GreetingService.class);
		
		//final ParentCallback parent = new ParentCallback(Callback0, Callback1) {
		ParentCallback parent = new ParentCallback
					(Callback0,
					 Callback1,
					 Callback2,
					 Callback3)
		{  
			public void handleSuccess() 
			{  
				createFormTalla((List<String>)getCallbackData(0),
						(List<String>)getCallbackData(1),
						(List<String>)getCallbackData(2), 
						(List<String>)getCallbackData(3));
			}
		};  		
		greetService.ObtenerCampanias(Callback0);//---
		greetService.ObtenerZonas(Callback1);	
		greetService.ObtenerEstaciones(Callback2);
		greetService.ObtenerEspecies(Callback3);			
	}

	//Se guardan los datos ingresados en el formulario validando su correctitud
	private void guardarDatos(){
				
			 	if (greetService == null)
			 		greetService = GWT.create(GreetingService.class);
				
				AsyncCallback<Void> callback1 = new AsyncCallback<Void>()     {
					@Override
					public void onFailure(Throwable caught) {
//						if (caught instanceof ExcepcionValidacion)
//							Window.alert("La longitud excede el maximo permitido");
						
						try {
					         throw caught;
					       } catch (ExcepcionLongitud e) {
					         //Excepcion ya que el campo excede el maximo admitido 
					    	   Window.alert("El campo" +e.getCampo()+" excede la longitud máxima");
					       } catch (ExcepcionNulo e) {
					    	   //Excepcion porque el campo no admite nulos
					    	   Window.alert("El campo "+e.getCampo()+" no puede ser nulo");
					       } catch (ExcepcionTipoCampo e) {
					    	   //Excepcion cuando el tipo de dato esperado para el campo no coincide con el ingresado
					    	   Window.alert("El tipo del campo es incorrecto");
					       } catch (IOException e) {
					    	   //Excepcion que ocurre cuando hay un error de E/S
					    	   Window.alert("Ocurrio un error al leer el archivo");
					       } catch (ExcepcionValidacion e) {
					    	   //Excepcion que ocurre cuando se da un error de validacion general
					    	   Window.alert("Ocurrio error de validación: "+e.getError());
					       } catch (Exception e) {
					    	   //Excepcion que ocurre cuando falla la comunación con el servidor
					    	   Window.alert("Ocurrio error de comunicación con el servidor ");
					       } catch (Throwable e) {
					    	   //Error general
								Window.alert("Ocurrio un Error");
						} 
					}


					@Override
					public void onSuccess(Void result) {
						Window.alert("Datos guardados");
						
					}	
				};
				greetService.guardarTalla(listaCampos,callback1);
	}
	
	protected void cargarArchivo(String filePath) {
		if(greetService==null)  
			GWT.create(GreetingService.class);

		AsyncCallback<List<List<String>>> callback1 = new AsyncCallback<List<List<String>>>() {
			public void onFailure(Throwable caught) {
			     try {
			         throw caught;
			       } catch (ExcepcionLongitud e) {
			         // this client is not compatible with the server; cleanup and refresh the 
			         // browser
			    	   Window.alert("El campo excede la longitud máxima");
			       } catch (ExcepcionNulo e) {
			    	   Window.alert("El  campo no puede ser nulo");
			         // the call didn't complete cleanly
			       } catch (ExcepcionTipoCampo e) {
			    	   Window.alert("El tipo del campo es incorrecto");
			         // one of the 'throws' from the original method
			       } catch (IOException e) {
			    	   Window.alert("Ocurrio un error al leer el archivo");
			         // one of the 'throws' from the original method
			       } catch (ExcepcionValidacion e) {
			    	   Window.alert("Ocurrio error de validación"+e.getError());
			         // last resort -- a very unexpected exception
			       } catch (Throwable e) {
			    	   Window.alert("Ocurrio error al procesar el archivo");
					// TODO Auto-generated catch block
		//	    	   e.printStackTrace();
				}
		
				//Window.alert("Error al leer archivo");
			}
		
			@Override
			public void onSuccess(List<List<String>> result) {
		
				for (int i = 0; i < result.size(); i++) {
					ElemTalla tallaNueva = new ElemTalla();
		
					List<String> lt = result.get(i);
					tallaNueva.setCampania_id(lt.get(0));
					tallaNueva.setZona_id(lt.get(1));
					tallaNueva.setEstacion_id(lt.get(2));
					tallaNueva.setEspecie_id(lt.get(3));
					tallaNueva.setNroTalla(lt.get(4));
					tallaNueva.setCantidad(lt.get(5));
						
					store.add(0,tallaNueva);
					
				}
				listaDatos = result;
			}
		};
		greetService
				.readFileZona(Configs.FILE_UPLOAD_PATH + filePath,"TALLAS",callback1);
				
	}
	
	public void guardarDatosArchivo(String tabla){
		if (greetService == null) 
			greetService = GWT.create(GreetingService.class);
		
		AsyncCallback<Void> callback1 = new AsyncCallback<Void>()     {
			public void onFailure(Throwable caught) {
				try {
					throw caught;
				} catch (ExcepcionValidacion e){
					Window.alert("Ocurrio un error de validacion: "+e.getError());
				} catch (Throwable e){
					Window.alert("Ocurrio un error al cargar datos al servidor");
				}
			}

			@Override
			public void onSuccess(Void result) {
				Window.alert("Datos guardados");
				
			}	
		};
		greetService.guardarDatosArchivo(tabla, listaDatos,callback1);
	}
	
	//Metodo que carga el formulario para la carga desde un archivo .csv
	private Widget cargarCSV (){
		FlowPanel panel = new FlowPanel();

		panel.add(new TextButton("Reset", new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				store.rejectChanges();
				store.clear();
			}
		}));
		
		panel.add(new TextButton("Guardar", new SelectHandler() {

			@Override
			public void onSelect(SelectEvent event) {
				store.commitChanges();
				guardarDatosArchivo("TALLAS");
			}
		}));

		panel.add(uploadLabel());
		panel.add(initTablaEstacion());
		return panel.asWidget();
	}
	
	//Metodo que inicia la tabla correspondiente
	private Widget initTablaEstacion() {
		List<ColumnConfig<ElemTalla, ?>> tabla = new ArrayList<ColumnConfig<ElemTalla, ?>>();
		IdentityValueProvider<ElemTalla> identity = new IdentityValueProvider<ElemTalla>();
		CheckBoxSelectionModel<ElemTalla> sm = new CheckBoxSelectionModel<ElemTalla>(identity);
		sm.setSelectionMode(SelectionMode.SINGLE);
		tabla.add(sm.getColumn());

		 ColumnConfig<ElemTalla, String> col1 = new ColumnConfig<ElemTalla, String>(
					props.id_talla(), 3, "ID");
		 ColumnConfig<ElemTalla, String> col2 = new ColumnConfig<ElemTalla, String>(
				props.campania_id(), 3, "ID Camapaña");
		 ColumnConfig<ElemTalla, String> col3 = new ColumnConfig<ElemTalla, String>(
				props.zona_id(), 3, "ID Zona");
		 ColumnConfig<ElemTalla, String> col4 = new ColumnConfig<ElemTalla, String>(
				props.estacion_id(), 3, "ID Estacion");
		 ColumnConfig<ElemTalla, String> col5 = new ColumnConfig<ElemTalla, String>(
				props.nroTalla(), 3, "Nro. talla");
		 ColumnConfig<ElemTalla, String> col6 = new ColumnConfig<ElemTalla, String>(
					props.cantidad(), 3, "Cantidad");
		

		tabla.add(col1);
		tabla.add(col2);
		tabla.add(col3);
		tabla.add(col4);
		tabla.add(col5);
		
		ColumnModel<ElemTalla> cm = new ColumnModel<ElemTalla>(tabla);
		
		store = new ListStore<ElemTalla>(props.key());

		Grid<ElemTalla> grid = new Grid<ElemTalla>(store, cm);
		grid.getView().setForceFit(true);
		grid.setLoadMask(true);
		grid.setBorders(true);
		grid.getView().setAutoExpandColumn(col1);
		grid.getView().setAutoExpandColumn(col2);
		grid.getView().setAutoExpandColumn(col3);
		grid.getView().setAutoExpandColumn(col4);
		grid.getView().setAutoExpandColumn(col5);
		grid.getView().setAutoExpandColumn(col6);
		grid.getView().setColumnLines(true);
		grid.setSelectionModel(sm);
		grid.addStyleName(Configs.CSS_UPLOADTABLE);
		
		ContentPanel contentPanel = new ContentPanel();
		contentPanel.setHeaderVisible(false);
		contentPanel.add(grid);
		contentPanel.forceLayout();
		return contentPanel.asWidget();
		

	}


}
