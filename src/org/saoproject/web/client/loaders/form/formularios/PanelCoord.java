package org.saoproject.web.client.loaders.form.formularios;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.sencha.gxt.widget.core.client.form.TextField;

public class PanelCoord extends FlowPanel{
	private HorizontalPanel horizP;
	public PanelCoord(int cantidad){
		horizP = new HorizontalPanel();
		for (int i=0;i<cantidad;i++)
			createCoord(i);
		this.add(horizP);
	}
	
	private void createCoord (int pos){
		int p = pos++;
		VerticalPanel vertP = new VerticalPanel();
		vertP.setWidth("20");
		TextField longitud = new TextField();
		longitud.setEmptyText("Latitud"+p);
		TextField latitud = new TextField();
		latitud.setEmptyText("Longitud"+p);
		vertP.add(longitud);
		vertP.add(latitud);
		horizP.add(vertP);
		
	}
}
