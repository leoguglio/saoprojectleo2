package org.saoproject.web.client;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.reveregroup.gwt.imagepreloader.ImageLoadEvent;
import com.reveregroup.gwt.imagepreloader.ImageLoadHandler;
import com.reveregroup.gwt.imagepreloader.ImagePreloader;

public class Banner extends FlowPanel
{

	public Banner(String empresa, String urlLogo )
	{
		super();
		Logger.getLogger("GIS-Logger").log(Level.INFO, "LogoUrl:"+urlLogo);
		
		ImagePreloader.load(urlLogo, new ImageLoadHandler() 
		{
		    public void imageLoaded(ImageLoadEvent event)
		    {
		        if (event.isLoadFailed())
		       	    Logger.getLogger("Banner").log(Level.SEVERE,"No se cargo la imagen"+event.getImageUrl());		       
				else
				{
					Logger.getLogger("Banner").log(Level.INFO, "[InLoader]"+event);
					Logger.getLogger("Banner").log(Level.INFO, "[InLoader]"+event.getImageUrl());					
					Logger.getLogger("Banner").log(Level.INFO, "[InLoader]"+event.getDimensions());
					Logger.getLogger("Banner").log(Level.INFO, "[InLoader]"+event.getDimensions().getWidth()+"px"+ ","+event.getDimensions().getHeight()+"px");
					setTamanio(event.getDimensions().getWidth()+"px", event.getDimensions().getHeight()+"px");
				}
		    }
		});
		
	}

	protected void setTamanio(String width, String height) 
	{
		this.getElement().setAttribute("style","width:" +width +"; height:"+height);	 
	}	

}
