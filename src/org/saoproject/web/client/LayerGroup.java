package org.saoproject.web.client;

import java.util.ArrayList;
import java.util.List;

import org.gwtopenmaps.openlayers.client.layer.WFS;
import org.gwtopenmaps.openlayers.client.layer.WFSParams;
import org.project.giisco.wfs.client.WFSManipulation;




public class LayerGroup {

	private String idItem;
	private String idGroup;
	private WFS layer;
	//private String zone;
	
	public LayerGroup()
	{

	}
	
	public String getGroup()
    {
	    	return idGroup;
	}
	
	public String getItem()
    {
	    	return idItem;
	}
	
	public WFS getLayer()
    {
	    	return layer;
	}
	
	
	
	public List<LayerGroup> setLayersGroup(List<WFSManipulation> listaWfs, String clave, WFSParams params, int indiceGroup)
	{

		@SuppressWarnings("unused")
		String idZona;
		String grupo="";
		
		WFSManipulation wfsm= new WFSManipulation();
		List<LayerGroup> listaLayer= new ArrayList<LayerGroup>();
		LayerGroup layerg;
		for (int i=0; i< listaWfs.size();i++)
		{
			
			
		   	wfsm = listaWfs.get(i);
		   	
		 
			if (wfsm.getName().indexOf(clave)>-1)
			{
				layerg= new LayerGroup();
				//Se busca el grupo al que pertenece
				
				layerg.idItem =wfsm.getName();
				
				grupo = wfsm.getName().substring(indiceGroup);
				layerg.idGroup=grupo.substring(0, grupo.indexOf("_"));
				layerg.layer = wfsm.setWfsLayer(params);								
				// Se obtiene la id de zona asociada BO, ES BF CLL
				idZona= wfsm.getName().substring(wfsm.getName().indexOf("_")+1, wfsm.getName().indexOf("_"));
			
									
			    listaLayer.add(layerg);	
					
			}
		}

		return listaLayer;
		
		
		
	}
	
	
	
	
	
}
