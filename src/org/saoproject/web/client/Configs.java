package org.saoproject.web.client;

public class Configs {


		//TOdos los codigos CSS estan en el archivo SaoProjectUI.css en el directorio war
	public static String CSS_DOCKPANEL 			= "map-dock-panel";
	public static String CSS_COLORBUTTONITEM		= "color-button-item";
	public static String CSS_SCROLLITEM			= "scroll-item";
	public static String CSS_BODYCONTAINER 		= "body-container";
	public static String CSS_FOLDERPPAL 			= "folderppal";
	public static String CSS_GLOBALCONTAINER 		= "global-container";
	public static String CSS_TOPCONTAINER 			= "header-container";
	public static String CSS_LEFTBARCONTAINER 		= "leftbar-container";    
	public static String CSS_MAPCONTAINER 		 	= "map-container";
	public static String CSS_ACORDIONCONTAINER 	= "acordion";
 	public static String CSS_SCROLL_CAPAS 			= "scroll-panel";
	public static String CSS_BANNER				= "bannerStyle";
	public static String CSS_TABMAPA				= "tab-mapa";
	public static String CSS_TABTABLAS				= "tab-tablas";
	public static String CSS_TABHISTOGRAMAS		= "tab-histogramas";
	public static String CSS_TOOLBARHISTOGRAMAS	= "barra-histogramas";
	public static String CSS_FLOATLEFT				= "float-left";
	public static String CSS_UNSELECTABLE			= "unselectable";
		
	// Web Designer Recommended Width, based on screen size statistics
	public static int    SIZE_RECOMENDEDWIDTH		= 968;
	public static String CSS_SCROLLPANELITEMTEXT	= "scroll-panel-item-text";
	public static String CSS_BUTTONCOLORITEM		= "scroll-panel-item-button";
	
	//Debug Constants
	public static String DEBUG_MAIN_LOGGER_NAME	= "GIS-Log";
	



}
