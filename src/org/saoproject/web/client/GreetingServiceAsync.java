package org.saoproject.web.client;

import java.util.List;

import org.datos.tallas.client.DatosTallas;
import org.gwtopenmaps.openlayers.client.layer.WFS;
import org.project.giisco.wfs.client.WFSManipulation;

import com.google.gwt.user.client.rpc.AsyncCallback;




/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface GreetingServiceAsync {
	//void crearMapa(String width, String height, int numZoom, String projection,
	//		double minX, double minY, double maxX, double maxY,
//			@SuppressWarnings("rawtypes") AsyncCallback callback);
	void obtenerUrl(String layer, AsyncCallback<String> callback);
	void obtenerWFS (String url,String workSpace,AsyncCallback<List<WFSManipulation>> callback);
	//void ObtenerTallas(String campania, String zona,String estacion, String especie,AsyncCallback<ResultSet> callback);
	void ObtenerCampanias(AsyncCallback<List<String>> callback);
	void ObtenerZonas(AsyncCallback<List<String>> callback);
	void ObtenerListadoZonas(AsyncCallback<List<String>> callback);

	void ObtenerZona(String idZona, AsyncCallback<List<String>> callback);
	void ObtenerEspecies(AsyncCallback<List<String>> callback);
	void ObtenerEstaciones(AsyncCallback<List<String>> callback);
	void ObtenerCampania(String idCamp, String codigoZona, AsyncCallback<List<String>> callback);
	void ObtenerCampaniaCenpat(String idCamp, AsyncCallback<List<String>> callback);
	void leerArchivoExcel(String archivoDestino, AsyncCallback<Void> callback);
	void ObtenerDistribucion(String Campana, String zona, int cantidad,
			AsyncCallback<List<String>> callback);	
	void callServer(String callMensaje,String[] callParametros, AsyncCallback<List<WFSManipulation>> callback);
	void callServerString(String callMensaje, String[] callParametros, AsyncCallback<List<String>> callback);
	void callServerTallas(String callMensaje, String[] callParametros, AsyncCallback<List<DatosTallas>> callback);
	void ObtenerTallas(String campania, String zona, String especie,
			String estacion, AsyncCallback<List<DatosTallas>> callback);
	void ObtenerZonasCampania(String campania,
			AsyncCallback<List<String>> callback);
	void ObtenerEstacionesCampania(String campania, String zona,
			AsyncCallback<List<String>> callback);
	void ObtenerEspeciesCampania(String campania, String zona, String estacion,
			AsyncCallback<List<String>> callback);
	void obtenerWFSA(String url, String workSpace,
			AsyncCallback<List<WFS>> callback);
	void readFileZona(String string, String tabla, AsyncCallback<List<List<String>>> callback1);
	void guardarDatosArchivo(String tabla, List<List<String>> datos, AsyncCallback<Void> callback);
	void guardarCampania(List<String> datos, AsyncCallback<Void> callback); 
	void guardarEstacion(List<String> datos, AsyncCallback<Void>  callback);
	void guardarZona(List<String> datos, AsyncCallback<Void> callback);
	void guardarEspecie(List<String> datos, AsyncCallback<Void> callback);
	void guardarTalla(List<String> datos, AsyncCallback<Void> callback);
	void guardarDatosBio(List<String> datos, AsyncCallback<Void> callback);

}