package org.saoproject.web.client.exporters;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.saoproject.web.client.Configs;
import org.saoproject.web.client.SaoProjectUI;

import com.google.gwt.user.client.Element;


public class PdfExporter
{

	public PdfExporter()
	{
		String elemento=SaoProjectUI.getMapElemet();
		export(elemento);
	}
	
	
	public void export(String html)
	{
		Logger.getLogger(Configs.DEBUG_MAIN_LOGGER_NAME).log
		(Level.WARNING, "Creating pdfExporter: ");
		 if (html == null ) {
	            throw new RuntimeException("Debe Pasar al menos una capa");
	     }
		 else
		 {
		 
	     	Logger.getLogger("PdfExporter").log
	        		(Level.INFO, html);	    	
	    	//LIBRERIA PRINTI!! Probar
	    	Print.it(html);
		 }
	    
	}
	
	public void export(Element elemento)
	{
		Logger.getLogger(Configs.DEBUG_MAIN_LOGGER_NAME).log
		(Level.WARNING, "Creating pdfExporter: ");
		 if (elemento == null ) {
	            throw new RuntimeException("Debe Pasar al menos una capa");
	     }
		 else
		 {
		 
	     	Logger.getLogger("PdfExporter").log
	        		(Level.INFO, elemento.getClassName());	    	
	    	//LIBRERIA PRINTI!! Probar
	    	Print.it(elemento.getStyle().toString(),elemento);
		 }
	    
	}
	
	/*public void export(Element... elementos)
	{
		Logger.getLogger(Configs.DEBUG_MAIN_LOGGER_NAME).log
		(Level.WARNING, "Creating pdfExporter: ");
		 if (elementos == null || elementos.length == 0) {
	            throw new RuntimeException("Debe Pasar al menos una capa");
	       }

	    for (Element elemento : elementos)
	    {
	        	Logger.getLogger("PdfExporter").log
	        		(Level.INFO, elemento.getClassName());	        
	        	
	        	//ÑIBRERIA PRINTI!! Probar
	        	Print.it(elemento);
	        	
	    }
	}*/

}
