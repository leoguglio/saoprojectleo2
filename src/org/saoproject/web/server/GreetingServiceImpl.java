package org.saoproject.web.server;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;




import org.datos.tallas.client.DatosTallas;
import org.ecologysubdomain.ejb.Tallas;
import org.ecologysubdomain.ejb.TallasBeanRemote;
import org.ecologysubdomian.ejb.BasicWFS;
import org.ecologysubdomian.ejb.LayerBeanRemote;

import org.gwtopenmaps.openlayers.client.layer.WFS;

import org.project.distribution.distributionRemote;
import org.project.giisco.wfs.client.WFSManipulation;
import org.saoproject.web.client.*;
import org.saoproject.web.server.altas.Altas;
import org.saoproject.web.server.altas.Campania;
import org.saoproject.web.server.loaders.ManejoArchivoCSV;
import org.saoproject.web.server.loaders.Tablas;
import org.saoproject.web.server.loaders.Tupla;
import org.saoproject.web.server.loaders.Validador;
import org.saoproject.web.shared.Direcciones;
import org.saoproject.web.shared.Retorno;
import org.saoproject.web.shared.exc.ExcepcionLongitud;
import org.saoproject.web.shared.exc.ExcepcionNulo;
import org.saoproject.web.shared.exc.ExcepcionTipoCampo;
import org.saoproject.web.shared.exc.ExcepcionValidacion;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;


@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements
GreetingService {
	
	 public String URLDIR = Direcciones.URLDIRLAYER;
	 public String URLPROPS =Direcciones.URLPROPS;
	 
	 //BDconfig se setea con el nombre del archivo de configuración de los datos de la Base de DAtos
	 private String BDconf = "config.txt";


public 	List<WFSManipulation> callServer(String callMensaje, String[] callParametros) {
	
	
	if (callMensaje.equalsIgnoreCase("obtenerwfs"))
		return obtenerWFS(callParametros[0],callParametros[1]);
	
	
	return null;
	 }

public 	List<DatosTallas> callServerTallas(String callMensaje, String[] callParametros) {
	
	
	if (callMensaje.equalsIgnoreCase("ObtenerTallas"))
		return ObtenerTallas(callParametros[0],callParametros[1],callParametros[2],callParametros[3]);
	
	
	return null;
	 }




public List<String> callServerString(String callMensaje, String[] callParametros) {
	
	if (callMensaje.equalsIgnoreCase("ObtenerCampanias"))
		return ObtenerCampanias();
		
	if (callMensaje.equalsIgnoreCase("ObtenerZonas"))
		return ObtenerZonas();
	
	if (callMensaje.equalsIgnoreCase("ObtenerEspecies"))
		return ObtenerEspecies();
	
	if (callMensaje.equalsIgnoreCase("ObtenerEstaciones"))
		return ObtenerEstaciones();
	
	if (callMensaje.equalsIgnoreCase("ObtenerZona"))
		return ObtenerZona(callParametros[0]);
	
	if (callMensaje.equalsIgnoreCase("ObtenerCampania"))
		return ObtenerCampania(callParametros[0],callParametros[1]);
	
	if (callMensaje.equalsIgnoreCase("ObtenerListadoZonas"))
		return ObtenerListadoZonas();
	if (callMensaje.equalsIgnoreCase("ObtenerDistribucion"))
		return ObtenerDistribucion(callParametros[0],callParametros[1], Integer.parseInt(callParametros[2]));
	
	if (callMensaje.equalsIgnoreCase("ObtenerZonasCampania"))
		return ObtenerZonasCampania(callParametros[0]);
	if (callMensaje.equalsIgnoreCase("ObtenerEstacionesCampania"))
		return ObtenerEstacionesCampania(callParametros[0],callParametros[1]);
	if (callMensaje.equalsIgnoreCase("ObtenerEspeciesCampania"))
		return ObtenerEspeciesCampania(callParametros[0],callParametros[1],callParametros[2]);
	
	
	
	
		return null;
}
	 
	 
	 
  public String obtenerUrl(String layer)
  {
	  Properties props = new Properties();
     
      props.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
  	  props.put("java.naming.factory.url.pkgs", "-org.jboss.naming:org.jnp.interfaces");
  	  props.put("java.naming.provider.url", URLPROPS);
 
      String nombre="";
		try {
			Context ctx= new InitialContext(props);
			LayerBeanRemote layerBean= (LayerBeanRemote) ctx.lookup("LayerBean/remote");
			Object [] urls = layerBean.getServerURL();
			
			
			if (layer.equals("basic"))
			{
				
				return (String) urls[0];
			}
			else
			{
				return (String) urls[1];
			}
		
			
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return nombre;
	  
  }
  public List<WFS> obtenerWFSA(String url, String workSpace)
  {
		    return null;	  
  }
  
  public List<WFSManipulation> obtenerWFS(String url, String workSpace)
  {
	  //List<WFS> listLayers= new ArrayList<WFS>();
	  
	  //String workSpace="giscenpat";
	  List<WFSManipulation> listLayers= new ArrayList<WFSManipulation>();
	  WFSManipulation layerwfs;  
	  
	  Properties props = new Properties();	  
      props.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
  	  props.put("java.naming.factory.url.pkgs", "-org.jboss.naming:org.jnp.interfaces");
  	  props.put("java.naming.provider.url", URLPROPS);
 
		try {

			//Armar el WFS y devolverlo en una lista
			Context ctx= new InitialContext(props);
			LayerBeanRemote layerBean= (LayerBeanRemote) ctx.lookup("LayerBean/remote");
			List<BasicWFS> listBasicWFS= layerBean.GetWFS(url);
		    //Aca creo mis objectos WFS para despues ser cargados
			//LayerManipulation layer = new LayerManipulation();
			BasicWFS basic;
			
			basic = new BasicWFS();
			basic = listBasicWFS.get(0);
			
			for (int i=0; i<listBasicWFS.size(); i++)
			{
				basic = new BasicWFS();
				basic = listBasicWFS.get(i);
				
				if (basic.getTypeName().startsWith(workSpace)){
					layerwfs= new WFSManipulation();
					layerwfs.setName(basic.getName());
					layerwfs.setTypeName(basic.getTypeName());
					layerwfs.setfeatureNS(basic.getfeatureNS());
					layerwfs.setURL(URLDIR+"/geoserver/wfs");
				   	listLayers.add(layerwfs);
				}
					
			}
			
			return listLayers;
			
			
		    } catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  
	  
	  
		    return listLayers;
  		}
  
  public Retorno obtenerWFS2(String url, String workSpace)
  {
	  //List<WFS> listLayers= new ArrayList<WFS>();
	  
	  //String workSpace="giscenpat";
	  List<WFSManipulation> listLayers= new ArrayList<WFSManipulation>();
	  WFSManipulation layerwfs;
      
	  
	  
	  Properties props = new Properties();
	  
      props.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
  	  props.put("java.naming.factory.url.pkgs", "-org.jboss.naming:org.jnp.interfaces");
  	  props.put("java.naming.provider.url", URLPROPS);
 
		try {

			//Armar el WFS y devolverlo en una lista
			Context ctx= new InitialContext(props);
			LayerBeanRemote layerBean= (LayerBeanRemote) ctx.lookup("LayerBean/remote");
			List<BasicWFS> listBasicWFS= layerBean.GetWFS(url);
		    //Aca creo mis objectos WFS para despues ser cargados
			//LayerManipulation layer = new LayerManipulation();
			BasicWFS basic;
			
			basic = new BasicWFS();
			basic = listBasicWFS.get(0);
			
			for (int i=0; i<listBasicWFS.size(); i++)
			{
				basic = new BasicWFS();
				basic = listBasicWFS.get(i);
				
				if (basic.getTypeName().startsWith(workSpace)){
					layerwfs= new WFSManipulation();
					layerwfs.setName(basic.getName());
					layerwfs.setTypeName(basic.getTypeName());
					layerwfs.setfeatureNS(basic.getfeatureNS());
					layerwfs.setURL(URLDIR+"/geoserver/wfs");
				   	listLayers.add(layerwfs);
				}
					
			}
			
			return new Retorno( listLayers);
			
			
		    } catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  
	  
	  
		    return new Retorno( listLayers);
  		}
  
 
  
 public  List<String> ObtenerCampanias()
 {
	 Properties props = new Properties();
	 props.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
     props.put("java.naming.factory.url.pkgs", "-org.jboss.naming:org.jnp.interfaces");
     props.put("java.naming.provider.url", URLPROPS);
     List<String> listaC = new ArrayList<String>();
     try {
			
			Context ctx= new InitialContext(props);
			TallasBeanRemote tallaBean= (TallasBeanRemote) ctx.lookup("TallasBean/remote");
			listaC= tallaBean.ObtenerCampanias(BDconf);	
			
			
				
			
			
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		
		
		return listaC;
     
	 
 }
 
 public  List<String> ObtenerZonas()
 {
	 Properties props = new Properties();
	 props.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
     props.put("java.naming.factory.url.pkgs", "-org.jboss.naming:org.jnp.interfaces");
     props.put("java.naming.provider.url", URLPROPS);
     List<String> listaZ = new ArrayList<String>();
     try {
			
			Context ctx= new InitialContext(props);
			TallasBeanRemote tallaBean= (TallasBeanRemote) ctx.lookup("TallasBean/remote");
			listaZ= tallaBean.ObtenerZonas(BDconf);	
			
     
     } catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return listaZ;
     
	 
 }
 public List<String> ObtenerZona(String idZona)
 {
	 Properties props = new Properties();
	 props.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
     props.put("java.naming.factory.url.pkgs", "-org.jboss.naming:org.jnp.interfaces");
     props.put("java.naming.provider.url", URLPROPS);
     List<String> resultado= null;
     try {
			
			Context ctx= new InitialContext(props);
			TallasBeanRemote tallaBean= (TallasBeanRemote) ctx.lookup("TallasBean/remote");
			resultado =  (List<String>) tallaBean.ObtenerZona(idZona,BDconf);	
			
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	return resultado;
 }
 
 public List<String> ObtenerCampania(String idCamp, String idZona)
 {
	 Properties props = new Properties();
	 props.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
     props.put("java.naming.factory.url.pkgs", "-org.jboss.naming:org.jnp.interfaces");
     props.put("java.naming.provider.url", URLPROPS);
     List<String> resultado= null;
     try {
			
			Context ctx= new InitialContext(props);
			TallasBeanRemote tallaBean= (TallasBeanRemote) ctx.lookup("TallasBean/remote");
			resultado =  tallaBean.ObtenerCampania(idCamp,idZona,BDconf);	
			
			
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
				
	return resultado;
 }
 
 
public List<String> ObtenerCampaniaCenpat(String idCamp)
 {
	 Properties props = new Properties();
	 props.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
     props.put("java.naming.factory.url.pkgs", "-org.jboss.naming:org.jnp.interfaces");
     props.put("java.naming.provider.url", URLPROPS);
     List<String> resultado = null;
     double inv =0;
     try {
			
			Context ctx= new InitialContext(props);
			TallasBeanRemote tallaBean= (TallasBeanRemote) ctx.lookup("TallasBean/remote");
			resultado = tallaBean.ObtenerCampania("CCVI-96","",BDconf);
			
			inv =  tallaBean.ObtenerPromedioBiomasa("invierno", "codium",BDconf);	
				
			
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	return resultado;
 }
 
 public  List<String> ObtenerEspecies()
 {
	 Properties props = new Properties();
	 props.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
     props.put("java.naming.factory.url.pkgs", "-org.jboss.naming:org.jnp.interfaces");
     props.put("java.naming.provider.url", URLPROPS);
     List<String> lista = new ArrayList<String>();
     try {
			
			Context ctx= new InitialContext(props);
			TallasBeanRemote tallaBean= (TallasBeanRemote) ctx.lookup("TallasBean/remote");
			lista= tallaBean.ObtenerEspecies(BDconf);	
			
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return lista;
     
	 
 }
 
 public  List<String> ObtenerEstaciones()
 {
	 Properties props = new Properties();
	 props.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
     props.put("java.naming.factory.url.pkgs", "-org.jboss.naming:org.jnp.interfaces");
     props.put("java.naming.provider.url", URLPROPS);
     List<String> listaE = new ArrayList<String>();
     try {
			
			Context ctx= new InitialContext(props);
			TallasBeanRemote tallaBean= (TallasBeanRemote) ctx.lookup("TallasBean/remote");
			listaE= tallaBean.ObtenerEstaciones(BDconf);	
			
			
				
			
			
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return listaE;
     
	 
 }
 public  List<String> ObtenerListadoZonas()
 {
	 Properties props = new Properties();
	 props.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
     props.put("java.naming.factory.url.pkgs", "-org.jboss.naming:org.jnp.interfaces");
     props.put("java.naming.provider.url", URLPROPS);
     List<String> listaZ = new ArrayList<String>();
     try {
			
			Context ctx= new InitialContext(props);
			TallasBeanRemote tallaBean= (TallasBeanRemote) ctx.lookup("TallasBean/remote");
			listaZ= tallaBean.ObtenerListadoZonas(BDconf);	
			
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return listaZ;
     
	 
 }
 public  void leerArchivoExcel(String archivoDestino) { 

	   /* try { 
	    Workbook archivoExcel = Workbook.getWorkbook(new File( 
	    archivoDestino)); 
	    System.out.println("Número de Hojas\t" 
	    + archivoExcel.getNumberOfSheets()); 
	    for (int sheetNo = 0; sheetNo < archivoExcel.getNumberOfSheets(); sheetNo++) // Recorre 
	    // cada    
	    // hoja                                                                                                                                                       
	    { 
	    Sheet hoja = archivoExcel.getSheet(sheetNo); 
	    int numColumnas = hoja.getColumns(); 
	    int numFilas = hoja.getRows(); 
	    String data; 
	    System.out.println("Nombre de la Hoja\t" 
	    + archivoExcel.getSheet(sheetNo).getName()); 
	    for (int fila = 0; fila < numFilas; fila++) { // Recorre cada 
	    // fila de la 
	    // hoja 
	    for (int columna = 0; columna < numColumnas; columna++) { // Recorre                                                                                
	    // cada                                                                                
	    // columna                                                                            
	    // de                                                                                
	    // la                                                                                
	    // fila 
	    data = hoja.getCell(columna, fila).getContents(); 
	    System.out.print(data + " "); 

	    } 
	    System.out.println("\n"); 
	    } 
	    } 
	    } catch (Exception ioe) { 
	    ioe.printStackTrace(); 
	    } 
*/
	    } 
 
  
 public  List<String> ObtenerDistribucion(String Campana, String zona, int cantidad)
 {
	 System.out.println("..............................");
	 System.out.println(Campana);
	 System.out.println(zona);
	 Properties props = new Properties();
	 props.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
     props.put("java.naming.factory.url.pkgs", "org.jboss.naming:org.jnp.interfaces");
     props.put("java.naming.provider.url", URLPROPS);
 
     
     
     List<String> listaCentro = new ArrayList<String>();
     
	try {
		 Context ctx = new InitialContext(props);
		 distributionRemote dBean= (distributionRemote) ctx.lookup("distribution/remote");
		 listaCentro = dBean.ObtenerHotPotsCampaña(Campana,zona, cantidad,BDconf);
			

		 
		 
	} catch (NamingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
		return listaCentro;
     
	 
 }


 

	public List<DatosTallas> ObtenerTallas(String campania, String zona,
			String especie, String estacion) throws IllegalArgumentException {
	
	
	
	 Properties props = new Properties();

		// List<String> talla = new ArrayList<String>();
		 List<Tallas> listaTallas = new ArrayList<Tallas>();
		 List<DatosTallas> datost = new ArrayList<DatosTallas>();
		 DatosTallas dt;
		    
		      props.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
		      props.put("java.naming.factory.url.pkgs", "-org.jboss.naming:org.jnp.interfaces");
		      props.put("java.naming.provider.url", Direcciones.URLPROPS);
		      System.err.println("Ingresando al componente ...................................");
		    	
				try {
			
					Context ctx= new InitialContext(props);
					TallasBeanRemote tallaBean= (TallasBeanRemote) ctx.lookup("TallasBean/remote");
					//TODO .---- cambiar!
					listaTallas= tallaBean.ObtenerTallas(campania, zona, especie,estacion,"config.txt");	
					
					for (int i=0; i<listaTallas.size();i++){
							dt = new DatosTallas();
							dt.setId(listaTallas.get(i).getId());
							dt.setCampania_id(listaTallas.get(i).getCampania_id());
							dt.setEstacion_id(listaTallas.get(i).getEstacion_id());
							dt.setEspecie_id(listaTallas.get(i).getEspecie_id());
							dt.setZona_id(listaTallas.get(i).getZona_id());
							dt.setNroTalla(listaTallas.get(i).getNrotalla());
							dt.setCantidad(listaTallas.get(i).getCantidad());
							datost.add(dt);
						} 
						
					
					
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				return datost;
	

	}

	
	public List<String> ObtenerZonasCampania(String campania)
			throws IllegalArgumentException {
		
		 List<String> listaZonasTallas = new ArrayList<String>();

		
		 Properties props = new Properties();

			    
			props.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
			props.put("java.naming.factory.url.pkgs", "-org.jboss.naming:org.jnp.interfaces");
			props.put("java.naming.provider.url", Direcciones.URLPROPS);
			System.err.println("Ingresando al componente ...................................");
			    	
		    try {
				
						Context ctx= new InitialContext(props);
						TallasBeanRemote tallaBean= (TallasBeanRemote) ctx.lookup("TallasBean/remote");
						//TODO .---- cambiar!
						listaZonasTallas= tallaBean.ObtenerZonaTallas(campania,"config.txt");	
												
						
						
					} catch (NamingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					return listaZonasTallas;
		
	}

	
	public List<String> ObtenerEstacionesCampania(String campania, String zona)throws IllegalArgumentException {

		 List<String> listaEstacionTallas = new ArrayList<String>();

		
		 Properties props = new Properties();

			    
			props.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
			props.put("java.naming.factory.url.pkgs", "-org.jboss.naming:org.jnp.interfaces");
			props.put("java.naming.provider.url", Direcciones.URLPROPS);
			System.err.println("Ingresando al componente ...................................");
			    	
		    try {
				
						Context ctx= new InitialContext(props);
						TallasBeanRemote tallaBean= (TallasBeanRemote) ctx.lookup("TallasBean/remote");
						//TODO .---- cambiar!
						listaEstacionTallas= tallaBean.ObtenerEstacionTallas(campania,zona,"config.txt");	
												
						
						
					} catch (NamingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					return listaEstacionTallas;
		
		
		
	}

	public List<String> ObtenerEspeciesCampania(String campania, String zona,String estacion) {
		 List<String> listaEspeciesTallas = new ArrayList<String>();

			
		 Properties props = new Properties();

			    
			props.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
			props.put("java.naming.factory.url.pkgs", "-org.jboss.naming:org.jnp.interfaces");
			props.put("java.naming.provider.url", Direcciones.URLPROPS);
			System.err.println("Ingresando al componente ...................................");
			    	
		    try {
				
						Context ctx= new InitialContext(props);
						TallasBeanRemote tallaBean= (TallasBeanRemote) ctx.lookup("TallasBean/remote");
						//TODO .---- cambiar!
						listaEspeciesTallas= tallaBean.ObtenerEspecieTallas(campania,zona,estacion,"config.txt");	
												
						
						
					} catch (NamingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					return listaEspeciesTallas;
	}

	public List<List<String>> readFileZona(String nameFile, String tabla) throws 
		IOException, 
		ExcepcionLongitud, 
		ExcepcionNulo, 
		ExcepcionTipoCampo, 
		ExcepcionValidacion
	{
		List<List<String>> ListaData = new ArrayList<List<String>>();
		ManejoArchivoCSV mn =new ManejoArchivoCSV(tabla); 
		try {
			File readFile=new File(nameFile);
			System.out.println("Leyendo archivo en metodo ReadFileZona");
			ListaData= mn.generarLecturaArchivo  (readFile, tabla);
			readFile.delete();
		} catch (ExcepcionLongitud e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		} catch (ExcepcionNulo e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		} catch (ExcepcionTipoCampo e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		} catch (ExcepcionValidacion e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}
		return ListaData;
	}
	
	public void guardarDatosArchivo(String tabla, List<List<String>> datos) throws ExcepcionValidacion{
		Tablas tab = new Tablas();
		int baseSelec;
		 
		baseSelec = tab.getIndice(tabla);
		for (List<String> elemList:datos){
			try{
				switch (baseSelec) {
				case 1: 
					guardarCampania(elemList);
					break;
				case 2: 
					guardarEstacion(elemList);
					break;
				case 3: 
					guardarZona(elemList);
					break;
				case 4:
					guardarTalla(elemList);
					break;
				case 5:
					guardarEspecie(elemList);
					break;
				case 6:
					guardarDatosBio(elemList);
					break;
			
				}
			}catch (ExcepcionValidacion e){
				throw e;
			}
		}
	} 
	
	public void guardarCampania(List<String> datos) 
	throws ExcepcionLongitud,
		   ExcepcionNulo,
		   ExcepcionValidacion,
		   ExcepcionTipoCampo{

		Tupla tupla = new Tupla(datos);
		System.out.println(tupla.toString());
		//System.out.println(tupla.getCantidad());
		Validador validador = new Validador();
				
		try {
			validador.validarCampania(tupla, 0);
		} catch (ExcepcionNulo e1) {
			throw e1;
			//e1.printStackTrace();
		} catch (ExcepcionLongitud e1) {
			// TODO Auto-generated catch block
			throw e1;
		} catch (ExcepcionTipoCampo e1) {
			// TODO Auto-generated catch block
			throw e1;
		} catch (ExcepcionValidacion e1) {
			// TODO Auto-generated catch block
			throw e1;
			//e1.printStackTrace();
		}

			Properties props = new Properties();
				 
			props.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
			props.put("java.naming.factory.url.pkgs", "-org.jboss.naming:org.jnp.interfaces");
			props.put("java.naming.provider.url", Direcciones.URLPROPS);
			System.err.println("Ingresando al componente ...................................");
					    	
			try {
				Context ctx= new InitialContext(props);
				TallasBeanRemote tallaBean= (TallasBeanRemote) ctx.lookup("TallasBean/remote");
				if(tallaBean.existeCodigo(BDconf, "CAMPANIAS",tupla.getCampo(0),"codigo"))
					throw new ExcepcionValidacion("CAMPAÑA EXISTENTE");
				else
					tallaBean.guardarCampania(datos, BDconf);
				
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
	}
	
	public void guardarEstacion(List<String> datos) 
	throws ExcepcionLongitud,
		   ExcepcionNulo,
		   ExcepcionValidacion,
		   ExcepcionTipoCampo{

		Tupla tupla = new Tupla(datos);
		System.out.println(tupla.toString());
		//System.out.println(tupla.getCantidad());
		Validador validador = new Validador();
				
		try {
			validador.validarEstaciones(tupla, 0);
		} catch (ExcepcionNulo e1) {
			throw e1;
			//e1.printStackTrace();
		} catch (ExcepcionLongitud e1) {
			// TODO Auto-generated catch block
			throw e1;
		} catch (ExcepcionTipoCampo e1) {
			// TODO Auto-generated catch block
			throw e1;
		} catch (ExcepcionValidacion e1) {
			// TODO Auto-generated catch block
			throw e1;
			//e1.printStackTrace();
		}

			Properties props = new Properties();
				 
			props.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
			props.put("java.naming.factory.url.pkgs", "-org.jboss.naming:org.jnp.interfaces");
			props.put("java.naming.provider.url", Direcciones.URLPROPS);
			System.err.println("Ingresando al componente ...................................");
					    	
			try {
				Context ctx= new InitialContext(props);
				TallasBeanRemote tallaBean= (TallasBeanRemote) ctx.lookup("TallasBean/remote");
				if(tallaBean.existeCodigo(BDconf, "ESTACIONES",tupla.getCampo(2),"codigo_estacion"))
					throw new ExcepcionValidacion("CODIGO ESTACION EXISTENTE");
				else
					tallaBean.guardarEstacion(datos, BDconf);
				
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
	}

	
	public void guardarZona(List<String> datos)
	throws ExcepcionLongitud,
	   ExcepcionNulo,
	   ExcepcionValidacion,
	   ExcepcionTipoCampo{
		Tupla tupla = new Tupla(datos);
		System.out.println(tupla.toString());
		//System.out.println(tupla.getCantidad());
		Validador validador = new Validador();
				
		try {
			validador.validarZona(tupla, 0);
		} catch (ExcepcionNulo e1) {
			throw e1;
			//e1.printStackTrace();
		} catch (ExcepcionLongitud e1) {
			// TODO Auto-generated catch block
			throw e1;
		} catch (ExcepcionTipoCampo e1) {
			// TODO Auto-generated catch block
			throw e1;
		} catch (ExcepcionValidacion e1) {
			// TODO Auto-generated catch block
			throw e1;
			//e1.printStackTrace();
		}

			Properties props = new Properties();
				 
			props.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
			props.put("java.naming.factory.url.pkgs", "-org.jboss.naming:org.jnp.interfaces");
			props.put("java.naming.provider.url", Direcciones.URLPROPS);
			System.err.println("Ingresando al componente ...................................");
					    	
			try {
				Context ctx= new InitialContext(props);
				TallasBeanRemote tallaBean= (TallasBeanRemote) ctx.lookup("TallasBean/remote");
				if(tallaBean.existeCodigo(BDconf, "ZONAS",tupla.getCampo(0),"codigo"))
					throw new ExcepcionValidacion("ZONA EXISTENTE");
				else
					tallaBean.guardarZona(datos, BDconf);
				
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	

		
	}

	public void guardarDatosBio(List<String> datos)
	throws ExcepcionLongitud,
	   ExcepcionNulo,
	   ExcepcionValidacion,
	   ExcepcionTipoCampo{
		Tupla tupla = new Tupla(datos);
		System.out.println(tupla.toString());
		//System.out.println(tupla.getCantidad());
		Validador validador = new Validador();
				
		try {
			validador.validarDatosBio(tupla, 0);
		} catch (ExcepcionNulo e1) {
			throw e1;
			//e1.printStackTrace();
		} catch (ExcepcionLongitud e1) {
			// TODO Auto-generated catch block
			throw e1;
		} catch (ExcepcionTipoCampo e1) {
			// TODO Auto-generated catch block
			throw e1;
		} catch (ExcepcionValidacion e1) {
			// TODO Auto-generated catch block
			throw e1;
			//e1.printStackTrace();
		}

			Properties props = new Properties();
				 
			props.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
			props.put("java.naming.factory.url.pkgs", "-org.jboss.naming:org.jnp.interfaces");
			props.put("java.naming.provider.url", Direcciones.URLPROPS);
			System.err.println("Ingresando al componente ...................................");
					    	
			try {
				Context ctx= new InitialContext(props);
				TallasBeanRemote tallaBean= (TallasBeanRemote) ctx.lookup("TallasBean/remote");
				if(tallaBean.existeCodigo(BDconf, "DATOS_BIOLOGICOS",tupla.getCampo(0),"id"))
					throw new ExcepcionValidacion("DATO BIOLOGICO EXISTENTE");
				else
					tallaBean.guardarDatosBio(datos, BDconf);
				
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	

		
	}
	
	public void guardarEspecie(List<String> datos) 
	throws ExcepcionLongitud,
		   ExcepcionNulo,
		   ExcepcionValidacion,
		   ExcepcionTipoCampo{

		Tupla tupla = new Tupla(datos);
		System.out.println(tupla.toString());
		//System.out.println(tupla.getCantidad());
		Validador validador = new Validador();
				
		try {
			validador.validarEspecie(tupla, 0);
		} catch (ExcepcionNulo e1) {
			throw e1;
			//e1.printStackTrace();
		} catch (ExcepcionLongitud e1) {
			// TODO Auto-generated catch block
			throw e1;
		} catch (ExcepcionTipoCampo e1) {
			// TODO Auto-generated catch block
			throw e1;
		} catch (ExcepcionValidacion e1) {
			// TODO Auto-generated catch block
			throw e1;
			//e1.printStackTrace();
		}

			Properties props = new Properties();
				 
			props.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
			props.put("java.naming.factory.url.pkgs", "-org.jboss.naming:org.jnp.interfaces");
			props.put("java.naming.provider.url", Direcciones.URLPROPS);
			System.err.println("Ingresando al componente ...................................");
					    	
			try {
				Context ctx= new InitialContext(props);
				TallasBeanRemote tallaBean= (TallasBeanRemote) ctx.lookup("TallasBean/remote");
				if(tallaBean.existeCodigo(BDconf, "ESPECIES",tupla.getCampo(0),"codigo"))
					throw new ExcepcionValidacion("ESPECE EXISTENTE");
				else
					tallaBean.guardarEspecie(datos, BDconf);
				
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
	}

	public void guardarTalla(List<String> datos) 
	throws ExcepcionLongitud,
		   ExcepcionNulo,
		   ExcepcionValidacion,
		   ExcepcionTipoCampo{

		Tupla tupla = new Tupla(datos);
		System.out.println(tupla.toString());
		//System.out.println(tupla.getCantidad());
		Validador validador = new Validador();
				
		try {
			validador.validarTalla(tupla, 0);
		} catch (ExcepcionNulo e1) {
			throw e1;
			//e1.printStackTrace();
		} catch (ExcepcionLongitud e1) {
			// TODO Auto-generated catch block
			throw e1;
		} catch (ExcepcionTipoCampo e1) {
			// TODO Auto-generated catch block
			throw e1;
		} catch (ExcepcionValidacion e1) {
			// TODO Auto-generated catch block
			throw e1;
			//e1.printStackTrace();
		}

			Properties props = new Properties();
				 
			props.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
			props.put("java.naming.factory.url.pkgs", "-org.jboss.naming:org.jnp.interfaces");
			props.put("java.naming.provider.url", Direcciones.URLPROPS);
			System.err.println("Ingresando al componente ...................................");
					    	
			try {
				Context ctx= new InitialContext(props);
				TallasBeanRemote tallaBean= (TallasBeanRemote) ctx.lookup("TallasBean/remote");
				if(tallaBean.existeCodigo(BDconf, "TALLAS",tupla.getCampo(0),"id"))
					throw new ExcepcionValidacion("TALLA EXISTENTE");
				else
					tallaBean.guardarTalla(datos, BDconf);
				
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
	}
	
}
