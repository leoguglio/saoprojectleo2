package org.saoproject.web.server.loaders;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.saoproject.web.shared.exc.ExcepcionLongitud;
import org.saoproject.web.shared.exc.ExcepcionNulo;
import org.saoproject.web.shared.exc.ExcepcionTipoCampo;
import org.saoproject.web.shared.exc.ExcepcionValidacion;

/**
 * Clase que se encarga de:
 * <ul>
 * <li> Gestionar el manejo de los archivos de entrada.</li>
 * <li> Validar los datos leidos del archivo de entrada.</li>
 * <li> Generar un archivo de salida con las consultas sql generadas que puede ser cargado a la base de datos.</li>
 * </ul>
 * @author Gugliottella Leonardo 
 * Modificado para adaptarse a GWT por Maxi
 */

public class ManejoArchivoCSV 
{
	private ArrayList<String> archivoGen;
	private int baseSelec;
	private String consulta;	
	private String encabezado;
	private Tablas tablas = new Tablas();
	
	public ManejoArchivoCSV(String tabla){
		archivoGen = new ArrayList<String>();
		baseSelec = 0;
	}

	/**
	* Metodo principal que parsea el archivo y genera una consulta insert por cada línea validando cada campo
	* @param camino dirección o path del archivo de texto a ser leido
	* @param tabla nombre de la tabla que se va a validar, cada tabla tiene un validador asignado		
	*/
//	public String generarConsulta  (File archivo, String tabla) 
//	    throws IOException,
//			    ExcepcionLongitud,
//			    ExcepcionNulo,
//			    ExcepcionTipoCampo,
//			    ExcepcionValidacion
//	{
//		Tablas tab = new Tablas();
//		consulta = null;
//		int numLinea=0;
//		try
//		{		    
//			String linea;
//			BufferedReader br = new BufferedReader(new FileReader(archivo));
//			StringTokenizer st = null;			
//			
//			linea = br.readLine();
//			st = new StringTokenizer(linea,",");
//			encabezado = generarEncabezado(st,tabla);
//			Validador validador = new Validador();
//			baseSelec = tab.getIndice(tabla);
//			if (!validador.validarEncabezado(linea.trim(),tab.getEncabezado(baseSelec)))
//				throw new ExcepcionValidacion(0,"Encabezado incorrecto de tabla "+tabla);
//		
//			while ((linea=br.readLine())!=null){
//					Tupla datos = new Tupla(linea);
//					numLinea++;
//					switch (baseSelec) {
//						case 1: 
//							validador.validarCampania(datos,numLinea);
//							break;
//						case 2: 
//							validador.validarEstaciones(datos,numLinea);
//							break;
//						case 3: 
//							validador.validarZona(datos,numLinea);
//							break;
//						case 4:
//							validador.validarTalla(datos,numLinea);
//							break;
//						case 5:
//							validador.validarEspecie(datos,numLinea);
//							break;
//						case 6:
//							validador.validarDatosBio(datos,numLinea);
//							break;
//						case 7:
//							validador.validarBatimetria(datos,numLinea);
//							break;
//					}
//				
//					consulta+= generarConsulta(datos,encabezado);
//				
//				Logger.getLogger("CSVLoader").log(Level.INFO,consulta);
//				
//			}
//			
//		} 
//		catch (ExcepcionNulo e) {
//			throw e;
//		}
//		catch (ExcepcionLongitud e){
//			throw e;
//		}
//		catch (ExcepcionTipoCampo e){
//			throw e;
//		}
//		catch (ExcepcionValidacion e){
//			throw e;
//		}
//		catch (Exception e){
//			System.out.println("Ocurrió una excepción al leer el archivo:" + archivo.getAbsolutePath()); 
//			throw new IOException("Ocurrió una excepción al leer el archivo:" + archivo.getAbsolutePath());
//		}
//		
//		return consulta;
//		
//	}	
	

	/**
	 * Metodo que genera el encabezado con los campos correspondientes a la primer fila del archivo
	 * @param st StringTokenizer que contiene el encabezado de la tabla
	 * @param tabla Nombre de la tabla que se desea cargar
	 * @return String
	 */
	private String generarEncabezado(StringTokenizer st, String tabla){
		String encabezado = "INSERT INTO " + tabla + " (";
		
		while (st.hasMoreTokens()) {
			encabezado = encabezado + st.nextToken().toUpperCase();
			
			if(st.hasMoreTokens()){
				encabezado = encabezado + ",";
			} else 
				encabezado = encabezado + ") values (";
		}
		
		return encabezado;
	}
	
	/**
	 * Metodo que genera la consulta insert SQL
	 * @param datos Tupla con los datos de los campos
	 * @param enc String de encabezado con los nombres de los campos
	 * @return String Consulta SQL
	 */
//	private String generarConsulta(Tupla datos, String enc){
//		String consultaAux,campo,campoAux;
//		campo = datos.getCampo(0).trim();
//		campoAux = campo;
//		if (campo.matches("^[^\\d].*"))
//			campo = "\'"+campo+"\'";
//		consultaAux = enc + campo;
//		//TODO verificar implementacion de este codigo
//		for (int i=1; i<datos.getCantidad();i++){
//			campo = datos.getCampo(i).trim();
//			if (campo.matches("^[^\\d].*")) {
//				campoAux = "\'"+campo+"\'";
//				campo = campoAux;
//			}			
//			consultaAux = consultaAux +","+ campo;
//		}
//
//		consultaAux = consultaAux + ");";
//		return consultaAux;
//		
//	}
//	
//	public String getConsulta()
//	{
//		return consulta;
//	}
	
	//----------------------------------------------------------
	
	/**
	* Metodo principal que parsea el archivo y genera una lista con las filas validas
	* @param camino dirección o path del archivo de texto a ser leido
	* @param tabla nombre de la tabla que se va a validar, cada tabla tiene un validador asignado		
	*/
	public List<List<String>> generarLecturaArchivo  (File archivo, String tabla) 
	    throws IOException,
			    ExcepcionLongitud,
			    ExcepcionNulo,
			    ExcepcionTipoCampo,
			    ExcepcionValidacion
	{
	
		List<List<String>> listaData = new ArrayList<List<String>>();
		List<String> consultaAux;
		Tablas tab = new Tablas();
		consulta = null;
		int numLinea=1;
		try
		{		    
			String linea;
			BufferedReader br = new BufferedReader(new FileReader(archivo));
			StringTokenizer st = null;			
			String encAux = null;
			System.out.println("leyendo-------------encabezado");
			linea = br.readLine();
			st = new StringTokenizer(linea,",");
			encAux = generarEncabezado(st,tabla);
			System.out.println("El encabezado generado es: "+encAux);
			Validador validador = new Validador();
			baseSelec = tab.getIndice(tabla);
			if (!validador.validarEncabezado(linea.trim().toUpperCase(),tab.getEncabezado(baseSelec)))
				throw new ExcepcionValidacion(0,"Encabezado incorrecto de tabla "+tabla);
			encabezado = encAux.toUpperCase();
			System.out.println("El encabezado leido es: "+encabezado);
			while ((linea=br.readLine())!=null){
				System.out.println("leyendo-------------"+numLinea);
					Tupla datos = new Tupla(linea);
					numLinea++;
					switch (baseSelec) {
						case 1: 
							validador.validarCampania(datos,numLinea);
							break;
						case 2: 
							validador.validarEstaciones(datos,numLinea);
							break;
						case 3: 
							validador.validarZona(datos,numLinea);
							break;
						case 4:
							validador.validarTalla(datos,numLinea);
							break;
						case 5:
							validador.validarEspecie(datos,numLinea);
							break;
						case 6:
							validador.validarDatosBio(datos,numLinea);
							break;
						case 7:
							validador.validarBatimetria(datos,numLinea);
							break;
					}
				
					consultaAux = generarDatos(datos);
					listaData.add(consultaAux);
				
				Logger.getLogger("CSVLoader").log(Level.INFO,consultaAux.toString());
				Logger.getLogger("CSVLoader").log(Level.INFO,listaData.toString());
				
			}
			
		} 
		catch (ExcepcionNulo e) {
			throw e;
		}
		catch (ExcepcionLongitud e){
			throw e;
		}
		catch (ExcepcionTipoCampo e){
			throw e;
		}
		catch (ExcepcionValidacion e){
			System.out.println("Ocurrio un error al leer el archivo en ManejoArchivoCSV.generarLecturaArchivo");
			throw e;
		}
		catch (Exception e){
			System.out.println("Ocurrió una excepción al leer el archivo:" + archivo.getAbsolutePath()); 
			throw new IOException("Ocurrió una excepción al leer el archivo:" + archivo.getAbsolutePath());
		}
		
		return listaData;
		
	}	
	
	
	
	/**
	 * Metodo que genera la lista de datos
	 * @param datos Tupla con los datos de los campos
	 * @return List<String> lista con campos  
	 */
	private List<String> generarDatos(Tupla datos){
		
		List<String> listaD= new ArrayList<String>(); 
		
		//TODO verificar implementacion de este codigo
		for (int i=0; i<datos.getCantidad();i++){
			System.out.println("-----------------"+ datos.getCampo(i));
			listaD.add(datos.getCampo(i));
//			String[] tl = datos.getCampo(i).split(";");
//			for (int j=0; j<tl.length;j++){
//				listaD.add(tl[j]);
		}
			
//		}
//		int j= listaD.size();
//		for (int i=j;i<14;i++)
//				listaD.add("");

		System.out.println("Tupla: "+listaD.toString());
		return listaD;
			
	}
	
//	public List<String> guardarDatos(String tabla, List<List<String>> datos){
//		List<String> consultas = new ArrayList<String>();
//		String linea, consultaAux ,lineaAux = null;
//		Iterator<List<String>> i = datos.iterator();
//		while (i.hasNext()){
//			linea = i.next().toString();
//			lineaAux = linea.replace('[', ' ');
//			lineaAux = lineaAux.replace(']', ' ');
//			System.out.println(lineaAux);
//			Tupla tupla = new Tupla(lineaAux);
//			consultaAux = generarConsulta(tupla,tablas.getEncabezadoInsert(tabla));
//			System.out.println("La consulta es: "+consultaAux);
//			consultas.add(consultaAux);
//		}
//			
//		return consultas;
//	}
	
}