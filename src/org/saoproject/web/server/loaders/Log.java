package org.saoproject.web.server.loaders;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;

public class Log {
	private  PrintWriter log;
	
	/**
	 * Constructor de la clase log que crea un archivo de texto donde se encontrará el listado de errores
	 * @param ruta Ruta y nombre del archivo de log
	 */

	public Log (String ruta){
		try {
			log = new PrintWriter(new BufferedWriter(new FileWriter(ruta)));
		}
		catch (IOException e){
			System.out.println("Ocurrio un error al generar el archivo de errores");
		}
	}
	
	/**
	 * Insertar una linea en el log
	 * @param error String que contiene el error
	 * @param numLinea Numero de línea que contiene el error
	 */
	
	public void insertarLinea (String error, int numLinea) {
		log.println(error + numLinea);
	}
	
	/**
	 * Cerrar el log luego de escribir todos los errores
	 */
	public void cerrarLog (){
		log.close();
	}
}