package org.saoproject.web.server.loaders;

import org.saoproject.web.shared.exc.ExcepcionLongitud;
import org.saoproject.web.shared.exc.ExcepcionNulo;
import org.saoproject.web.shared.exc.ExcepcionTipoCampo;
import org.saoproject.web.shared.exc.ExcepcionValidacion;


/**
 * Clase utilizada para validar cada una de las lineas del archivo y verificar que se cumplen las restricciones
 * de integridad de cada una de las tablas que se desea cargar
 * @author Gugliottella Leonardo
 *
 */

public class Validador {
	
	public Validador (){
		super();
	}
	
	
	public boolean validarEncabezado(String enc1,String enc2 ){
		return enc1.equals(enc2);
	}
	
	private boolean esNulo (String valor) {
		return valor==null;
	}
	
	private boolean esEntero (int valor) {
		return valor % 1 == 0;
	}
	
	private boolean validarLongitud (String valor, int max) {
		return valor.length() <= max;
	}
	
	private boolean validarBoolean(String valor) {
		String val;
		val = valor.toUpperCase();
		return val.equals("'TRUE'") | val.equals("'FALSE'");
	}
	
	private boolean validarCantCampos(Tupla tupla, int min,int max){
		return (tupla.getCantidad() >= min) && (tupla.getCantidad()<=max);
	}
	/**
	 * Metodo que valida los campos pertenecientes a una tupla de la tabla Campanias
	 * @param datos
	 * @throws ExcepcionNulo
	 * @throws ExcepcionLongitud
	 * @throws ExcepcionTipoCampo
	 */
	public void validarCampania (Tupla datos,int linea) throws ExcepcionNulo,ExcepcionLongitud,ExcepcionTipoCampo, ExcepcionValidacion{
		
//		CONSTRAINTS
//		  codigo character(10) NOT NULL
//		  anio character(9) NOT NULL
//		  nombre character(20) NOT NULL
//		  tipo_campania character(20) NOT NULL
//		  tipo_muestreo character(20) NOT NULL
//		  nro_estaciones integer
//		  fecha character(30)
		String cod, anio, nombre, tipoCamp, tipoMuestreo, fecha;
		String nroEstaciones;
		int nro;
		cod = datos.getCampo(0);
		
		if (!validarCantCampos(datos,5,7)) throw new ExcepcionValidacion(linea,"CAMPOS FALTANTES");

		if (esNulo(cod)) throw new ExcepcionNulo("Código");
		else
			if (!validarLongitud(cod,10)) throw new ExcepcionLongitud(linea,10,"Código");

		anio = datos.getCampo(1);
		if (esNulo(anio)) throw new ExcepcionNulo("Año");
		else 
			if (!validarLongitud(anio,9)) throw new ExcepcionLongitud(linea,9,"Año");

		nombre = datos.getCampo(2);
		if (esNulo(nombre)) throw new ExcepcionNulo("Nombre");
		else 
			if (!validarLongitud(nombre,20)) throw new ExcepcionLongitud(linea,20,"Nombre");

		tipoCamp = datos.getCampo(3);
		if (tipoCamp == null) throw new ExcepcionTipoCampo(linea);
		else 
			if (!validarLongitud(tipoCamp,20)) throw new ExcepcionLongitud(linea,20,"Tipo Campaña");

		tipoMuestreo = datos.getCampo(4);
		if (esNulo(tipoMuestreo)) throw new ExcepcionNulo("Tipo muestreo");
		else 
			if (!validarLongitud(tipoMuestreo,20)) throw new ExcepcionLongitud(linea,20,"Tipo Muestreo");
		
		nroEstaciones = datos.getCampo(5);
		if (!esNulo(nroEstaciones)){
			try {
				nro = new Integer(Integer.parseInt(nroEstaciones));
			}
			catch (Exception e){
				System.out.println("El tipo de dato 'Número de estaciones'  debe ser entero");
				throw new ExcepcionTipoCampo(linea);			
			}
		}

		fecha = datos.getCampo(6);
		if (!esNulo(fecha)) 
			if (!validarLongitud(fecha,30)) throw new ExcepcionLongitud(linea,30,"Fecha");	
	}
	
	/**
	 * Metodo que valida los campos de una tupla pertenecientes a la tabla Estaciones
	 * @param datos
	 * @throws ExcepcionNulo
	 * @throws ExcepcionLongitud
	 * @throws ExcepcionTipoCampo
	 */
	public void validarEstaciones (Tupla datos,int linea) throws ExcepcionValidacion,ExcepcionNulo,ExcepcionLongitud,ExcepcionTipoCampo{
//		CONSTRAINTS
//		campania_id character(10) NOT NULL
//		zona_id character(3) NOT NULL
//		codigo_estacion character(3) NOT NULL
//		lat numeric(8,6) 
//		lon numeric(8,6)
//		posicion geometry
//		id serial NOT NULL
		String campania_id, zona_id, codigo_estacion,latitud,longitud;
		double lat;
		double lon;
		
		if (!validarCantCampos(datos,3,7)) throw new ExcepcionValidacion(linea,"CAMPOS FALTANTES");

		campania_id = datos.getCampo(0);
		if (esNulo(campania_id)) throw new ExcepcionNulo(linea,"CAMPAÑA ID");
		else
			if (!validarLongitud(campania_id,10)) throw new ExcepcionLongitud(linea, 10, "Campaña ID");
		
		zona_id = datos.getCampo(1);
		if (esNulo(zona_id)) throw new ExcepcionNulo(linea,"ZONA ID");
		else
			if (!validarLongitud(zona_id,10)) throw new ExcepcionLongitud(linea,10,"Zona ID");
		
		codigo_estacion = datos.getCampo(2);
		if (esNulo(codigo_estacion)) throw new ExcepcionNulo(linea,"CODIGO ESTACION");
		else 
			if (!validarLongitud(codigo_estacion,3)) throw new ExcepcionLongitud(linea,3,"Código Estación");

//		latitud = datos.getCampo(3);
//		if (!esNulo(latitud)){
//			try {
//				lat = new Double(Double.parseDouble(datos.getCampo(3)));
//			}
//			catch (Exception e) {
//				System.out.println("El tipo de dato del campo latitud debe ser Double");
//				throw new ExcepcionTipoCampo(linea);
//			}
//		}
//		
//		longitud = datos.getCampo(4);
//		if (!esNulo(longitud)) {
//			try {
//				lon = new Double(Double.parseDouble(longitud));
//			}
//			catch (Exception e) {
//				System.out.println("El tipo de dato del campo longitud debe ser Double");
//				throw new ExcepcionTipoCampo(linea);
//			}
//		}
	}
	
	/**
	 * Metodo que valida los campos de una tupla perteneciente a la tabla Zonas
	 * @param datos
	 * @throws ExcepcionNulo
	 * @throws ExcepcionLongitud
	 */
	public void validarZona(Tupla datos,int linea) throws ExcepcionNulo, ExcepcionLongitud,ExcepcionValidacion{
//	CONSTRAINTS
//	codigo  CHARACTER (3) NOT NULL
//	descripcion CHARACTER (30) NOT NULL
		
		String codigo, descripcion,latlon;
		double coordenada;
		if (!validarCantCampos(datos,2,14)) throw new ExcepcionValidacion(linea,"CAMPOS FALTANTES");

		codigo = datos.getCampo(0);
		if (esNulo(codigo)) throw new ExcepcionNulo(linea,"CODIGO ZONA");
		else if (!validarLongitud(codigo,3)) throw new ExcepcionLongitud(linea,3,"Codigo Zona");
		
		descripcion = datos.getCampo(1);
		if (esNulo(descripcion)) throw new ExcepcionNulo(linea,"DESCRIPCION ZONA");
		else if (!validarLongitud(descripcion,30)) throw new ExcepcionLongitud(linea,30,"Descripción Zona");

		for (int i=2; i<14;i++){
			latlon = datos.getCampo(i);
			if (!esNulo(latlon)){
				try {
					coordenada = new Double(Double.parseDouble(datos.getCampo(i)));
				}
				catch (Exception e) {
					System.out.println("El tipo de dato del campo debe ser double");
					throw new ExcepcionTipoCampo(linea);
				}
			}
			else {
				latlon = datos.getCampo(i++);
				if (esNulo(latlon))
					throw new ExcepcionValidacion(linea,"CAMPO COORDENADA LATITUD FALTANTE");
			}
		}


	}
	
	/**
	 * Metodo que valida los campos pertenecientes a una tupla de la tabla Tallas
	 * @param datos
	 * @throws ExcepcionNulo
	 * @throws ExcepcionLongitud
	 * @throws ExcepcionTipoCampo
	 */
	
	public void validarTalla (Tupla datos, int linea) throws ExcepcionValidacion,ExcepcionNulo, ExcepcionLongitud, ExcepcionTipoCampo {
//	CONSTRAINTS
//	id INTEGER NOT NULL
//	campaña_id CHARACTER (10) NOT NULL
//	zona_id CHARACTER (3) NOT NULL
//	estacion_id CHARACTER (3) NOT NULL
	
	String idTalla, nroTalla, cantidad,campaniaId, zonaId, estacionId, especieId;
	int id_talla, nro_talla, nro_cantidad;	
	
	if (!validarCantCampos(datos,4,7)) throw new ExcepcionValidacion(linea,"CAMPOS FALTANTES");
	
	
	idTalla = datos.getCampo(0);
	if (esNulo(idTalla)){
		throw new ExcepcionNulo(linea,"ID TALLA");
	}
	else {
		try {
			id_talla = new Integer(Integer.parseInt(idTalla));
		}
		catch (Exception e){
			System.out.println("El tipo de dato 'ID TALLA'  debe ser entero");
			throw new ExcepcionTipoCampo(linea);			
		}
	}

	campaniaId = datos.getCampo(1);
	if (esNulo(campaniaId)) throw new ExcepcionNulo(linea,"CAMPAÑA ID");
	else if (!validarLongitud(campaniaId,10)) throw new ExcepcionLongitud(linea,10,"CAMPAÑA ID");
	
	zonaId = datos.getCampo(2);
	if (esNulo(zonaId)) throw new ExcepcionNulo(linea,"ZONA ID");
	else if (!validarLongitud(zonaId,3)) throw new ExcepcionLongitud(linea,3,"ZONA ID");
	
	estacionId = datos.getCampo(3);
	if (esNulo(estacionId)) throw new ExcepcionNulo(linea,"ESTACION ID");
	else if (!validarLongitud(estacionId,3)) throw new ExcepcionLongitud(linea,3,"ESTACION ID");
	
	especieId = datos.getCampo(4);
	if (esNulo(especieId)) throw new ExcepcionNulo(linea,"ESPECIE ID");
	else if (!validarLongitud(especieId,2)) throw new ExcepcionLongitud(linea,2,"ESPECIE ID");
	
	nroTalla = datos.getCampo(5);
	if (!esNulo(nroTalla)) 
		try {
			nro_talla = new Integer(Integer.parseInt(nroTalla));
		}
		catch (Exception e ){
			System.out.println("El tipo de dato 'NUMERO TALLA' debe ser entero");
			throw new ExcepcionTipoCampo(linea);
		}
	
	cantidad = datos.getCampo(6);
	if (!esNulo(cantidad))
		try {
			nro_cantidad = new Integer(Integer.parseInt(cantidad));
		}
		catch (Exception e)	{
			System.out.println("El tipo de dato 'CANTIDAD' debe ser entero");
			throw new ExcepcionTipoCampo(linea);
		}
	}
	
	/**
	 * Metodo que valida los campos de una tupla de la tabla Especie
	 * @param datos
	 * @throws ExcepcionNulo
	 * @throws ExcepcionLongitud
	 */
	
	public void validarEspecie (Tupla datos, int linea) throws ExcepcionValidacion,ExcepcionNulo, ExcepcionLongitud {
	
//	codigo CHARACTER(2)	NOT NULL
//	nombre CHARACTER(3O) NOT NULL
//	nombre_cientifico CHARACTER(50) NOT NULL
	
	String codigo, nombre, nombreCientifico;

	if (!validarCantCampos(datos,3,3)) throw new ExcepcionValidacion(linea,"CAMPOS FALTANTES");

	
	codigo = datos.getCampo(0);
	if (esNulo(codigo)) throw new ExcepcionNulo(linea,"CODIGO ESPECIE");
	else if (!validarLongitud(codigo,2)) throw new ExcepcionLongitud(linea,2,"CODIGO");
	
	nombre = datos.getCampo(1);
	if (esNulo(nombre)) throw new ExcepcionNulo(linea,"NOMBRE ESPECIE");
	else if (!validarLongitud(nombre,30)) throw new ExcepcionLongitud(linea,30,"NOMBRE");
	
	nombreCientifico = datos.getCampo(2);
	if (esNulo(nombreCientifico)) throw new ExcepcionNulo(linea,"NOMBRE CIENTIFICO");
	else if (!validarLongitud(nombreCientifico,50)) throw new ExcepcionLongitud(linea,50,"NOMBRE CIENTIFICO");
	
	}
	
	/**
	 * Metodo que valida los campos de una tupla correspondiente a la tabla DatosBiologicos de una especie
	 * @param datos
	 * @throws ExcepcionNulo
	 * @throws ExcepcionLongitud
	 * @throws ExcepcionTipoCampo
	 */
	public void validarDatosBio(Tupla datos, int linea) throws ExcepcionValidacion,ExcepcionNulo, ExcepcionLongitud, ExcepcionTipoCampo {
//	id SERIAL NOT NULL
//	estacion_id INTEGER NOT NULL
//	especie_id CHARACTER (2) NOT NULL
//	abundancia INTEGER
//	densidad NUMERIC (13,10)
//	distribucion boolean
	
	String id, estacionId, especieId, abundancia, densidad, distribucion;
	int nroId, nroEstacion, nroAbundancia;
	float nroDensidad;
	
	if (!validarCantCampos(datos,3,6)) throw new ExcepcionValidacion(linea,"CAMPOS FALTANTES");

	
	id = datos.getCampo(0);
	System.out.println("Campo 0: "+id);
	if (esNulo(id)) throw new ExcepcionNulo(linea,"ID");
	else
		try {
			nroId = new Integer(Integer.parseInt(id));
		} 
		catch (Exception e) {
			System.out.println("El tipo de dato 'ID' debe ser entero");
			throw new ExcepcionTipoCampo(linea);
		}
	
	estacionId = datos.getCampo(1);
	System.out.println("Campo 1: "+estacionId);
	if (esNulo(id)) throw new ExcepcionNulo(linea,"ID ESTACION ");
	else
		try {
			nroEstacion = new Integer(Integer.parseInt(estacionId));
		}
		catch (Exception e) {
			System.out.println("El tipo de dato 'ID ESTACION' debe ser entero");
			throw new ExcepcionTipoCampo(linea);
		}

	especieId = datos.getCampo(2);
	System.out.println("Campo 2: "+especieId);
	if (esNulo(especieId)) throw new ExcepcionNulo(linea,"ESPECIE ID");
	else if (!validarLongitud(especieId,2)) throw new ExcepcionLongitud(linea,2,"ESPECIE ID");
	
	abundancia = datos.getCampo(3);
	System.out.println("Campo 3: "+abundancia);
	if (!esNulo(abundancia)) {
		try {
			nroAbundancia = new Integer(Integer.parseInt(abundancia));
		}
		catch (Exception e) {
			System.out.println("El tipo de dato 'ABUNDANCIA' debe ser entero");
		}
	}
	
	densidad = datos.getCampo(4);
	System.out.println("Campo 4: "+densidad);
	if (!esNulo(densidad)){
		try {
			nroDensidad = new Float(Float.parseFloat(densidad));
		}
		catch (Exception e) {
			System.out.println("El tipo de dato del campo 'DENSIDAD' debe ser float");
			throw new ExcepcionTipoCampo(linea);
		}
	}
	
	distribucion = datos.getCampo(5);
	System.out.println("Campo 5: "+distribucion);
	if (!esNulo(distribucion)) 
		if (!validarBoolean(distribucion)) {
			System.out.println("El tipo de dato del campo 'DISTRIBUCION' debe ser boolean");
			throw new ExcepcionTipoCampo(linea);
		}

	}
	
	/**
	 * Metodo para validar una tupla correspondiente a la tabla batimetria
	 * @param datos
	 * @throws ExcepcionNulo
	 * @throws ExcepcionTipoCampo
	 */
	
	public void validarBatimetria (Tupla datos, int linea) throws ExcepcionValidacion,ExcepcionNulo, ExcepcionTipoCampo {
//	gid INTEGER NOT NULL
//	id INTEGER 
//	profundidad DOUBLE PRECISION
//	prof DOUBLE PRECISION
//	the_geom GEOMETRY
	
	String gid,id,profundidad,prof,theGeom;
	int nroGid,nroId;
	double prof1, prof2;
	
	if (!validarCantCampos(datos,1,5)) throw new ExcepcionValidacion(linea,"CAMPOS FALTANTES");

	gid = datos.getCampo(0);
	if (esNulo(gid)) throw new ExcepcionNulo(linea,"GID");
	else
		try {
			nroGid = new Integer(Integer.parseInt(gid));
		}
		catch(Exception e){
			System.out.println("El campo 'GID' debe ser de tipo entero");
			throw new ExcepcionTipoCampo(linea);
		}
	
	id = datos.getCampo(1);
	if (!esNulo(id))
		try {
			nroId = new Integer(Integer.parseInt(id));
		}
		catch (Exception e){
			System.out.println("El campo 'ID' debe ser de tipo entero");
			throw new ExcepcionTipoCampo(linea);
		}
	
	profundidad = datos.getCampo(2);
	if (!esNulo(profundidad))
		try {
			prof1 = new Double(Double.parseDouble(profundidad));
		}
		catch (Exception e)	{
			System.out.println("El campo 'PROFUNDIDAD' debe ser de tipo Double");
			throw new ExcepcionTipoCampo(linea);
		}

	prof = datos.getCampo(3);
	if (!esNulo(prof))
		try {
			prof2 = new Double(Double.parseDouble(prof));
		}
		catch (Exception e)	{
			System.out.println("El campo 'PROF' debe ser de tipo Double");
			throw new ExcepcionTipoCampo(linea);
		}
	
	//TODO VER VALIDACION PARA EL TIPO DE DATOS GEOMETRY
		//theGeom = datos.getCampo(4);
	
	
	}
	
	public static void main (String []args) {
//		StringTokenizer st = new StringTokenizer("codigo,anio,nombre,tipo_campania,tipo_muestreo,,06/09/1984",",");
//		
//		Validador validador = new Validador ();
//		try {
//			validador.validarCampania(st);
//			System.out.println("La tupla es correcta");
//		} catch (ExcepcionNulo e) {
//			// TODO Auto-generated catch block
//			System.out.println("Ocurrio un error al procesar el campo "+e.getCampo()+" no puede ser nulo");
//		} catch (ExcepcionLongitud e) {
//			// TODO Auto-generated catch block
//		} catch (ExcepcionTipoCampo e) {
//			// TODO Auto-generated catch block
//		}
		final String linea = "codigo,anio,nombre,tipo_campania,tipo_muestreo,,06/09/1984,,";
		String []datos = linea.split(",", -1);
		for (int i=0;i<datos.length;i++ ) {
			System.out.print("Campo" + i + ": ");
			if (datos[i].isEmpty())
				System.out.println("NULL");
			else
				System.out.println(datos[i]);
		}
		
	}
}