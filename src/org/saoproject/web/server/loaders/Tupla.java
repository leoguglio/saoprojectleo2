package org.saoproject.web.server.loaders;

import java.util.List;
import java.util.ArrayList;

import com.google.gwt.user.client.rpc.core.java.util.Arrays;

/**
 * Clase utilizada para contener cada uno de los campos de la tupla y la cantidad de campos total de la misma.
 * @author Gugliottella Leonardo
 *
 */
public class Tupla {
	
	private int cantCampos;
	private String[] campos;

	/**
	*	Metodo que genera una tupla de valores con los datos de los campos de la tabla
	*	@param linea string que contiene la linea leida del archivo
	*
	*/	
	public Tupla(String linea){
		super();
		campos = linea.split(",",-1);
		cantCampos = campos.length;
		//TODO ANALIZAR ESTE CODIGO
		for (int i=0; i<cantCampos;i++){
			if (campos[i].trim().isEmpty())
				campos[i]=null;
		}
	}
	
	public Tupla (List<String> linea){
		campos = linea.toArray(new String[linea.size()]);
		System.out.println(linea.toString());
		setCantidad(campos.length); 
		for (String elem:campos)
			System.out.println("Campo: "+elem);
		System.out.println(this.getCantidad());
	}
	
	private void setCantidad (int cant){
		cantCampos = cant;
	}

	/**
	*	Metodo que devuelve la cantidad de campos de la tupla
	*	@return int (cantidad de campos)
	*/	
	public int getCantidad() {
		return cantCampos;
	}
	
	/**
	*	Metodo que setea la cantidad de campos de la tupla
	*	@param camp cantidad de campos
	*/
	public void setCampos(String[] camp) {
		campos = camp;
	}

	/**
	*	Metodo que devuelve un campo determinado especificado por una posicion
	*	@param pos cantidad de campos
	*	@return String
	*/
	public String getCampo(int pos) {
		return campos[pos];
				
	}
	
	public String toString(){
		String campo = new String();
		for (String e:campos){
			campo = campo + e;
		}
		return campo;
	}
}