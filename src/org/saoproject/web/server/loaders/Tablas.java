package org.saoproject.web.server.loaders;
import java.util.HashMap;

/**
 * Clase que se encarga de gestionar los nombres de las tablas y funciona como libreria para las clases que
 * hagan uso de las tablas
 * @author Gugliottella Leonardo
 */

public class Tablas {
	private   HashMap<String,Integer> tablaDatos;
	private HashMap<Integer,String> encabezados;
	public static final String[] nombreTablas = {"CAMPANIAS","ESTACIONES","ZONAS",
											  "TALLAS","ESPECIES","DATOS BIOLOGICOS","BATIMETRIA"};
	private HashMap<String,String> primaryKeys;
	
	public Tablas(){
		tablaDatos = new HashMap<String,Integer>();
		encabezados = new HashMap<Integer,String>();
		primaryKeys = new HashMap<String, String>();
		iniciarTablas();
	}
	
	private void iniciarTablas() {
		tablaDatos.put("CAMPANIAS",1);
		tablaDatos.put("ESTACIONES",2);
		tablaDatos.put("ZONAS",3);
		tablaDatos.put("TALLAS",4);
		tablaDatos.put("ESPECIES",5);
		tablaDatos.put("DATOS_BIOLOGICOS",6);
		tablaDatos.put("BATIMETRIA",7);
		iniciarEnc();
		iniciarPK();
	}
	
	private void iniciarPK(){
		primaryKeys.put("CAMPANIAS", "CODIGO");
		primaryKeys.put("ESTACIONES", "CODIGO_ESTACION");
		primaryKeys.put("ZONAS", "CODIGO");
		primaryKeys.put("TALLAS", "ID");
		primaryKeys.put("ESPECIES", "ID");
		primaryKeys.put("DATOS_BIOLOGICOS","ID");
		primaryKeys.put("BATIMETRIA", "GID");
	}
	
	private void iniciarEnc(){
		encabezados.put(1,"CODIGO,ANIO,NOMBRE,TIPO_CAMPANIA,TIPO_MUESTREO,NRO_ESTACIONES,FECHA");
		encabezados.put(2,"CAMPANIA_ID,ZONA_ID,CODIGO_ESTACION,LATITUD,LONGITUD");
		encabezados.put(3,"CODIGO,DESCRIPCION,LAT1,LON1,LAT2,LON2,LAT3,LON3,LAT4,LON4,LAT5,LON5,LAT6,LON6");
		encabezados.put(4,"ID,CAMPAÑA_ID,ZONA_ID,ESTACION_ID,ESPECIE_ID,NRO_TALLA,CANTIDAD");
		encabezados.put(5,"CODIGO,NOMBRE,NOMBRE_CIENTIFICO");
		encabezados.put(6,"ID,ESTACION_ID,ESPECIE_ID,ABUNDANCIA,DENSIDAD,DISTRIBUCION");
		encabezados.put(7,"GID,ID,PROFUNDIDAD,PROF");
		
	}
	

	public String getEncabezado(int indice) {
		return encabezados.get(indice);
	}
	
	public String getEncabezadoInsert(String tabla){
		return"INSERT INTO "+tabla.toUpperCase()+" ("+getEncabezado(getIndice(tabla))+") VALUES (";
	}
	
	public int getIndice(String tabla){
		return (int) tablaDatos.get(tabla);
	}
	
	public String getPK(String tabla){
		return primaryKeys.get(tabla);
	}
	

	

		public static void main(String[] args){
		Tablas tab = new Tablas();
		tab.iniciarTablas();
		System.out.println("INDICE: " + tab.getIndice("CAMPANIAS"));
		System.out.println("INDICE: " + tab.getIndice("ZONAS"));
		System.out.println("INDICE: " + tab.getEncabezado(0));
		System.out.println("INDICE: " + tab.getEncabezado(1));
		System.out.println("INDICE: " + tab.getEncabezado(2));
		System.out.println("INDICE: " + tab.getEncabezado(3));
		System.out.println("INDICE: " + tab.getEncabezado(4));
		System.out.println("INDICE: " + tab.getEncabezado(5));
		System.out.println("INDICE: " + tab.getEncabezado(6));
		System.out.println("INDICE: " + tab.getEncabezado(7));
		System.out.println("ENCABEZADO DE TABLA CAMPANIAS:  "+tab.getEncabezadoInsert("CAMPANIAS"));
		System.out.println("ENCABEZADO DE TABLA ESTACIONES:  "+tab.getEncabezadoInsert("ESTACIONES"));
		System.out.println("ENCABEZADO DE TABLA ZONAS:  "+tab.getEncabezadoInsert("ZONAS"));
		System.out.println("ENCABEZADO DE TABLA TALLAS:  "+tab.getEncabezadoInsert("TALLAS"));
		System.out.println("ENCABEZADO DE TABLA ESPECIES:  "+tab.getEncabezadoInsert("ESPECIES"));
		System.out.println("ENCABEZADO DE TABLA DATOS_BIOLOGICOS:  "+tab.getEncabezadoInsert("DATOS_BIOLOGICOS"));
		System.out.println("ENCABEZADO DE TABLA BATIMETRIA:  "+tab.getEncabezadoInsert("BATIMETRIA"));
	}
}