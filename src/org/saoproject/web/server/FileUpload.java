package org.saoproject.web.server;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 * Servlet implementation class FileUpload
 */
public class FileUpload extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FileUpload() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	
	 private long FILE_SIZE_LIMIT = 20 * 1024 * 1024; // 20 MiB
	 
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	 
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  	
			try {
				Logger.getLogger("Fileupload").log(Level.INFO,"Entering FileUpload Servlet");

				PrintWriter out = response.getWriter();  
		        response.setContentType("text/plain");  
		        out.println("<h1>Servlet File Upload Example using Commons File Upload</h1>");  
		        out.println();  
		        String contextRoot = getServletContext().getRealPath("/");  
		   
		        DiskFileItemFactory  fileItemFactory = new DiskFileItemFactory ();  
		        fileItemFactory.setRepository(new File(contextRoot + "WebContent/upload/"));  
		        /* 
		         *Set the size threshold, above which content will be stored on disk. 
		         */  
		        fileItemFactory.setSizeThreshold(1*1024*1024); //1 MB  
		        /* 
		         * Set the temporary directory to store the uploaded files of size above threshold. 
		         */  
	            ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);  
	      
	    
	            /* 
	             * Parse the request 
	             */  
	            List<FileItem> items = uploadHandler.parseRequest(request);  
	            Iterator<FileItem> itr = items.iterator();  
	            while(itr.hasNext()) {  
	                FileItem item = (FileItem) itr.next();  
	                /* 
	                 * Handle Form Fields. 
	                 */  
	                if(item.isFormField()) {  
	                    out.println("File Name = "+item.getFieldName()+", Value = "+item.getString());  
	                } else {  
	                    //Handle Uploaded files.  
	                	  Logger.getLogger("Fileupload").log(Level.INFO, "File Name = "+item.getName()+  
	                        ", Content type = "+item.getContentType()+  
	                        ", File Size = "+item.getSize());  
	                     String fileName = item.getName();	                     
	                     File saveFile = new File("/tmp/"+fileName);          
	                     saveFile.createNewFile();       
	                     Logger.getLogger("Fileupload").log(Level.INFO, "File:"+saveFile.getPath());  
	                     item.write(saveFile);   
	  
	                      
	                }  
	                out.close();  
	            }  
	        }catch(FileUploadException ex) {  
	            log("Error encountered while parsing the request",ex);  
	        } catch(Exception ex) {  
	            log("Error encountered while uploading file",ex);  
	        }  
	   
	    }  
		
	}
