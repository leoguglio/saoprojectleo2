package org.saoproject.web.server.altas;

import java.io.Serializable;


public class Campania implements Serializable {
	
	   private String codigo="";
	   private String anio="";
	   private String nombre="";
	   private String tipo_campania="";
	   private String tipo_muestreo="";
	   private int nro_estaciones=0;
	   private String fecha="";

	   public  Campania() {

		      }
	   
	   public String getCodigo() {
		        return codigo;
		      }
	   
	   public String getAnio() {
	        return anio;
	      }
	   
	   public String getNombre() {
	        return nombre;
	      }
	   
	   public String getTipoCampania(){
		   return tipo_campania;
	   }
	   
	   public String getTipoMuestreo(){
		   return tipo_muestreo;
	   }
	   
	   public int getNroEstaciones(){
		   
		   return nro_estaciones;
	   }
	   
	   public String getFecha(){
		   return fecha;
	   }
	   
	   public void setCodigo(String codigo) {
	      this.codigo= codigo;
	      }
	   
   
	   public void setAnio(String anio) {
		   this.anio= anio;
	   }
   
	   public void setNombre(String nombre) {
		   this.nombre=nombre;
	   }
   
	   public void setTipoCampania(String tipo_campania){
		   this.tipo_campania= tipo_campania;
	   }
   
	   public void setTipoMuestreo(String tipo_muestreo){
	   		this.tipo_muestreo=tipo_muestreo;
	   }
   
	   public void setNroEstaciones(int nro_estaciones){

		   this.nro_estaciones=nro_estaciones;
	   }
   
	   public void setFecha(String fecha){
		   this.fecha=fecha;
	   }
	   
	   

	   
	/*   public List<Campanias> getAllCampanias() 
	   {
		   Campanias c = new Campanias();
		   List<Campanias> listaCampanias = new ArrayList<Campanias>();
		   BDAcceso datos = new BDAcceso();
			//Iniciamos la Coneccion a la Base de Datos
			datos.conectarse();
			// Ejemplo de como Realizar una Consulta SELECT
			//String root = System.getProperty("user.dir");
	         //System.out.println("ACA"+root); // Llamamos al método que devuelve la ruta absoluta
			String select = "select * from Campanias";
			datos.desconectarse();
			ResultSet respuesta = datos.EjecutarSelect(select);
			
			try {
				while(respuesta.next()){
					c.codigo = respuesta.getString("codigo");
					c.anio = respuesta.getString("anio");
					c.nombre = respuesta.getString("nombre");
					c.tipo_campania = respuesta.getString("tipo_campania");
					c.tipo_muestreo = respuesta.getString("tipo_muestreo");
					c.nro_estaciones = Integer.parseInt(respuesta.getString("nro_estaciones"));
					c.fecha = respuesta.getString("fecha");
					listaCampanias.add(c);
					
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			return listaCampanias;
		   
	   }*/
	   
	   
	   
	

}
