package org.saoproject.web.server.altas;

import java.util.List;

import org.saoproject.web.server.loaders.Tupla;
import org.saoproject.web.server.loaders.Validador;
import org.saoproject.web.shared.exc.ExcepcionValidacion;

public class Altas {
	/* METODO QUE DA DE ALTA UNA NUEVA CAMPANIA VALIDANDO DATOS
	 * Y DEVOLVIENDO TRUE SI TIENE EXITO LA VALIDACION Y FALSE
	 * EN CASO CONTRARIO
	 * 
	 */
	public Campania darAltaCampania(List<String> datosCampania){
		Tupla tupla = new Tupla(datosCampania);
		Validador validador = new Validador();
		Campania campania = null;
		try {
			validador.validarCampania(tupla, 0);
			campania = new Campania();
			campania.setCodigo(tupla.getCampo(0));
			campania.setAnio(tupla.getCampo(1));
			campania.setNombre(tupla.getCampo(2));
			campania.setTipoCampania(tupla.getCampo(3));
			campania.setTipoMuestreo(tupla.getCampo(4));
			campania.setNroEstaciones(Integer.parseInt(tupla.getCampo(5)));
			campania.setFecha(tupla.getCampo(6));
		} catch (Exception e){
			e.printStackTrace();
		}
		
		return campania;

	}
}
