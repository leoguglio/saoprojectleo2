package org.saoproject.web.shared;

import java.io.Serializable;

public  class Retorno implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3632771888796663743L;
	Object datos;
	
	
	public Retorno(){
		this.datos =  null;
	}
	public Retorno(Object datos){
		this.datos =  datos;
	}
	public  Object getDatos(){
		return datos;
	};
	
}
