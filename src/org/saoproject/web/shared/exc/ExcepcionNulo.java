package org.saoproject.web.shared.exc;

import java.io.Serializable;

public class ExcepcionNulo extends ExcepcionValidacion implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 7695365960155910301L;

	public ExcepcionNulo(){
		super();
	}
	
	public ExcepcionNulo(String campo){
		setCampo(campo);
	}
	
	public ExcepcionNulo (int linea, String campo){
		super(linea);
		setError(getError() + "El campo "+ campo + " no puede ser nulo]");
		setCampo(campo);
	}
}
