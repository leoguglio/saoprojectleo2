package org.saoproject.web.shared.exc;

import java.io.Serializable;
public class ExcepcionLongitud extends ExcepcionValidacion implements Serializable {
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 2995531998916035230L;
	
	public ExcepcionLongitud(){
		super();
	}

	public ExcepcionLongitud(int linea, int lon, String nombre){
		super(linea);
		setCampo(nombre);
		setError(getError() + "La longitud del campo " + nombre +" no puede ser mayor a "+lon+"]");
	}
}
