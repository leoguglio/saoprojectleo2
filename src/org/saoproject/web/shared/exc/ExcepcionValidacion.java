package org.saoproject.web.shared.exc;

import java.io.Serializable;

public class ExcepcionValidacion extends Exception implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1732343109535664285L;
	private String campo;
	private String error;
	
	public ExcepcionValidacion(){
		super();
	}
	
	public ExcepcionValidacion(int linea){
		setError("[Linea " + linea + ": ");
		
	}
	
	public ExcepcionValidacion (int linea, String texto){
		setError("[Linea " + linea + ":" + texto + "]");
	}
	
	public ExcepcionValidacion (String error){
		setError(error);
	}
	
	public String getCampo(){
		return campo;
	}
	
	public void setCampo(String camp){
		campo = camp;
	}
	public void validarCantCampos(){
		
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getError() {
		return error;
	}
	
}
