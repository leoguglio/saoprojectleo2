package org.saoproject.web.shared.exc;

import java.io.Serializable;

public class ExcepcionTipoCampo extends ExcepcionValidacion implements Serializable{



	/**
	 * 
	 */
	private static final long serialVersionUID = -3437352339014097695L;

	public ExcepcionTipoCampo(){
		super();
	}
	
	public ExcepcionTipoCampo (int linea){
		super(linea);
		System.out.println(getError() + "El tipo del campo es incorrecto]");
	}
	
	public void ExcepcionTipoCampoEntero (String campo) {
		System.out.println(getError() + "El tipo de campo "+campo+" debe ser entero]");
		setCampo(campo);
	}

	public void ExcepcionTipoCampoFloat (String campo) {
		System.out.println(getError() + "El tipo de campo "+campo+" debe ser un numero de punto flotante]");
		setCampo(campo);
	}
	
	public void ExcepcionTipoCampoDouble (String campo) {
		System.out.println(getError() + "El tipo de campo "+campo+" debe ser un numero de punto flotante de doble precisión]");
		setCampo(campo);
	}
	
	public void ExcepcionTipoCampoBooleann (String campo) {
		System.out.println(getError()+"El tipo del campo "+campo+" debe ser boolean");
		setCampo(campo);
	}

}
