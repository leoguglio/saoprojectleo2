package org.saoproject.web.shared;

import java.util.Arrays;
import java.util.List;

public class Tipos {
	public static List<String> lista = Arrays.asList("CAMPANIAS","ESTACIONES","ZONAS",
			  "TALLAS","ESPECIES","DATOS BIOLOGICOS");
	public static List<String> tipo_campania = Arrays.asList("PROSPECCIÓN","MUESTREO PUNTUAL");
	public static List<String> tipo_muestreo = Arrays.asList("RASTRA COMERCIAL","MUESTREO AUTONOMO");

}
